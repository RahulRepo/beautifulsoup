#!/usr/bin/env groovy
import hudson.Util;

pipeline {
  agent {
    docker {
      registryCredentialsId 'ed031c71-da04-4f62-963d-a95d020a6d1e'
      registryUrl 'https://zgwpt-maui-docker-local.jfrog.io'
      image 'python3:latest'
    }
  }

  triggers {
    cron('10 00 * * *')
  }

  parameters {
    string(name: 'BRANCH', defaultValue: 'master', description: 'The branch to create an artifact from')
  }

  stages {
    stage('Checkout seo automation code') {
      steps {
        git([url: 'ssh://git@stash.sv2.trulia.com/seo/seo-automation.git', branch: '${BRANCH}'])
      }
    }
    stage('Install Dependencies and Starting tests...') {
      steps {
        withEnv(["HOME=${env.WORKSPACE}"]) {
          sh(script: 'pip3 install -r requirements.txt --user')
          sh(script: 'python3 -m pytest srp/tests/ -v')
        }
      }
    }
  }

  post {
    always {
      echo 'Job finished. Recording the duration of the job..'
      script{
        duration = Util.getTimeSpanString(System.currentTimeMillis() - currentBuild.startTimeInMillis)
      }

      // Clean up workspace
      cleanWs()
    }

    success {
      echo 'Test Success, Notifying to slack..'
      slackMessage(buildStatusMessage(env, "Success", duration), "good")
    }

    failure {
      echo 'Test Failed, Notifying to slack..'
      slackMessage(buildStatusMessage(env, 'Failed', duration), "danger")
    }

    aborted {
      echo 'Test Aborted, Notifying to slack..'
      slackMessage(buildStatusMessage(env, 'Aborted', duration), "warning")
    }
  }
}

def buildStartedMessage() {
    return """*:jenkins: Pipeline Started: seo-automation*
"""
}

def buildStatusMessage(env, status, duration) {
    def emoji = ":jenkins:"
    if ("${status}" != "Success") {
        emoji = ":angry_jenkins:"
    }
    return  """*${emoji} Pipeline ${status}: seo-automation*
> *Build Number*: `${env.BUILD_NUMBER}`
> *Build URL*: `${env.BUILD_URL}`
> *Duration*: `${duration}`
"""
}

def slackMessage(message, color) {
  slackSend channel: '#ale-automation-notify', message: message, teamDomain: 'zillowgroup', tokenCredentialId: 'slack-secret-token-2', color: color
}