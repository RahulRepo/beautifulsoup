SEO Automation
===============

Pytest test suite for SEO:
* SRP
* PDP (TODO)

Installation
-----------------------

#### Prerequisites
* Python 3.2
* BeautifulSoup4
* Selenium

#### Setup
    git clone ssh://git@stash.sv2.trulia.com/seo/seo-automation.git
    cd seo-automation/
    pip3 install -r requirements.txt

Usage
-----------------------

The following command will execute all tests:

    python3 -m pytest srp/tests/ -v
    

