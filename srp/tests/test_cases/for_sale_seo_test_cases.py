# =================================
#
# Test Suites template:
#
#      page_test_cases= [
#         (
#             URL
#         ),
#         (
#             URL
#        ),
#         ......
#      ]
#
#      navbar_test_cases= [
#         (
#             URL,
#             [navbar_menu_links]
#         ),
#         (
#             URL,
#             [navbar_menu_links]
#        ),
#         ......
#      ]
#
#      footer_test_cases= [
#         (
#             URL,
#             [footer_link_group]
#         ),
#         (
#             URL,
#             [footer_link_group]
#         ),
#         ......
#      ]
#
#      minimized_footer_test_cases= [
#         (
#             URL,
#             [minimized_footer_link_group]
#         ),
#         (
#             URL,
#             [minimized_footer_link_group]
#         ),
#        ......
#     ]
#
# =================================

for_sale_page_test_cases = [
    (
        'https://www.trulia.com/CA/San_Francisco/'
    ),
    (
        'https://www.trulia.com/for_sale/San_Francisco,CA/p_oh/'
    ),
    (
        'https://www.trulia.com/for_sale/San_Francisco,CA/new_homes_lt/'
    ),
    (
        'https://www.trulia.com/for_sale/San_Francisco,CA/SINGLE-FAMILY_HOME_type/'
    ),
    (
        'https://www.trulia.com/for_sale/San_Francisco,CA/'
    ),
    (
        'https://www.trulia.com/for_sale/06075_c/new_homes_lt/'
    ),
    (
        'https://www.trulia.com/for_sale/06075_c/SINGLE-FAMILY_HOME_type/'
    ),
    (
        'https://www.trulia.com/for_sale/06075_c/p_oh/'
    ),
    (
        'https://www.trulia.com/for_sale/1468_nh/new_homes_lt/'
    ),
    (
        'https://www.trulia.com/for_sale/1468_nh/p_oh/'
    ),
    (
        'https://www.trulia.com/for_sale/06075_c/fsbo_lt/'
    ),
    (
        'https://www.trulia.com/for_sale/San_Francisco,CA/fsbo_lt/'
    ),
    (
        'https://www.trulia.com/for_sale/San_Francisco,CA/foreclosure_lt/'
    ),
    (
        'https://www.trulia.com/for_sale/06075_c/foreclosure_lt/'
    ),
    (
        'https://www.trulia.com/for_sale/95073_zip/foreclosure_lt/'
    ),
    (
        'https://www.trulia.com/for_sale/New_York,NY/resale_lt/'
    ),
    (
        'https://www.trulia.com/for_sale/New_York,NY/foreclosure,fsbo,new_homes,resale,sale_pending_lt/'
    ),
    (
        'https://www.trulia.com/for_sale/San_Mateo,CA/37.514857406994,37.580526177666,-122.35212773203,-122.27316349863_xy/14_zm/'
    ),
    (
        'https://www.trulia.com/NY/New_York/'
    ),
    (
        'https://www.trulia.com/for_sale/94546_zip/new_homes_lt/'
    ),
    (
        'https://www.trulia.com/for_sale/1468_nh/SINGLE-FAMILY_HOME_type/'
    ),
    (
        'https://www.trulia.com/for_sale/1468_nh/p_oh/'
    ),
    (
        'https://www.trulia.com/for_sale/1468_nh/fsbo_lt/'
    ),
    (
        'https://www.trulia.com/for_sale/94546_zip/fsbo_lt/'
    ),
    (
        'https://www.trulia.com/for_sale/1468_nh/foreclosure_lt/'
    ),
    (
        'https://www.trulia.com/for_sale/San_Francisco,CA/english-cottages/'
    ),
    (
        'https://www.trulia.com/for_sale/1468_nh/p_oh/'
    ),
    (
        'https://www.trulia.com/for_sale/37.538050449811,38.04863829018,-122.31357480293,-121.68186093574_xy/SINGLE-FAMILY_HOME_type/11_zm/'
    ),
    (
        'https://www.trulia.com/for_sale/36.861754825027,37.888660248935,-121.57103313202,-120.30760539765_xy/foreclosure_lt/10_zm/'
    ),
    (
        'https://www.trulia.com/for_sale/37.684308412501,37.812034250769,-122.48558262954,-122.32765416274_xy/'
    ),
    (
        'https://www.trulia.com/for_sale/29.797777039763,30.092292361495,-90.271999096805,-89.956142163212_xy/1p_beds/12_zm/'
    ),
    (
        'https://www.trulia.com/for_sale/San_Mateo,CA/37.514857406994,37.580526177666,-122.35212773203,-122.27316349863_xy/14_zm/'
    ),
    (
        'https://www.trulia.com/for_sale/37.514857406994,37.580526177666,-122.35212773203,-122.27316349863_xy/14_zm/'
    ),
    (
        'https://www.trulia.com/for_sale/600088777_sd/'
    ),
    (
        'https://www.trulia.com/for_sale/5700044525_sd/'
    ),

]

for_sale_navbar_test_cases = [
    (
        'https://www.trulia.com/CA/San_Francisco/',
        [
            {
                'sectionHeaderText': 'Buy',
                'href': '/CA/San_Francisco/',
                'dropDown': [
                    {
                        'dropDownHeaderText': 'San Francisco',
                        'dropDownList': [
                            {'text': 'Homes for Sale', 'href': '/CA/San_Francisco/'},
                            {'text': 'Open Houses', 'href': '/for_sale/San_Francisco,CA/p_oh/'},
                            {'text': 'New Homes', 'href': '/for_sale/San_Francisco,CA/new_homes_lt/'},
                            {'text': 'Recently Sold', 'href': '/sold/San_Francisco,CA/'}
                        ]
                    },
                    {'text': 'See Newest Listings', 'href': '/new-for-sale-properties/CA/'}
                ]
            },
            {
                'sectionHeaderText': 'Rent',
                'href': '/for_rent/San_Francisco,CA/',
                'dropDown': [
                    {
                        'dropDownHeaderText': 'San Francisco',
                        'dropDownList': [
                            {'text': 'All Rentals', 'href': '/for_rent/San_Francisco,CA/'},
                            {'text': 'Apartments for Rent',
                             'href': '/for_rent/San_Francisco,CA/APARTMENT,APARTMENT_COMMUNITY,APARTMENT%7CCONDO%7CTOWNHOUSE,CONDO,COOP,LOFT,TIC_type/'},
                            {'text': 'Houses for Rent', 'href': '/for_rent/San_Francisco,CA/SINGLE-FAMILY_HOME_type/'},
                            {'text': 'Rooms for Rent', 'href': '/for_rent/San_Francisco,CA/ROOM_FOR_RENT_type/'}
                        ]
                    },
                    {'text': 'See Newest Listings', 'href': '/new-for-rent-properties/CA/'},
                    {'text': 'Post A Rental Listing', 'href': '/post-rental/'}
                ]
            },
            {
                'sectionHeaderText': 'Mortgage',
                'href': '/mortgages/',
                'dropDown': [
                    {
                        'dropDownHeaderText': 'San Francisco',
                        'dropDownList': [
                            {'text': 'Mortgage Overview', 'href': '/mortgages/'},
                            {'text': 'Get Pre-Qualified',
                             'href': '/mortgages/pre-approval?omni_src=mortgage%7Cglobal_nav'},
                            {'text': 'Mortgage Rates', 'href': '/mortgage-rates/San_Francisco,CA/'},
                            {'text': 'Refinance Rates', 'href': '/refinance-rates/San_Francisco,CA/'}
                        ]
                    },
                    {'text': 'Mortgage Calculator', 'href': '/mortgage-payment-calculator/'},
                    {'text': 'Affordability Calculator', 'href': '/house-affordability-calculator/'},
                    {'text': 'Rent vs Buy Calculator', 'href': '/rent_vs_buy/'},
                    {'text': 'Refinance Calculator', 'href': '/home-loan-refinance-calculator/'},
                ]
            },
        ]
    ),
    (
        'https://www.trulia.com/for_sale/San_Francisco,CA/p_oh/',
        [
            {
                'sectionHeaderText': 'Buy',
                'href': '/CA/San_Francisco/',
                'dropDown': [
                    {
                        'dropDownHeaderText': 'San Francisco',
                        'dropDownList': [
                            {'text': 'Homes for Sale', 'href': '/CA/San_Francisco/'},
                            {'text': 'Open Houses', 'href': '/for_sale/San_Francisco,CA/p_oh/'},
                            {'text': 'New Homes', 'href': '/for_sale/San_Francisco,CA/new_homes_lt/'},
                            {'text': 'Recently Sold', 'href': '/sold/San_Francisco,CA/'}
                        ]
                    },
                    {'text': 'See Newest Listings', 'href': '/new-for-sale-properties/CA/'}
                ]
            },
            {
                'sectionHeaderText': 'Rent',
                'href': '/for_rent/San_Francisco,CA/',
                'dropDown': [
                    {
                        'dropDownHeaderText': 'San Francisco',
                        'dropDownList': [
                            {'text': 'All Rentals', 'href': '/for_rent/San_Francisco,CA/'},
                            {'text': 'Apartments for Rent',
                             'href': '/for_rent/San_Francisco,CA/APARTMENT,APARTMENT_COMMUNITY,APARTMENT%7CCONDO%7CTOWNHOUSE,CONDO,COOP,LOFT,TIC_type/'},
                            {'text': 'Houses for Rent', 'href': '/for_rent/San_Francisco,CA/SINGLE-FAMILY_HOME_type/'},
                            {'text': 'Rooms for Rent', 'href': '/for_rent/San_Francisco,CA/ROOM_FOR_RENT_type/'}
                        ]
                    },
                    {'text': 'See Newest Listings', 'href': '/new-for-rent-properties/CA/'},
                    {'text': 'Post A Rental Listing', 'href': '/post-rental/'}
                ]
            },
            {
                'sectionHeaderText': 'Mortgage',
                'href': '/mortgages/',
                'dropDown': [
                    {
                        'dropDownHeaderText': 'San Francisco',
                        'dropDownList': [
                            {'text': 'Mortgage Overview', 'href': '/mortgages/'},
                            {'text': 'Get Pre-Qualified',
                             'href': '/mortgages/pre-approval?omni_src=mortgage%7Cglobal_nav'},
                            {'text': 'Mortgage Rates', 'href': '/mortgage-rates/San_Francisco,CA/'},
                            {'text': 'Refinance Rates', 'href': '/refinance-rates/San_Francisco,CA/'}
                        ]
                    },
                    {'text': 'Mortgage Calculator', 'href': '/mortgage-payment-calculator/'},
                    {'text': 'Affordability Calculator', 'href': '/house-affordability-calculator/'},
                    {'text': 'Rent vs Buy Calculator', 'href': '/rent_vs_buy/'},
                    {'text': 'Refinance Calculator', 'href': '/home-loan-refinance-calculator/'},
                ]
            },
        ]
    ),
    (
        'https://www.trulia.com/for_sale/San_Francisco,CA/new_homes_lt/',
        [
            {
                'sectionHeaderText': 'Buy',
                'href': '/CA/San_Francisco/',
                'dropDown': [
                    {
                        'dropDownHeaderText': 'San Francisco',
                        'dropDownList': [
                            {'text': 'Homes for Sale', 'href': '/CA/San_Francisco/'},
                            {'text': 'Open Houses', 'href': '/for_sale/San_Francisco,CA/p_oh/'},
                            {'text': 'New Homes', 'href': '/for_sale/San_Francisco,CA/new_homes_lt/'},
                            {'text': 'Recently Sold', 'href': '/sold/San_Francisco,CA/'}
                        ]
                    },
                    {'text': 'See Newest Listings', 'href': '/new-for-sale-properties/CA/'}
                ]
            },
            {
                'sectionHeaderText': 'Rent',
                'href': '/for_rent/San_Francisco,CA/',
                'dropDown': [
                    {
                        'dropDownHeaderText': 'San Francisco',
                        'dropDownList': [
                            {'text': 'All Rentals', 'href': '/for_rent/San_Francisco,CA/'},
                            {'text': 'Apartments for Rent',
                             'href': '/for_rent/San_Francisco,CA/APARTMENT,APARTMENT_COMMUNITY,APARTMENT%7CCONDO%7CTOWNHOUSE,CONDO,COOP,LOFT,TIC_type/'},
                            {'text': 'Houses for Rent', 'href': '/for_rent/San_Francisco,CA/SINGLE-FAMILY_HOME_type/'},
                            {'text': 'Rooms for Rent', 'href': '/for_rent/San_Francisco,CA/ROOM_FOR_RENT_type/'}
                        ]
                    },
                    {'text': 'See Newest Listings', 'href': '/new-for-rent-properties/CA/'},
                    {'text': 'Post A Rental Listing', 'href': '/post-rental/'}
                ]
            },
            {
                'sectionHeaderText': 'Mortgage',
                'href': '/mortgages/',
                'dropDown': [
                    {
                        'dropDownHeaderText': 'San Francisco',
                        'dropDownList': [
                            {'text': 'Mortgage Overview', 'href': '/mortgages/'},
                            {'text': 'Get Pre-Qualified',
                             'href': '/mortgages/pre-approval?omni_src=mortgage%7Cglobal_nav'},
                            {'text': 'Mortgage Rates', 'href': '/mortgage-rates/San_Francisco,CA/'},
                            {'text': 'Refinance Rates', 'href': '/refinance-rates/San_Francisco,CA/'}
                        ]
                    },
                    {'text': 'Mortgage Calculator', 'href': '/mortgage-payment-calculator/'},
                    {'text': 'Affordability Calculator', 'href': '/house-affordability-calculator/'},
                    {'text': 'Rent vs Buy Calculator', 'href': '/rent_vs_buy/'},
                    {'text': 'Refinance Calculator', 'href': '/home-loan-refinance-calculator/'},
                ]
            },
        ]
    ),
    (
        'https://www.trulia.com/for_sale/San_Francisco,CA/SINGLE-FAMILY_HOME_type/',
        [
            {
                'sectionHeaderText': 'Buy',
                'href': '/CA/San_Francisco/',
                'dropDown': [
                    {
                        'dropDownHeaderText': 'San Francisco',
                        'dropDownList': [
                            {'text': 'Homes for Sale', 'href': '/CA/San_Francisco/'},
                            {'text': 'Open Houses', 'href': '/for_sale/San_Francisco,CA/p_oh/'},
                            {'text': 'New Homes', 'href': '/for_sale/San_Francisco,CA/new_homes_lt/'},
                            {'text': 'Recently Sold', 'href': '/sold/San_Francisco,CA/'}
                        ]
                    },
                    {'text': 'See Newest Listings', 'href': '/new-for-sale-properties/CA/'}
                ]
            },
            {
                'sectionHeaderText': 'Rent',
                'href': '/for_rent/San_Francisco,CA/',
                'dropDown': [
                    {
                        'dropDownHeaderText': 'San Francisco',
                        'dropDownList': [
                            {'text': 'All Rentals', 'href': '/for_rent/San_Francisco,CA/'},
                            {'text': 'Apartments for Rent',
                             'href': '/for_rent/San_Francisco,CA/APARTMENT,APARTMENT_COMMUNITY,APARTMENT%7CCONDO%7CTOWNHOUSE,CONDO,COOP,LOFT,TIC_type/'},
                            {'text': 'Houses for Rent', 'href': '/for_rent/San_Francisco,CA/SINGLE-FAMILY_HOME_type/'},
                            {'text': 'Rooms for Rent', 'href': '/for_rent/San_Francisco,CA/ROOM_FOR_RENT_type/'}
                        ]
                    },
                    {'text': 'See Newest Listings', 'href': '/new-for-rent-properties/CA/'},
                    {'text': 'Post A Rental Listing', 'href': '/post-rental/'}
                ]
            },
            {
                'sectionHeaderText': 'Mortgage',
                'href': '/mortgages/',
                'dropDown': [
                    {
                        'dropDownHeaderText': 'San Francisco',
                        'dropDownList': [
                            {'text': 'Mortgage Overview', 'href': '/mortgages/'},
                            {'text': 'Get Pre-Qualified',
                             'href': '/mortgages/pre-approval?omni_src=mortgage%7Cglobal_nav'},
                            {'text': 'Mortgage Rates', 'href': '/mortgage-rates/San_Francisco,CA/'},
                            {'text': 'Refinance Rates', 'href': '/refinance-rates/San_Francisco,CA/'}
                        ]
                    },
                    {'text': 'Mortgage Calculator', 'href': '/mortgage-payment-calculator/'},
                    {'text': 'Affordability Calculator', 'href': '/house-affordability-calculator/'},
                    {'text': 'Rent vs Buy Calculator', 'href': '/rent_vs_buy/'},
                    {'text': 'Refinance Calculator', 'href': '/home-loan-refinance-calculator/'},
                ]
            },

        ]
    ),
    (
        'https://www.trulia.com/for_sale/San_Francisco,CA/',
        [
            {
                'sectionHeaderText': 'Buy',
                'href': '/CA/San_Francisco/',
                'dropDown': [
                    {
                        'dropDownHeaderText': 'San Francisco',
                        'dropDownList': [
                            {'text': 'Homes for Sale', 'href': '/CA/San_Francisco/'},
                            {'text': 'Open Houses', 'href': '/for_sale/San_Francisco,CA/p_oh/'},
                            {'text': 'New Homes', 'href': '/for_sale/San_Francisco,CA/new_homes_lt/'},
                            {'text': 'Recently Sold', 'href': '/sold/San_Francisco,CA/'}
                        ]
                    },
                    {'text': 'See Newest Listings', 'href': '/new-for-sale-properties/CA/'}
                ]
            },
            {
                'sectionHeaderText': 'Rent',
                'href': '/for_rent/San_Francisco,CA/',
                'dropDown': [
                    {
                        'dropDownHeaderText': 'San Francisco',
                        'dropDownList': [
                            {'text': 'All Rentals', 'href': '/for_rent/San_Francisco,CA/'},
                            {'text': 'Apartments for Rent',
                             'href': '/for_rent/San_Francisco,CA/APARTMENT,APARTMENT_COMMUNITY,APARTMENT%7CCONDO%7CTOWNHOUSE,CONDO,COOP,LOFT,TIC_type/'},
                            {'text': 'Houses for Rent', 'href': '/for_rent/San_Francisco,CA/SINGLE-FAMILY_HOME_type/'},
                            {'text': 'Rooms for Rent', 'href': '/for_rent/San_Francisco,CA/ROOM_FOR_RENT_type/'}
                        ]
                    },
                    {'text': 'See Newest Listings', 'href': '/new-for-rent-properties/CA/'},
                    {'text': 'Post A Rental Listing', 'href': '/post-rental/'}
                ]
            },
            {
                'sectionHeaderText': 'Mortgage',
                'href': '/mortgages/',
                'dropDown': [
                    {
                        'dropDownHeaderText': 'San Francisco',
                        'dropDownList': [
                            {'text': 'Mortgage Overview', 'href': '/mortgages/'},
                            {'text': 'Get Pre-Qualified',
                             'href': '/mortgages/pre-approval?omni_src=mortgage%7Cglobal_nav'},
                            {'text': 'Mortgage Rates', 'href': '/mortgage-rates/San_Francisco,CA/'},
                            {'text': 'Refinance Rates', 'href': '/refinance-rates/San_Francisco,CA/'}
                        ]
                    },
                    {'text': 'Mortgage Calculator', 'href': '/mortgage-payment-calculator/'},
                    {'text': 'Affordability Calculator', 'href': '/house-affordability-calculator/'},
                    {'text': 'Rent vs Buy Calculator', 'href': '/rent_vs_buy/'},
                    {'text': 'Refinance Calculator', 'href': '/home-loan-refinance-calculator/'},
                ]
            },
        ]
    ),
    (
        'https://www.trulia.com/for_sale/06075_c/new_homes_lt/',
        [
            {
                'sectionHeaderText': 'Buy',
                'href': '/for_sale/06075_c/',
                'dropDown': [
                    {
                        'dropDownHeaderText': 'San Francisco County County',
                        'dropDownList': [
                            {'text': 'Homes for Sale', 'href': '/for_sale/06075_c/'},
                            {'text': 'Open Houses', 'href': '/for_sale/06075_c/p_oh/'},
                            {'text': 'New Homes', 'href': '/for_sale/06075_c/new_homes_lt/'},
                            {'text': 'Recently Sold', 'href': '/sold/06075_c/'}
                        ]
                    },
                    {'text': 'See Newest Listings', 'href': '/new-for-sale-properties/CA/'}
                ]
            },
            {
                'sectionHeaderText': 'Rent',
                'href': '/for_rent/06075_c/',
                'dropDown': [
                    {
                        'dropDownHeaderText': 'San Francisco County County',
                        'dropDownList': [
                            {'text': 'All Rentals', 'href': '/for_rent/06075_c/'},
                            {'text': 'Apartments for Rent',
                             'href': '/for_rent/06075_c/APARTMENT,APARTMENT_COMMUNITY,APARTMENT%7CCONDO%7CTOWNHOUSE,CONDO,COOP,LOFT,TIC_type/'},
                            {'text': 'Houses for Rent', 'href': '/for_rent/06075_c/SINGLE-FAMILY_HOME_type/'},
                            {'text': 'Rooms for Rent', 'href': '/for_rent/06075_c/ROOM_FOR_RENT_type/'}
                        ]
                    },
                    {'text': 'See Newest Listings', 'href': '/new-for-rent-properties/CA/'},
                    {'text': 'Post A Rental Listing', 'href': '/post-rental/'}
                ]
            },
            {
                'sectionHeaderText': 'Mortgage',
                'href': '/mortgages/',
                'dropDown': [
                    {'text': 'Mortgage Overview', 'href': '/mortgages/'},
                    {'text': 'Get Pre-Qualified',
                     'href': '/mortgages/pre-approval?omni_src=mortgage%7Cglobal_nav'},
                    {'text': 'Mortgage Calculator', 'href': '/mortgage-payment-calculator/'},
                    {'text': 'Affordability Calculator', 'href': '/house-affordability-calculator/'},
                    {'text': 'Rent vs Buy Calculator', 'href': '/rent_vs_buy/'},
                    {'text': 'Refinance Calculator', 'href': '/home-loan-refinance-calculator/'},
                ]
            },

        ]
    ),
    (
        'https://www.trulia.com/for_sale/06075_c/SINGLE-FAMILY_HOME_type/',
        [
            {
                'sectionHeaderText': 'Buy',
                'href': '/for_sale/06075_c/',
                'dropDown': [
                    {
                        'dropDownHeaderText': 'San Francisco County County',
                        'dropDownList': [
                            {'text': 'Homes for Sale', 'href': '/for_sale/06075_c/'},
                            {'text': 'Open Houses', 'href': '/for_sale/06075_c/p_oh/'},
                            {'text': 'New Homes', 'href': '/for_sale/06075_c/new_homes_lt/'},
                            {'text': 'Recently Sold', 'href': '/sold/06075_c/'}
                        ]
                    },
                    {'text': 'See Newest Listings', 'href': '/new-for-sale-properties/CA/'}
                ]
            },
            {
                'sectionHeaderText': 'Rent',
                'href': '/for_rent/06075_c/',
                'dropDown': [
                    {
                        'dropDownHeaderText': 'San Francisco County County',
                        'dropDownList': [
                            {'text': 'All Rentals', 'href': '/for_rent/06075_c/'},
                            {'text': 'Apartments for Rent',
                             'href': '/for_rent/06075_c/APARTMENT,APARTMENT_COMMUNITY,APARTMENT%7CCONDO%7CTOWNHOUSE,CONDO,COOP,LOFT,TIC_type/'},
                            {'text': 'Houses for Rent', 'href': '/for_rent/06075_c/SINGLE-FAMILY_HOME_type/'},
                            {'text': 'Rooms for Rent', 'href': '/for_rent/06075_c/ROOM_FOR_RENT_type/'}
                        ]
                    },
                    {'text': 'See Newest Listings', 'href': '/new-for-rent-properties/CA/'},
                    {'text': 'Post A Rental Listing', 'href': '/post-rental/'}
                ]
            },
            {
                'sectionHeaderText': 'Mortgage',
                'href': '/mortgages/',
                'dropDown': [
                    {'text': 'Mortgage Overview', 'href': '/mortgages/'},
                    {'text': 'Get Pre-Qualified',
                     'href': '/mortgages/pre-approval?omni_src=mortgage%7Cglobal_nav'},
                    {'text': 'Mortgage Calculator', 'href': '/mortgage-payment-calculator/'},
                    {'text': 'Affordability Calculator', 'href': '/house-affordability-calculator/'},
                    {'text': 'Rent vs Buy Calculator', 'href': '/rent_vs_buy/'},
                    {'text': 'Refinance Calculator', 'href': '/home-loan-refinance-calculator/'},
                ]
            },
        ]
    ),
    (
        'https://www.trulia.com/for_sale/06075_c/p_oh/',
        [
            {
                'sectionHeaderText': 'Buy',
                'href': '/for_sale/06075_c/',
                'dropDown': [
                    {
                        'dropDownHeaderText': 'San Francisco County County',
                        'dropDownList': [
                            {'text': 'Homes for Sale', 'href': '/for_sale/06075_c/'},
                            {'text': 'Open Houses', 'href': '/for_sale/06075_c/p_oh/'},
                            {'text': 'New Homes', 'href': '/for_sale/06075_c/new_homes_lt/'},
                            {'text': 'Recently Sold', 'href': '/sold/06075_c/'}
                        ]
                    },
                    {'text': 'See Newest Listings', 'href': '/new-for-sale-properties/CA/'}
                ]
            },
            {
                'sectionHeaderText': 'Rent',
                'href': '/for_rent/06075_c/',
                'dropDown': [
                    {
                        'dropDownHeaderText': 'San Francisco County County',
                        'dropDownList': [
                            {'text': 'All Rentals', 'href': '/for_rent/06075_c/'},
                            {'text': 'Apartments for Rent',
                             'href': '/for_rent/06075_c/APARTMENT,APARTMENT_COMMUNITY,APARTMENT%7CCONDO%7CTOWNHOUSE,CONDO,COOP,LOFT,TIC_type/'},
                            {'text': 'Houses for Rent', 'href': '/for_rent/06075_c/SINGLE-FAMILY_HOME_type/'},
                            {'text': 'Rooms for Rent', 'href': '/for_rent/06075_c/ROOM_FOR_RENT_type/'}
                        ]
                    },
                    {'text': 'See Newest Listings', 'href': '/new-for-rent-properties/CA/'},
                    {'text': 'Post A Rental Listing', 'href': '/post-rental/'}
                ]
            },
            {
                'sectionHeaderText': 'Mortgage',
                'href': '/mortgages/',
                'dropDown': [
                    {'text': 'Mortgage Overview', 'href': '/mortgages/'},
                    {'text': 'Get Pre-Qualified',
                     'href': '/mortgages/pre-approval?omni_src=mortgage%7Cglobal_nav'},
                    {'text': 'Mortgage Calculator', 'href': '/mortgage-payment-calculator/'},
                    {'text': 'Affordability Calculator', 'href': '/house-affordability-calculator/'},
                    {'text': 'Rent vs Buy Calculator', 'href': '/rent_vs_buy/'},
                    {'text': 'Refinance Calculator', 'href': '/home-loan-refinance-calculator/'},
                ]
            },

        ]
    ),
    (
        'https://www.trulia.com/for_sale/1468_nh/new_homes_lt/',
        [
            {
                'sectionHeaderText': 'Buy',
                'href': '/CA/San_Francisco,1468,Tenderloin/',
                'dropDown': [
                    {
                        'dropDownHeaderText': 'Tenderloin',
                        'dropDownList': [
                            {'text': 'Homes for Sale', 'href': '/CA/San_Francisco,1468,Tenderloin/'},
                            {'text': 'Open Houses', 'href': '/for_sale/1468_nh/p_oh/'},
                            {'text': 'New Homes', 'href': '/for_sale/1468_nh/new_homes_lt/'},
                            {'text': 'Recently Sold', 'href': '/sold/1468_nh/'}
                        ]
                    },
                    {
                        'dropDownHeaderText': 'San Francisco',
                        'dropDownList': [
                            {'text': 'Homes for Sale', 'href': '/CA/San_Francisco/'},
                            {'text': 'Open Houses', 'href': '/for_sale/San_Francisco,CA/p_oh/'},
                            {'text': 'Recently Sold', 'href': '/sold/San_Francisco,CA/'}
                        ]
                    },
                    {'text': 'See Newest Listings', 'href': '/new-for-sale-properties/CA/'}
                ]
            },
            {
                'sectionHeaderText': 'Rent',
                'href': '/for_rent/1468_nh/',
                'dropDown': [
                    {
                        'dropDownHeaderText': 'Tenderloin',
                        'dropDownList': [
                            {'text': 'All Rentals', 'href': '/for_rent/1468_nh/'},
                            {'text': 'Apartments for Rent',
                             'href': '/for_rent/1468_nh/APARTMENT,APARTMENT_COMMUNITY,APARTMENT%7CCONDO%7CTOWNHOUSE,CONDO,COOP,LOFT,TIC_type/'},
                            {'text': 'Houses for Rent', 'href': '/for_rent/1468_nh/SINGLE-FAMILY_HOME_type/'},
                            {'text': 'Rooms for Rent', 'href': '/for_rent/1468_nh/ROOM_FOR_RENT_type/'}
                        ]
                    },
                    {
                        'dropDownHeaderText': 'San Francisco',
                        'dropDownList': [
                            {'text': 'All Rentals', 'href': '/for_rent/San_Francisco,CA/'},
                            {'text': 'Houses for Rent', 'href': '/for_rent/San_Francisco,CA/SINGLE-FAMILY_HOME_type/'},
                            {'text': 'Rooms for Rent', 'href': '/for_rent/San_Francisco,CA/ROOM_FOR_RENT_type/'}
                        ]
                    },
                    {'text': 'See Newest Listings', 'href': '/new-for-rent-properties/CA/'},
                    {'text': 'Post A Rental Listing', 'href': '/post-rental/'}
                ]
            },
            {
                'sectionHeaderText': 'Mortgage',
                'href': '/mortgages/',
                'dropDown': [
                    {
                        'dropDownHeaderText': 'San Francisco',
                        'dropDownList': [
                            {'text': 'Mortgage Overview', 'href': '/mortgages/'},
                            {'text': 'Get Pre-Qualified',
                             'href': '/mortgages/pre-approval?omni_src=mortgage%7Cglobal_nav'},
                            {'text': 'Mortgage Rates', 'href': '/mortgage-rates/San_Francisco,CA/'},
                            {'text': 'Refinance Rates', 'href': '/refinance-rates/San_Francisco,CA/'}
                        ]
                    },
                    {'text': 'Mortgage Calculator', 'href': '/mortgage-payment-calculator/'},
                    {'text': 'Affordability Calculator', 'href': '/house-affordability-calculator/'},
                    {'text': 'Rent vs Buy Calculator', 'href': '/rent_vs_buy/'},
                    {'text': 'Refinance Calculator', 'href': '/home-loan-refinance-calculator/'},
                ]
            },
        ]
    ),
    (
        'https://www.trulia.com/for_sale/1468_nh/p_oh/',
        [
            {
                'sectionHeaderText': 'Buy',
                'href': '/CA/San_Francisco,1468,Tenderloin/',
                'dropDown': [
                    {
                        'dropDownHeaderText': 'Tenderloin',
                        'dropDownList': [
                            {'text': 'Homes for Sale', 'href': '/CA/San_Francisco,1468,Tenderloin/'},
                            {'text': 'Open Houses', 'href': '/for_sale/1468_nh/p_oh/'},
                            {'text': 'New Homes', 'href': '/for_sale/1468_nh/new_homes_lt/'},
                            {'text': 'Recently Sold', 'href': '/sold/1468_nh/'}
                        ]
                    },
                    {
                        'dropDownHeaderText': 'San Francisco',
                        'dropDownList': [
                            {'text': 'Homes for Sale', 'href': '/CA/San_Francisco/'},
                            {'text': 'Open Houses', 'href': '/for_sale/San_Francisco,CA/p_oh/'},
                            {'text': 'Recently Sold', 'href': '/sold/San_Francisco,CA/'}
                        ]
                    },
                    {'text': 'See Newest Listings', 'href': '/new-for-sale-properties/CA/'}
                ]
            },
            {
                'sectionHeaderText': 'Rent',
                'href': '/for_rent/1468_nh/',
                'dropDown': [
                    {
                        'dropDownHeaderText': 'Tenderloin',
                        'dropDownList': [
                            {'text': 'All Rentals', 'href': '/for_rent/1468_nh/'},
                            {'text': 'Apartments for Rent',
                             'href': '/for_rent/1468_nh/APARTMENT,APARTMENT_COMMUNITY,APARTMENT%7CCONDO%7CTOWNHOUSE,CONDO,COOP,LOFT,TIC_type/'},
                            {'text': 'Houses for Rent', 'href': '/for_rent/1468_nh/SINGLE-FAMILY_HOME_type/'},
                            {'text': 'Rooms for Rent', 'href': '/for_rent/1468_nh/ROOM_FOR_RENT_type/'}
                        ]
                    },
                    {
                        'dropDownHeaderText': 'San Francisco',
                        'dropDownList': [
                            {'text': 'All Rentals', 'href': '/for_rent/San_Francisco,CA/'},
                            {'text': 'Houses for Rent', 'href': '/for_rent/San_Francisco,CA/SINGLE-FAMILY_HOME_type/'},
                            {'text': 'Rooms for Rent', 'href': '/for_rent/San_Francisco,CA/ROOM_FOR_RENT_type/'}
                        ]
                    },
                    {'text': 'See Newest Listings', 'href': '/new-for-rent-properties/CA/'},
                    {'text': 'Post A Rental Listing', 'href': '/post-rental/'}
                ]
            },
            {
                'sectionHeaderText': 'Mortgage',
                'href': '/mortgages/',
                'dropDown': [
                    {
                        'dropDownHeaderText': 'San Francisco',
                        'dropDownList': [
                            {'text': 'Mortgage Overview', 'href': '/mortgages/'},
                            {'text': 'Get Pre-Qualified',
                             'href': '/mortgages/pre-approval?omni_src=mortgage%7Cglobal_nav'},
                            {'text': 'Mortgage Rates', 'href': '/mortgage-rates/San_Francisco,CA/'},
                            {'text': 'Refinance Rates', 'href': '/refinance-rates/San_Francisco,CA/'}
                        ]
                    },
                    {'text': 'Mortgage Calculator', 'href': '/mortgage-payment-calculator/'},
                    {'text': 'Affordability Calculator', 'href': '/house-affordability-calculator/'},
                    {'text': 'Rent vs Buy Calculator', 'href': '/rent_vs_buy/'},
                    {'text': 'Refinance Calculator', 'href': '/home-loan-refinance-calculator/'},
                ]
            },

        ]
    ),
    (
        'https://www.trulia.com/for_sale/06075_c/fsbo_lt/',
        [
            {
                'sectionHeaderText': 'Buy',
                'href': '/for_sale/06075_c/',
                'dropDown': [
                    {
                        'dropDownHeaderText': 'San Francisco County County',
                        'dropDownList': [
                            {'text': 'Homes for Sale', 'href': '/for_sale/06075_c/'},
                            {'text': 'Open Houses', 'href': '/for_sale/06075_c/p_oh/'},
                            {'text': 'New Homes', 'href': '/for_sale/06075_c/new_homes_lt/'},
                            {'text': 'Recently Sold', 'href': '/sold/06075_c/'}
                        ]
                    },
                    {'text': 'See Newest Listings', 'href': '/new-for-sale-properties/CA/'}
                ]
            },
            {
                'sectionHeaderText': 'Rent',
                'href': '/for_rent/06075_c/',
                'dropDown': [
                    {
                        'dropDownHeaderText': 'San Francisco County County',
                        'dropDownList': [
                            {'text': 'All Rentals', 'href': '/for_rent/06075_c/'},
                            {'text': 'Apartments for Rent',
                             'href': '/for_rent/06075_c/APARTMENT,APARTMENT_COMMUNITY,APARTMENT%7CCONDO%7CTOWNHOUSE,CONDO,COOP,LOFT,TIC_type/'},
                            {'text': 'Houses for Rent', 'href': '/for_rent/06075_c/SINGLE-FAMILY_HOME_type/'},
                            {'text': 'Rooms for Rent', 'href': '/for_rent/06075_c/ROOM_FOR_RENT_type/'}
                        ]
                    },
                    {'text': 'See Newest Listings', 'href': '/new-for-rent-properties/CA/'},
                    {'text': 'Post A Rental Listing', 'href': '/post-rental/'}
                ]
            },
            {
                'sectionHeaderText': 'Mortgage',
                'href': '/mortgages/',
                'dropDown': [
                    {'text': 'Mortgage Overview', 'href': '/mortgages/'},
                    {'text': 'Get Pre-Qualified',
                     'href': '/mortgages/pre-approval?omni_src=mortgage%7Cglobal_nav'},
                    {'text': 'Mortgage Calculator', 'href': '/mortgage-payment-calculator/'},
                    {'text': 'Affordability Calculator', 'href': '/house-affordability-calculator/'},
                    {'text': 'Rent vs Buy Calculator', 'href': '/rent_vs_buy/'},
                    {'text': 'Refinance Calculator', 'href': '/home-loan-refinance-calculator/'},
                ]
            },

        ]
    ),
    (
        'https://www.trulia.com/for_sale/San_Francisco,CA/fsbo_lt/',
        [
            {
                'sectionHeaderText': 'Buy',
                'href': '/CA/San_Francisco/',
                'dropDown': [
                    {
                        'dropDownHeaderText': 'San Francisco',
                        'dropDownList': [
                            {'text': 'Homes for Sale', 'href': '/CA/San_Francisco/'},
                            {'text': 'Open Houses', 'href': '/for_sale/San_Francisco,CA/p_oh/'},
                            {'text': 'New Homes', 'href': '/for_sale/San_Francisco,CA/new_homes_lt/'},
                            {'text': 'Recently Sold', 'href': '/sold/San_Francisco,CA/'}
                        ]
                    },
                    {'text': 'See Newest Listings', 'href': '/new-for-sale-properties/CA/'}
                ]
            },
            {
                'sectionHeaderText': 'Rent',
                'href': '/for_rent/San_Francisco,CA/',
                'dropDown': [
                    {
                        'dropDownHeaderText': 'San Francisco',
                        'dropDownList': [
                            {'text': 'All Rentals', 'href': '/for_rent/San_Francisco,CA/'},
                            {'text': 'Apartments for Rent',
                             'href': '/for_rent/San_Francisco,CA/APARTMENT,APARTMENT_COMMUNITY,APARTMENT%7CCONDO%7CTOWNHOUSE,CONDO,COOP,LOFT,TIC_type/'},
                            {'text': 'Houses for Rent', 'href': '/for_rent/San_Francisco,CA/SINGLE-FAMILY_HOME_type/'},
                            {'text': 'Rooms for Rent', 'href': '/for_rent/San_Francisco,CA/ROOM_FOR_RENT_type/'}
                        ]
                    },
                    {'text': 'See Newest Listings', 'href': '/new-for-rent-properties/CA/'},
                    {'text': 'Post A Rental Listing', 'href': '/post-rental/'}
                ]
            },
            {
                'sectionHeaderText': 'Mortgage',
                'href': '/mortgages/',
                'dropDown': [
                    {
                        'dropDownHeaderText': 'San Francisco',
                        'dropDownList': [
                            {'text': 'Mortgage Overview', 'href': '/mortgages/'},
                            {'text': 'Get Pre-Qualified',
                             'href': '/mortgages/pre-approval?omni_src=mortgage%7Cglobal_nav'},
                            {'text': 'Mortgage Rates', 'href': '/mortgage-rates/San_Francisco,CA/'},
                            {'text': 'Refinance Rates', 'href': '/refinance-rates/San_Francisco,CA/'}
                        ]
                    },
                    {'text': 'Mortgage Calculator', 'href': '/mortgage-payment-calculator/'},
                    {'text': 'Affordability Calculator', 'href': '/house-affordability-calculator/'},
                    {'text': 'Rent vs Buy Calculator', 'href': '/rent_vs_buy/'},
                    {'text': 'Refinance Calculator', 'href': '/home-loan-refinance-calculator/'},
                ]
            },

        ]
    ),
    (
        'https://www.trulia.com/for_sale/San_Francisco,CA/foreclosure_lt/',
        [
            {
                'sectionHeaderText': 'Buy',
                'href': '/CA/San_Francisco/',
                'dropDown': [
                    {
                        'dropDownHeaderText': 'San Francisco',
                        'dropDownList': [
                            {'text': 'Homes for Sale', 'href': '/CA/San_Francisco/'},
                            {'text': 'Open Houses', 'href': '/for_sale/San_Francisco,CA/p_oh/'},
                            {'text': 'New Homes', 'href': '/for_sale/San_Francisco,CA/new_homes_lt/'},
                            {'text': 'Recently Sold', 'href': '/sold/San_Francisco,CA/'}
                        ]
                    },
                    {'text': 'See Newest Listings', 'href': '/new-for-sale-properties/CA/'}
                ]
            },
            {
                'sectionHeaderText': 'Rent',
                'href': '/for_rent/San_Francisco,CA/',
                'dropDown': [
                    {
                        'dropDownHeaderText': 'San Francisco',
                        'dropDownList': [
                            {'text': 'All Rentals', 'href': '/for_rent/San_Francisco,CA/'},
                            {'text': 'Apartments for Rent',
                             'href': '/for_rent/San_Francisco,CA/APARTMENT,APARTMENT_COMMUNITY,APARTMENT%7CCONDO%7CTOWNHOUSE,CONDO,COOP,LOFT,TIC_type/'},
                            {'text': 'Houses for Rent', 'href': '/for_rent/San_Francisco,CA/SINGLE-FAMILY_HOME_type/'},
                            {'text': 'Rooms for Rent', 'href': '/for_rent/San_Francisco,CA/ROOM_FOR_RENT_type/'}
                        ]
                    },
                    {'text': 'See Newest Listings', 'href': '/new-for-rent-properties/CA/'},
                    {'text': 'Post A Rental Listing', 'href': '/post-rental/'}
                ]
            },
            {
                'sectionHeaderText': 'Mortgage',
                'href': '/mortgages/',
                'dropDown': [
                    {
                        'dropDownHeaderText': 'San Francisco',
                        'dropDownList': [
                            {'text': 'Mortgage Overview', 'href': '/mortgages/'},
                            {'text': 'Get Pre-Qualified',
                             'href': '/mortgages/pre-approval?omni_src=mortgage%7Cglobal_nav'},
                            {'text': 'Mortgage Rates', 'href': '/mortgage-rates/San_Francisco,CA/'},
                            {'text': 'Refinance Rates', 'href': '/refinance-rates/San_Francisco,CA/'}
                        ]
                    },
                    {'text': 'Mortgage Calculator', 'href': '/mortgage-payment-calculator/'},
                    {'text': 'Affordability Calculator', 'href': '/house-affordability-calculator/'},
                    {'text': 'Rent vs Buy Calculator', 'href': '/rent_vs_buy/'},
                    {'text': 'Refinance Calculator', 'href': '/home-loan-refinance-calculator/'},
                ]
            },

        ]
    ),
    (
        'https://www.trulia.com/for_sale/06075_c/foreclosure_lt/',
        [
            {
                'sectionHeaderText': 'Buy',
                'href': '/for_sale/06075_c/',
                'dropDown': [
                    {
                        'dropDownHeaderText': 'San Francisco County County',
                        'dropDownList': [
                            {'text': 'Homes for Sale', 'href': '/for_sale/06075_c/'},
                            {'text': 'Open Houses', 'href': '/for_sale/06075_c/p_oh/'},
                            {'text': 'New Homes', 'href': '/for_sale/06075_c/new_homes_lt/'},
                            {'text': 'Recently Sold', 'href': '/sold/06075_c/'}
                        ]
                    },
                    {'text': 'See Newest Listings', 'href': '/new-for-sale-properties/CA/'}
                ]
            },
            {
                'sectionHeaderText': 'Rent',
                'href': '/for_rent/06075_c/',
                'dropDown': [
                    {
                        'dropDownHeaderText': 'San Francisco County County',
                        'dropDownList': [
                            {'text': 'All Rentals', 'href': '/for_rent/06075_c/'},
                            {'text': 'Apartments for Rent',
                             'href': '/for_rent/06075_c/APARTMENT,APARTMENT_COMMUNITY,APARTMENT%7CCONDO%7CTOWNHOUSE,CONDO,COOP,LOFT,TIC_type/'},
                            {'text': 'Houses for Rent', 'href': '/for_rent/06075_c/SINGLE-FAMILY_HOME_type/'},
                            {'text': 'Rooms for Rent', 'href': '/for_rent/06075_c/ROOM_FOR_RENT_type/'}
                        ]
                    },
                    {'text': 'See Newest Listings', 'href': '/new-for-rent-properties/CA/'},
                    {'text': 'Post A Rental Listing', 'href': '/post-rental/'}
                ]
            },
            {
                'sectionHeaderText': 'Mortgage',
                'href': '/mortgages/',
                'dropDown': [
                    {'text': 'Mortgage Overview', 'href': '/mortgages/'},
                    {'text': 'Get Pre-Qualified',
                     'href': '/mortgages/pre-approval?omni_src=mortgage%7Cglobal_nav'},
                    {'text': 'Mortgage Calculator', 'href': '/mortgage-payment-calculator/'},
                    {'text': 'Affordability Calculator', 'href': '/house-affordability-calculator/'},
                    {'text': 'Rent vs Buy Calculator', 'href': '/rent_vs_buy/'},
                    {'text': 'Refinance Calculator', 'href': '/home-loan-refinance-calculator/'},
                ]
            },

        ]
    ),
    (
        'https://www.trulia.com/for_sale/95073_zip/foreclosure_lt/',
        [
            {
                'sectionHeaderText': 'Buy',
                'href': '/CA/Soquel/95073/',
                'dropDown': [
                    {
                        'dropDownHeaderText': '95073',
                        'dropDownList': [
                            {'text': 'Homes for Sale', 'href': '/CA/Soquel/95073/'},
                            {'text': 'Open Houses', 'href': '/for_sale/95073_zip/p_oh/'},
                            {'text': 'New Homes', 'href': '/for_sale/95073_zip/new_homes_lt/'},
                            {'text': 'Recently Sold', 'href': '/sold/95073_zip/'}
                        ]
                    },
                    {'text': 'See Newest Listings', 'href': '/new-for-sale-properties/CA/'}
                ]
            },
            {
                'sectionHeaderText': 'Rent',
                'href': '/for_rent/95073_zip/',
                'dropDown': [
                    {
                        'dropDownHeaderText': '95073',
                        'dropDownList': [
                            {'text': 'All Rentals', 'href': '/for_rent/95073_zip/'},
                            {'text': 'Apartments for Rent',
                             'href': '/for_rent/95073_zip/APARTMENT,APARTMENT_COMMUNITY,APARTMENT%7CCONDO%7CTOWNHOUSE,CONDO,COOP,LOFT,TIC_type/'},
                            {'text': 'Houses for Rent', 'href': '/for_rent/95073_zip/SINGLE-FAMILY_HOME_type/'},
                            {'text': 'Rooms for Rent', 'href': '/for_rent/95073_zip/ROOM_FOR_RENT_type/'}
                        ]
                    },
                    {'text': 'See Newest Listings', 'href': '/new-for-rent-properties/CA/'},
                    {'text': 'Post A Rental Listing', 'href': '/post-rental/'}
                ]
            },
            {
                'sectionHeaderText': 'Mortgage',
                'href': '/mortgages/',
                'dropDown': [
                    {
                        'dropDownHeaderText': 'Soquel',
                        'dropDownList': [
                            {'text': 'Mortgage Overview', 'href': '/mortgages/'},
                            {'text': 'Get Pre-Qualified',
                             'href': '/mortgages/pre-approval?omni_src=mortgage%7Cglobal_nav'},
                            {'text': 'Mortgage Rates', 'href': '/mortgage-rates/Soquel,CA/'},
                            {'text': 'Refinance Rates', 'href': '/refinance-rates/Soquel,CA/'}
                        ]
                    },
                    {'text': 'Mortgage Calculator', 'href': '/mortgage-payment-calculator/'},
                    {'text': 'Affordability Calculator', 'href': '/house-affordability-calculator/'},
                    {'text': 'Rent vs Buy Calculator', 'href': '/rent_vs_buy/'},
                    {'text': 'Refinance Calculator', 'href': '/home-loan-refinance-calculator/'},
                ]
            },
        ]
    ),
    (
        'https://www.trulia.com/for_sale/New_York,NY/resale_lt/',
        [
            {
                'sectionHeaderText': 'Buy',
                'href': '/NY/New_York/',
                'dropDown': [
                    {
                        'dropDownHeaderText': 'New York City',
                        'dropDownList': [
                            {'text': 'Homes for Sale', 'href': '/NY/New_York/'},
                            {'text': 'Open Houses', 'href': '/for_sale/New_York,NY/p_oh/'},
                            {'text': 'New Homes', 'href': '/for_sale/New_York,NY/new_homes_lt/'},
                            {'text': 'Recently Sold', 'href': '/sold/New_York,NY/'}
                        ]
                    },
                    {'text': 'See Newest Listings', 'href': '/new-for-sale-properties/NY/'}
                ]
            },
            {
                'sectionHeaderText': 'Rent',
                'href': '/for_rent/New_York,NY/',
                'dropDown': [
                    {
                        'dropDownHeaderText': 'New York City',
                        'dropDownList': [
                            {'text': 'All Rentals', 'href': '/for_rent/New_York,NY/'},
                            {'text': 'Apartments for Rent',
                             'href': '/for_rent/New_York,NY/APARTMENT,APARTMENT_COMMUNITY,APARTMENT%7CCONDO%7CTOWNHOUSE,CONDO,COOP,LOFT,TIC_type/'},
                            {'text': 'Houses for Rent', 'href': '/for_rent/New_York,NY/SINGLE-FAMILY_HOME_type/'},
                            {'text': 'Rooms for Rent', 'href': '/for_rent/New_York,NY/ROOM_FOR_RENT_type/'}
                        ]
                    },
                    {'text': 'See Newest Listings', 'href': '/new-for-rent-properties/NY/'},
                    {'text': 'Post A Rental Listing', 'href': '/post-rental/'}
                ]
            },
            {
                'sectionHeaderText': 'Mortgage',
                'href': '/mortgages/',
                'dropDown': [
                    {
                        'dropDownHeaderText': 'New York City',
                        'dropDownList': [
                            {'text': 'Mortgage Overview', 'href': '/mortgages/'},
                            {'text': 'Get Pre-Qualified',
                             'href': '/mortgages/pre-approval?omni_src=mortgage%7Cglobal_nav'},
                            {'text': 'Mortgage Rates', 'href': '/mortgage-rates/New_York,NY/'},
                            {'text': 'Refinance Rates', 'href': '/refinance-rates/New_York,NY/'}
                        ]
                    },
                    {'text': 'Mortgage Calculator', 'href': '/mortgage-payment-calculator/'},
                    {'text': 'Affordability Calculator', 'href': '/house-affordability-calculator/'},
                    {'text': 'Rent vs Buy Calculator', 'href': '/rent_vs_buy/'},
                    {'text': 'Refinance Calculator', 'href': '/home-loan-refinance-calculator/'},
                ]
            },

        ]
    ),
    (
        'https://www.trulia.com/for_sale/New_York,NY/foreclosure,fsbo,new_homes,resale,sale_pending_lt/',
        [
            {
                'sectionHeaderText': 'Buy',
                'href': '/NY/New_York/',
                'dropDown': [
                    {
                        'dropDownHeaderText': 'New York City',
                        'dropDownList': [
                            {'text': 'Homes for Sale', 'href': '/NY/New_York/'},
                            {'text': 'Open Houses', 'href': '/for_sale/New_York,NY/p_oh/'},
                            {'text': 'New Homes', 'href': '/for_sale/New_York,NY/new_homes_lt/'},
                            {'text': 'Recently Sold', 'href': '/sold/New_York,NY/'}
                        ]
                    },
                    {'text': 'See Newest Listings', 'href': '/new-for-sale-properties/NY/'}
                ]
            },
            {
                'sectionHeaderText': 'Rent',
                'href': '/for_rent/New_York,NY/',
                'dropDown': [
                    {
                        'dropDownHeaderText': 'New York City',
                        'dropDownList': [
                            {'text': 'All Rentals', 'href': '/for_rent/New_York,NY/'},
                            {'text': 'Apartments for Rent',
                             'href': '/for_rent/New_York,NY/APARTMENT,APARTMENT_COMMUNITY,APARTMENT%7CCONDO%7CTOWNHOUSE,CONDO,COOP,LOFT,TIC_type/'},
                            {'text': 'Houses for Rent', 'href': '/for_rent/New_York,NY/SINGLE-FAMILY_HOME_type/'},
                            {'text': 'Rooms for Rent', 'href': '/for_rent/New_York,NY/ROOM_FOR_RENT_type/'}
                        ]
                    },
                    {'text': 'See Newest Listings', 'href': '/new-for-rent-properties/NY/'},
                    {'text': 'Post A Rental Listing', 'href': '/post-rental/'}
                ]
            },
            {
                'sectionHeaderText': 'Mortgage',
                'href': '/mortgages/',
                'dropDown': [
                    {
                        'dropDownHeaderText': 'New York City',
                        'dropDownList': [
                            {'text': 'Mortgage Overview', 'href': '/mortgages/'},
                            {'text': 'Get Pre-Qualified',
                             'href': '/mortgages/pre-approval?omni_src=mortgage%7Cglobal_nav'},
                            {'text': 'Mortgage Rates', 'href': '/mortgage-rates/New_York,NY/'},
                            {'text': 'Refinance Rates', 'href': '/refinance-rates/New_York,NY/'}
                        ]
                    },
                    {'text': 'Mortgage Calculator', 'href': '/mortgage-payment-calculator/'},
                    {'text': 'Affordability Calculator', 'href': '/house-affordability-calculator/'},
                    {'text': 'Rent vs Buy Calculator', 'href': '/rent_vs_buy/'},
                    {'text': 'Refinance Calculator', 'href': '/home-loan-refinance-calculator/'},
                ]
            },

        ]
    ),
    (
        'https://www.trulia.com/for_sale/San_Mateo,CA/37.514857406994,37.580526177666,-122.35212773203,-122.27316349863_xy/14_zm/',
        [
            {
                'sectionHeaderText': 'Buy',
                'href': '/CA/San_Mateo/',
                'dropDown': [
                    {
                        'dropDownHeaderText': 'San Mateo',
                        'dropDownList': [
                            {'text': 'Homes for Sale', 'href': '/CA/San_Mateo/'},
                            {'text': 'Open Houses', 'href': '/for_sale/San_Mateo,CA/p_oh/'},
                            {'text': 'New Homes', 'href': '/for_sale/San_Mateo,CA/new_homes_lt/'},
                            {'text': 'Recently Sold', 'href': '/sold/San_Mateo,CA/'}
                        ]
                    },
                    {'text': 'See Newest Listings', 'href': '/new-for-sale-properties/CA/'}
                ]
            },
            {
                'sectionHeaderText': 'Rent',
                'href': '/for_rent/San_Mateo,CA/',
                'dropDown': [
                    {
                        'dropDownHeaderText': 'San Mateo',
                        'dropDownList': [
                            {'text': 'All Rentals', 'href': '/for_rent/San_Mateo,CA/'},
                            {'text': 'Apartments for Rent',
                             'href': '/for_rent/San_Mateo,CA/APARTMENT,APARTMENT_COMMUNITY,APARTMENT%7CCONDO%7CTOWNHOUSE,CONDO,COOP,LOFT,TIC_type/'},
                            {'text': 'Houses for Rent', 'href': '/for_rent/San_Mateo,CA/SINGLE-FAMILY_HOME_type/'},
                            {'text': 'Rooms for Rent', 'href': '/for_rent/San_Mateo,CA/ROOM_FOR_RENT_type/'}
                        ]
                    },
                    {'text': 'See Newest Listings', 'href': '/new-for-rent-properties/CA/'},
                    {'text': 'Post A Rental Listing', 'href': '/post-rental/'}
                ]
            },
            {
                'sectionHeaderText': 'Mortgage',
                'href': '/mortgages/',
                'dropDown': [
                    {
                        'dropDownHeaderText': 'San Mateo',
                        'dropDownList': [
                            {'text': 'Mortgage Overview', 'href': '/mortgages/'},
                            {'text': 'Get Pre-Qualified',
                             'href': '/mortgages/pre-approval?omni_src=mortgage%7Cglobal_nav'},
                            {'text': 'Mortgage Rates', 'href': '/mortgage-rates/San_Mateo,CA/'},
                            {'text': 'Refinance Rates', 'href': '/refinance-rates/San_Mateo,CA/'}
                        ]
                    },
                    {'text': 'Mortgage Calculator', 'href': '/mortgage-payment-calculator/'},
                    {'text': 'Affordability Calculator', 'href': '/house-affordability-calculator/'},
                    {'text': 'Rent vs Buy Calculator', 'href': '/rent_vs_buy/'},
                    {'text': 'Refinance Calculator', 'href': '/home-loan-refinance-calculator/'},
                ]
            },

        ]
    ),
    (
        'https://www.trulia.com/NY/New_York/',
        [
            {
                'sectionHeaderText': 'Buy',
                'href': '/NY/New_York/',
                'dropDown': [
                    {
                        'dropDownHeaderText': 'New York City',
                        'dropDownList': [
                            {'text': 'Homes for Sale', 'href': '/NY/New_York/'},
                            {'text': 'Open Houses', 'href': '/for_sale/New_York,NY/p_oh/'},
                            {'text': 'New Homes', 'href': '/for_sale/New_York,NY/new_homes_lt/'},
                            {'text': 'Recently Sold', 'href': '/sold/New_York,NY/'}
                        ]
                    },
                    {'text': 'See Newest Listings', 'href': '/new-for-sale-properties/NY/'}
                ]
            },
            {
                'sectionHeaderText': 'Rent',
                'href': '/for_rent/New_York,NY/',
                'dropDown': [
                    {
                        'dropDownHeaderText': 'New York City',
                        'dropDownList': [
                            {'text': 'All Rentals', 'href': '/for_rent/New_York,NY/'},
                            {'text': 'Apartments for Rent',
                             'href': '/for_rent/New_York,NY/APARTMENT,APARTMENT_COMMUNITY,APARTMENT%7CCONDO%7CTOWNHOUSE,CONDO,COOP,LOFT,TIC_type/'},
                            {'text': 'Houses for Rent', 'href': '/for_rent/New_York,NY/SINGLE-FAMILY_HOME_type/'},
                            {'text': 'Rooms for Rent', 'href': '/for_rent/New_York,NY/ROOM_FOR_RENT_type/'}
                        ]
                    },
                    {'text': 'See Newest Listings', 'href': '/new-for-rent-properties/NY/'},
                    {'text': 'Post A Rental Listing', 'href': '/post-rental/'}
                ]
            },
            {
                'sectionHeaderText': 'Mortgage',
                'href': '/mortgages/',
                'dropDown': [
                    {
                        'dropDownHeaderText': 'New York City',
                        'dropDownList': [
                            {'text': 'Mortgage Overview', 'href': '/mortgages/'},
                            {'text': 'Get Pre-Qualified',
                             'href': '/mortgages/pre-approval?omni_src=mortgage%7Cglobal_nav'},
                            {'text': 'Mortgage Rates', 'href': '/mortgage-rates/New_York,NY/'},
                            {'text': 'Refinance Rates', 'href': '/refinance-rates/New_York,NY/'}
                        ]
                    },
                    {'text': 'Mortgage Calculator', 'href': '/mortgage-payment-calculator/'},
                    {'text': 'Affordability Calculator', 'href': '/house-affordability-calculator/'},
                    {'text': 'Rent vs Buy Calculator', 'href': '/rent_vs_buy/'},
                    {'text': 'Refinance Calculator', 'href': '/home-loan-refinance-calculator/'},
                ]
            },

        ]
    ),
    (
        'https://www.trulia.com/for_sale/94546_zip/new_homes_lt/',
        [
            {
                'sectionHeaderText': 'Buy',
                'href': '/CA/Castro_Valley/94546/',
                'dropDown': [
                    {
                        'dropDownHeaderText': '94546',
                        'dropDownList': [
                            {'text': 'Homes for Sale', 'href': '/CA/Castro_Valley/94546/'},
                            {'text': 'Open Houses', 'href': '/for_sale/94546_zip/p_oh/'},
                            {'text': 'New Homes', 'href': '/for_sale/94546_zip/new_homes_lt/'},
                            {'text': 'Recently Sold', 'href': '/sold/94546_zip/'}
                        ]
                    },
                    {
                        'dropDownHeaderText': 'Castro Valley',
                        'dropDownList': [
                            {'text': 'Homes for Sale', 'href': '/CA/Castro_Valley/'},
                            {'text': 'Open Houses', 'href': '/for_sale/Castro_Valley,CA/p_oh/'},
                            {'text': 'Recently Sold', 'href': '/sold/Castro_Valley,CA/'}
                        ]
                    },
                    {'text': 'See Newest Listings', 'href': '/new-for-sale-properties/CA/'}
                ]
            },
            {
                'sectionHeaderText': 'Rent',
                'href': '/for_rent/94546_zip/',
                'dropDown': [
                    {
                        'dropDownHeaderText': '94546',
                        'dropDownList': [
                            {'text': 'All Rentals', 'href': '/for_rent/94546_zip/'},
                            {'text': 'Apartments for Rent',
                             'href': '/for_rent/94546_zip/APARTMENT,APARTMENT_COMMUNITY,APARTMENT%7CCONDO%7CTOWNHOUSE,CONDO,COOP,LOFT,TIC_type/'},
                            {'text': 'Houses for Rent', 'href': '/for_rent/94546_zip/SINGLE-FAMILY_HOME_type/'},
                            {'text': 'Rooms for Rent', 'href': '/for_rent/94546_zip/ROOM_FOR_RENT_type/'}
                        ]
                    },
                    {
                        'dropDownHeaderText': 'Castro Valley',
                        'dropDownList': [
                            {'text': 'All Rentals', 'href': '/for_rent/Castro_Valley,CA/'},
                            {'text': 'Houses for Rent', 'href': '/for_rent/Castro_Valley,CA/SINGLE-FAMILY_HOME_type/'},
                            {'text': 'Rooms for Rent', 'href': '/for_rent/Castro_Valley,CA/ROOM_FOR_RENT_type/'}
                        ]
                    },
                    {'text': 'See Newest Listings', 'href': '/new-for-rent-properties/CA/'},
                    {'text': 'Post A Rental Listing', 'href': '/post-rental/'}
                ]
            },
            {
                'sectionHeaderText': 'Mortgage',
                'href': '/mortgages/',
                'dropDown': [
                    {
                        'dropDownHeaderText': 'Castro Valley',
                        'dropDownList': [
                            {'text': 'Mortgage Overview', 'href': '/mortgages/'},
                            {'text': 'Get Pre-Qualified',
                             'href': '/mortgages/pre-approval?omni_src=mortgage%7Cglobal_nav'},
                            {'text': 'Mortgage Rates', 'href': '/mortgage-rates/Castro_Valley,CA/'},
                            {'text': 'Refinance Rates', 'href': '/refinance-rates/Castro_Valley,CA/'}
                        ]
                    },
                    {'text': 'Mortgage Calculator', 'href': '/mortgage-payment-calculator/'},
                    {'text': 'Affordability Calculator', 'href': '/house-affordability-calculator/'},
                    {'text': 'Rent vs Buy Calculator', 'href': '/rent_vs_buy/'},
                    {'text': 'Refinance Calculator', 'href': '/home-loan-refinance-calculator/'},
                ]
            },

        ]
    ),
    (
        'https://www.trulia.com/for_sale/1468_nh/SINGLE-FAMILY_HOME_type/',
        [
            {
                'sectionHeaderText': 'Buy',
                'href': '/CA/San_Francisco,1468,Tenderloin/',
                'dropDown': [
                    {
                        'dropDownHeaderText': 'Tenderloin',
                        'dropDownList': [
                            {'text': 'Homes for Sale', 'href': '/CA/San_Francisco,1468,Tenderloin/'},
                            {'text': 'Open Houses', 'href': '/for_sale/1468_nh/p_oh/'},
                            {'text': 'New Homes', 'href': '/for_sale/1468_nh/new_homes_lt/'},
                            {'text': 'Recently Sold', 'href': '/sold/1468_nh/'}
                        ]
                    },
                    {
                        'dropDownHeaderText': 'San Francisco',
                        'dropDownList': [
                            {'text': 'Homes for Sale', 'href': '/CA/San_Francisco/'},
                            {'text': 'Open Houses', 'href': '/for_sale/San_Francisco,CA/p_oh/'},
                            {'text': 'Recently Sold', 'href': '/sold/San_Francisco,CA/'}
                        ]
                    },
                    {'text': 'See Newest Listings', 'href': '/new-for-sale-properties/CA/'}
                ]
            },
            {
                'sectionHeaderText': 'Rent',
                'href': '/for_rent/1468_nh/',
                'dropDown': [
                    {
                        'dropDownHeaderText': 'Tenderloin',
                        'dropDownList': [
                            {'text': 'All Rentals', 'href': '/for_rent/1468_nh/'},
                            {'text': 'Apartments for Rent',
                             'href': '/for_rent/1468_nh/APARTMENT,APARTMENT_COMMUNITY,APARTMENT%7CCONDO%7CTOWNHOUSE,CONDO,COOP,LOFT,TIC_type/'},
                            {'text': 'Houses for Rent', 'href': '/for_rent/1468_nh/SINGLE-FAMILY_HOME_type/'},
                            {'text': 'Rooms for Rent', 'href': '/for_rent/1468_nh/ROOM_FOR_RENT_type/'}
                        ]
                    },
                    {
                        'dropDownHeaderText': 'San Francisco',
                        'dropDownList': [
                            {'text': 'All Rentals', 'href': '/for_rent/San_Francisco,CA/'},
                            {'text': 'Houses for Rent', 'href': '/for_rent/San_Francisco,CA/SINGLE-FAMILY_HOME_type/'},
                            {'text': 'Rooms for Rent', 'href': '/for_rent/San_Francisco,CA/ROOM_FOR_RENT_type/'}
                        ]
                    },
                    {'text': 'See Newest Listings', 'href': '/new-for-rent-properties/CA/'},
                    {'text': 'Post A Rental Listing', 'href': '/post-rental/'}
                ]
            },
            {
                'sectionHeaderText': 'Mortgage',
                'href': '/mortgages/',
                'dropDown': [
                    {
                        'dropDownHeaderText': 'San Francisco',
                        'dropDownList': [
                            {'text': 'Mortgage Overview', 'href': '/mortgages/'},
                            {'text': 'Get Pre-Qualified',
                             'href': '/mortgages/pre-approval?omni_src=mortgage%7Cglobal_nav'},
                            {'text': 'Mortgage Rates', 'href': '/mortgage-rates/San_Francisco,CA/'},
                            {'text': 'Refinance Rates', 'href': '/refinance-rates/San_Francisco,CA/'}
                        ]
                    },
                    {'text': 'Mortgage Calculator', 'href': '/mortgage-payment-calculator/'},
                    {'text': 'Affordability Calculator', 'href': '/house-affordability-calculator/'},
                    {'text': 'Rent vs Buy Calculator', 'href': '/rent_vs_buy/'},
                    {'text': 'Refinance Calculator', 'href': '/home-loan-refinance-calculator/'},
                ]
            },
        ]
    ),
    (
        'https://www.trulia.com/for_sale/1468_nh/p_oh/',
        [
            {
                'sectionHeaderText': 'Buy',
                'href': '/CA/San_Francisco,1468,Tenderloin/',
                'dropDown': [
                    {
                        'dropDownHeaderText': 'Tenderloin',
                        'dropDownList': [
                            {'text': 'Homes for Sale', 'href': '/CA/San_Francisco,1468,Tenderloin/'},
                            {'text': 'Open Houses', 'href': '/for_sale/1468_nh/p_oh/'},
                            {'text': 'New Homes', 'href': '/for_sale/1468_nh/new_homes_lt/'},
                            {'text': 'Recently Sold', 'href': '/sold/1468_nh/'}
                        ]
                    },
                    {
                        'dropDownHeaderText': 'San Francisco',
                        'dropDownList': [
                            {'text': 'Homes for Sale', 'href': '/CA/San_Francisco/'},
                            {'text': 'Open Houses', 'href': '/for_sale/San_Francisco,CA/p_oh/'},
                            {'text': 'Recently Sold', 'href': '/sold/San_Francisco,CA/'}
                        ]
                    },
                    {'text': 'See Newest Listings', 'href': '/new-for-sale-properties/CA/'}
                ]
            },
            {
                'sectionHeaderText': 'Rent',
                'href': '/for_rent/1468_nh/',
                'dropDown': [
                    {
                        'dropDownHeaderText': 'Tenderloin',
                        'dropDownList': [
                            {'text': 'All Rentals', 'href': '/for_rent/1468_nh/'},
                            {'text': 'Apartments for Rent',
                             'href': '/for_rent/1468_nh/APARTMENT,APARTMENT_COMMUNITY,APARTMENT%7CCONDO%7CTOWNHOUSE,CONDO,COOP,LOFT,TIC_type/'},
                            {'text': 'Houses for Rent', 'href': '/for_rent/1468_nh/SINGLE-FAMILY_HOME_type/'},
                            {'text': 'Rooms for Rent', 'href': '/for_rent/1468_nh/ROOM_FOR_RENT_type/'}
                        ]
                    },
                    {
                        'dropDownHeaderText': 'San Francisco',
                        'dropDownList': [
                            {'text': 'All Rentals', 'href': '/for_rent/San_Francisco,CA/'},
                            {'text': 'Houses for Rent', 'href': '/for_rent/San_Francisco,CA/SINGLE-FAMILY_HOME_type/'},
                            {'text': 'Rooms for Rent', 'href': '/for_rent/San_Francisco,CA/ROOM_FOR_RENT_type/'}
                        ]
                    },
                    {'text': 'See Newest Listings', 'href': '/new-for-rent-properties/CA/'},
                    {'text': 'Post A Rental Listing', 'href': '/post-rental/'}
                ]
            },
            {
                'sectionHeaderText': 'Mortgage',
                'href': '/mortgages/',
                'dropDown': [
                    {
                        'dropDownHeaderText': 'San Francisco',
                        'dropDownList': [
                            {'text': 'Mortgage Overview', 'href': '/mortgages/'},
                            {'text': 'Get Pre-Qualified',
                             'href': '/mortgages/pre-approval?omni_src=mortgage%7Cglobal_nav'},
                            {'text': 'Mortgage Rates', 'href': '/mortgage-rates/San_Francisco,CA/'},
                            {'text': 'Refinance Rates', 'href': '/refinance-rates/San_Francisco,CA/'}
                        ]
                    },
                    {'text': 'Mortgage Calculator', 'href': '/mortgage-payment-calculator/'},
                    {'text': 'Affordability Calculator', 'href': '/house-affordability-calculator/'},
                    {'text': 'Rent vs Buy Calculator', 'href': '/rent_vs_buy/'},
                    {'text': 'Refinance Calculator', 'href': '/home-loan-refinance-calculator/'},
                ]
            },
        ]
    ),
    (
        'https://www.trulia.com/for_sale/1468_nh/fsbo_lt/',
        [
            {
                'sectionHeaderText': 'Buy',
                'href': '/CA/San_Francisco,1468,Tenderloin/',
                'dropDown': [
                    {
                        'dropDownHeaderText': 'Tenderloin',
                        'dropDownList': [
                            {'text': 'Homes for Sale', 'href': '/CA/San_Francisco,1468,Tenderloin/'},
                            {'text': 'Open Houses', 'href': '/for_sale/1468_nh/p_oh/'},
                            {'text': 'New Homes', 'href': '/for_sale/1468_nh/new_homes_lt/'},
                            {'text': 'Recently Sold', 'href': '/sold/1468_nh/'}
                        ]
                    },
                    {
                        'dropDownHeaderText': 'San Francisco',
                        'dropDownList': [
                            {'text': 'Homes for Sale', 'href': '/CA/San_Francisco/'},
                            {'text': 'Open Houses', 'href': '/for_sale/San_Francisco,CA/p_oh/'},
                            {'text': 'Recently Sold', 'href': '/sold/San_Francisco,CA/'}
                        ]
                    },
                    {'text': 'See Newest Listings', 'href': '/new-for-sale-properties/CA/'}
                ]
            },
            {
                'sectionHeaderText': 'Rent',
                'href': '/for_rent/1468_nh/',
                'dropDown': [
                    {
                        'dropDownHeaderText': 'Tenderloin',
                        'dropDownList': [
                            {'text': 'All Rentals', 'href': '/for_rent/1468_nh/'},
                            {'text': 'Apartments for Rent',
                             'href': '/for_rent/1468_nh/APARTMENT,APARTMENT_COMMUNITY,APARTMENT%7CCONDO%7CTOWNHOUSE,CONDO,COOP,LOFT,TIC_type/'},
                            {'text': 'Houses for Rent', 'href': '/for_rent/1468_nh/SINGLE-FAMILY_HOME_type/'},
                            {'text': 'Rooms for Rent', 'href': '/for_rent/1468_nh/ROOM_FOR_RENT_type/'}
                        ]
                    },
                    {
                        'dropDownHeaderText': 'San Francisco',
                        'dropDownList': [
                            {'text': 'All Rentals', 'href': '/for_rent/San_Francisco,CA/'},
                            {'text': 'Houses for Rent', 'href': '/for_rent/San_Francisco,CA/SINGLE-FAMILY_HOME_type/'},
                            {'text': 'Rooms for Rent', 'href': '/for_rent/San_Francisco,CA/ROOM_FOR_RENT_type/'}
                        ]
                    },
                    {'text': 'See Newest Listings', 'href': '/new-for-rent-properties/CA/'},
                    {'text': 'Post A Rental Listing', 'href': '/post-rental/'}
                ]
            },
            {
                'sectionHeaderText': 'Mortgage',
                'href': '/mortgages/',
                'dropDown': [
                    {
                        'dropDownHeaderText': 'San Francisco',
                        'dropDownList': [
                            {'text': 'Mortgage Overview', 'href': '/mortgages/'},
                            {'text': 'Get Pre-Qualified',
                             'href': '/mortgages/pre-approval?omni_src=mortgage%7Cglobal_nav'},
                            {'text': 'Mortgage Rates', 'href': '/mortgage-rates/San_Francisco,CA/'},
                            {'text': 'Refinance Rates', 'href': '/refinance-rates/San_Francisco,CA/'}
                        ]
                    },
                    {'text': 'Mortgage Calculator', 'href': '/mortgage-payment-calculator/'},
                    {'text': 'Affordability Calculator', 'href': '/house-affordability-calculator/'},
                    {'text': 'Rent vs Buy Calculator', 'href': '/rent_vs_buy/'},
                    {'text': 'Refinance Calculator', 'href': '/home-loan-refinance-calculator/'},
                ]
            },
        ]
    ),
    (
        'https://www.trulia.com/for_sale/1468_nh/foreclosure_lt/',
        [
            {
                'sectionHeaderText': 'Buy',
                'href': '/CA/San_Francisco,1468,Tenderloin/',
                'dropDown': [
                    {
                        'dropDownHeaderText': 'Tenderloin',
                        'dropDownList': [
                            {'text': 'Homes for Sale', 'href': '/CA/San_Francisco,1468,Tenderloin/'},
                            {'text': 'Open Houses', 'href': '/for_sale/1468_nh/p_oh/'},
                            {'text': 'New Homes', 'href': '/for_sale/1468_nh/new_homes_lt/'},
                            {'text': 'Recently Sold', 'href': '/sold/1468_nh/'}
                        ]
                    },
                    {
                        'dropDownHeaderText': 'San Francisco',
                        'dropDownList': [
                            {'text': 'Homes for Sale', 'href': '/CA/San_Francisco/'},
                            {'text': 'Open Houses', 'href': '/for_sale/San_Francisco,CA/p_oh/'},
                            {'text': 'Recently Sold', 'href': '/sold/San_Francisco,CA/'}
                        ]
                    },
                    {'text': 'See Newest Listings', 'href': '/new-for-sale-properties/CA/'}
                ]
            },
            {
                'sectionHeaderText': 'Rent',
                'href': '/for_rent/1468_nh/',
                'dropDown': [
                    {
                        'dropDownHeaderText': 'Tenderloin',
                        'dropDownList': [
                            {'text': 'All Rentals', 'href': '/for_rent/1468_nh/'},
                            {'text': 'Apartments for Rent',
                             'href': '/for_rent/1468_nh/APARTMENT,APARTMENT_COMMUNITY,APARTMENT%7CCONDO%7CTOWNHOUSE,CONDO,COOP,LOFT,TIC_type/'},
                            {'text': 'Houses for Rent', 'href': '/for_rent/1468_nh/SINGLE-FAMILY_HOME_type/'},
                            {'text': 'Rooms for Rent', 'href': '/for_rent/1468_nh/ROOM_FOR_RENT_type/'}
                        ]
                    },
                    {
                        'dropDownHeaderText': 'San Francisco',
                        'dropDownList': [
                            {'text': 'All Rentals', 'href': '/for_rent/San_Francisco,CA/'},
                            {'text': 'Houses for Rent', 'href': '/for_rent/San_Francisco,CA/SINGLE-FAMILY_HOME_type/'},
                            {'text': 'Rooms for Rent', 'href': '/for_rent/San_Francisco,CA/ROOM_FOR_RENT_type/'}
                        ]
                    },
                    {'text': 'See Newest Listings', 'href': '/new-for-rent-properties/CA/'},
                    {'text': 'Post A Rental Listing', 'href': '/post-rental/'}
                ]
            },
            {
                'sectionHeaderText': 'Mortgage',
                'href': '/mortgages/',
                'dropDown': [
                    {
                        'dropDownHeaderText': 'San Francisco',
                        'dropDownList': [
                            {'text': 'Mortgage Overview', 'href': '/mortgages/'},
                            {'text': 'Get Pre-Qualified',
                             'href': '/mortgages/pre-approval?omni_src=mortgage%7Cglobal_nav'},
                            {'text': 'Mortgage Rates', 'href': '/mortgage-rates/San_Francisco,CA/'},
                            {'text': 'Refinance Rates', 'href': '/refinance-rates/San_Francisco,CA/'}
                        ]
                    },
                    {'text': 'Mortgage Calculator', 'href': '/mortgage-payment-calculator/'},
                    {'text': 'Affordability Calculator', 'href': '/house-affordability-calculator/'},
                    {'text': 'Rent vs Buy Calculator', 'href': '/rent_vs_buy/'},
                    {'text': 'Refinance Calculator', 'href': '/home-loan-refinance-calculator/'},
                ]
            },
        ]
    ),
    (
        'https://www.trulia.com/for_sale/94546_zip/fsbo_lt/',
        [
            {
                'sectionHeaderText': 'Buy',
                'href': '/CA/Castro_Valley/94546/',
                'dropDown': [
                    {
                        'dropDownHeaderText': '94546',
                        'dropDownList': [
                            {'text': 'Homes for Sale', 'href': '/CA/Castro_Valley/94546/'},
                            {'text': 'Open Houses', 'href': '/for_sale/94546_zip/p_oh/'},
                            {'text': 'New Homes', 'href': '/for_sale/94546_zip/new_homes_lt/'},
                            {'text': 'Recently Sold', 'href': '/sold/94546_zip/'}
                        ]
                    },
                    {
                        'dropDownHeaderText': 'Castro Valley',
                        'dropDownList': [
                            {'text': 'Homes for Sale', 'href': '/CA/Castro_Valley/'},
                            {'text': 'Open Houses', 'href': '/for_sale/Castro_Valley,CA/p_oh/'},
                            {'text': 'Recently Sold', 'href': '/sold/Castro_Valley,CA/'}
                        ]
                    },
                    {'text': 'See Newest Listings', 'href': '/new-for-sale-properties/CA/'}
                ]
            },
            {
                'sectionHeaderText': 'Rent',
                'href': '/for_rent/94546_zip/',
                'dropDown': [
                    {
                        'dropDownHeaderText': '94546',
                        'dropDownList': [
                            {'text': 'All Rentals', 'href': '/for_rent/94546_zip/'},
                            {'text': 'Apartments for Rent',
                             'href': '/for_rent/94546_zip/APARTMENT,APARTMENT_COMMUNITY,APARTMENT%7CCONDO%7CTOWNHOUSE,CONDO,COOP,LOFT,TIC_type/'},
                            {'text': 'Houses for Rent', 'href': '/for_rent/94546_zip/SINGLE-FAMILY_HOME_type/'},
                            {'text': 'Rooms for Rent', 'href': '/for_rent/94546_zip/ROOM_FOR_RENT_type/'}
                        ]
                    },
                    {
                        'dropDownHeaderText': 'Castro Valley',
                        'dropDownList': [
                            {'text': 'All Rentals', 'href': '/for_rent/Castro_Valley,CA/'},
                            {'text': 'Houses for Rent', 'href': '/for_rent/Castro_Valley,CA/SINGLE-FAMILY_HOME_type/'},
                            {'text': 'Rooms for Rent', 'href': '/for_rent/Castro_Valley,CA/ROOM_FOR_RENT_type/'}
                        ]
                    },
                    {'text': 'See Newest Listings', 'href': '/new-for-rent-properties/CA/'},
                    {'text': 'Post A Rental Listing', 'href': '/post-rental/'}
                ]
            },
            {
                'sectionHeaderText': 'Mortgage',
                'href': '/mortgages/',
                'dropDown': [
                    {
                        'dropDownHeaderText': 'Castro Valley',
                        'dropDownList': [
                            {'text': 'Mortgage Overview', 'href': '/mortgages/'},
                            {'text': 'Get Pre-Qualified',
                             'href': '/mortgages/pre-approval?omni_src=mortgage%7Cglobal_nav'},
                            {'text': 'Mortgage Rates', 'href': '/mortgage-rates/Castro_Valley,CA/'},
                            {'text': 'Refinance Rates', 'href': '/refinance-rates/Castro_Valley,CA/'}
                        ]
                    },
                    {'text': 'Mortgage Calculator', 'href': '/mortgage-payment-calculator/'},
                    {'text': 'Affordability Calculator', 'href': '/house-affordability-calculator/'},
                    {'text': 'Rent vs Buy Calculator', 'href': '/rent_vs_buy/'},
                    {'text': 'Refinance Calculator', 'href': '/home-loan-refinance-calculator/'},
                ]
            },

        ]
    ),
    (
        'https://www.trulia.com/for_sale/San_Francisco,CA/english-cottages/',
        [
            {
                'sectionHeaderText': 'Buy',
                'href': '/CA/San_Francisco/',
                'dropDown': [
                    {
                        'dropDownHeaderText': 'San Francisco',
                        'dropDownList': [
                            {'text': 'Homes for Sale', 'href': '/CA/San_Francisco/'},
                            {'text': 'Open Houses', 'href': '/for_sale/San_Francisco,CA/p_oh/'},
                            {'text': 'New Homes', 'href': '/for_sale/San_Francisco,CA/new_homes_lt/'},
                            {'text': 'Recently Sold', 'href': '/sold/San_Francisco,CA/'}
                        ]
                    },
                    {'text': 'See Newest Listings', 'href': '/new-for-sale-properties/CA/'}
                ]
            },
            {
                'sectionHeaderText': 'Rent',
                'href': '/for_rent/San_Francisco,CA/',
                'dropDown': [
                    {
                        'dropDownHeaderText': 'San Francisco',
                        'dropDownList': [
                            {'text': 'All Rentals', 'href': '/for_rent/San_Francisco,CA/'},
                            {'text': 'Apartments for Rent',
                             'href': '/for_rent/San_Francisco,CA/APARTMENT,APARTMENT_COMMUNITY,APARTMENT%7CCONDO%7CTOWNHOUSE,CONDO,COOP,LOFT,TIC_type/'},
                            {'text': 'Houses for Rent', 'href': '/for_rent/San_Francisco,CA/SINGLE-FAMILY_HOME_type/'},
                            {'text': 'Rooms for Rent', 'href': '/for_rent/San_Francisco,CA/ROOM_FOR_RENT_type/'}
                        ]
                    },
                    {'text': 'See Newest Listings', 'href': '/new-for-rent-properties/CA/'},
                    {'text': 'Post A Rental Listing', 'href': '/post-rental/'}
                ]
            },
            {
                'sectionHeaderText': 'Mortgage',
                'href': '/mortgages/',
                'dropDown': [
                    {
                        'dropDownHeaderText': 'San Francisco',
                        'dropDownList': [
                            {'text': 'Mortgage Overview', 'href': '/mortgages/'},
                            {'text': 'Get Pre-Qualified',
                             'href': '/mortgages/pre-approval?omni_src=mortgage%7Cglobal_nav'},
                            {'text': 'Mortgage Rates', 'href': '/mortgage-rates/San_Francisco,CA/'},
                            {'text': 'Refinance Rates', 'href': '/refinance-rates/San_Francisco,CA/'}
                        ]
                    },
                    {'text': 'Mortgage Calculator', 'href': '/mortgage-payment-calculator/'},
                    {'text': 'Affordability Calculator', 'href': '/house-affordability-calculator/'},
                    {'text': 'Rent vs Buy Calculator', 'href': '/rent_vs_buy/'},
                    {'text': 'Refinance Calculator', 'href': '/home-loan-refinance-calculator/'},
                ]
            },
        ]
    ),
    (
        'https://www.trulia.com/for_sale/1468_nh/p_oh/',
        [
            {
                'sectionHeaderText': 'Buy',
                'href': '/CA/San_Francisco,1468,Tenderloin/',
                'dropDown': [
                    {
                        'dropDownHeaderText': 'Tenderloin',
                        'dropDownList': [
                            {'text': 'Homes for Sale', 'href': '/CA/San_Francisco,1468,Tenderloin/'},
                            {'text': 'Open Houses', 'href': '/for_sale/1468_nh/p_oh/'},
                            {'text': 'New Homes', 'href': '/for_sale/1468_nh/new_homes_lt/'},
                            {'text': 'Recently Sold', 'href': '/sold/1468_nh/'}
                        ]
                    },
                    {
                        'dropDownHeaderText': 'San Francisco',
                        'dropDownList': [
                            {'text': 'Homes for Sale', 'href': '/CA/San_Francisco/'},
                            {'text': 'Open Houses', 'href': '/for_sale/San_Francisco,CA/p_oh/'},
                            {'text': 'Recently Sold', 'href': '/sold/San_Francisco,CA/'}
                        ]
                    },
                    {'text': 'See Newest Listings', 'href': '/new-for-sale-properties/CA/'}
                ]
            },
            {
                'sectionHeaderText': 'Rent',
                'href': '/for_rent/1468_nh/',
                'dropDown': [
                    {
                        'dropDownHeaderText': 'Tenderloin',
                        'dropDownList': [
                            {'text': 'All Rentals', 'href': '/for_rent/1468_nh/'},
                            {'text': 'Apartments for Rent',
                             'href': '/for_rent/1468_nh/APARTMENT,APARTMENT_COMMUNITY,APARTMENT%7CCONDO%7CTOWNHOUSE,CONDO,COOP,LOFT,TIC_type/'},
                            {'text': 'Houses for Rent', 'href': '/for_rent/1468_nh/SINGLE-FAMILY_HOME_type/'},
                            {'text': 'Rooms for Rent', 'href': '/for_rent/1468_nh/ROOM_FOR_RENT_type/'}
                        ]
                    },
                    {
                        'dropDownHeaderText': 'San Francisco',
                        'dropDownList': [
                            {'text': 'All Rentals', 'href': '/for_rent/San_Francisco,CA/'},
                            {'text': 'Houses for Rent', 'href': '/for_rent/San_Francisco,CA/SINGLE-FAMILY_HOME_type/'},
                            {'text': 'Rooms for Rent', 'href': '/for_rent/San_Francisco,CA/ROOM_FOR_RENT_type/'}
                        ]
                    },
                    {'text': 'See Newest Listings', 'href': '/new-for-rent-properties/CA/'},
                    {'text': 'Post A Rental Listing', 'href': '/post-rental/'}
                ]
            },
            {
                'sectionHeaderText': 'Mortgage',
                'href': '/mortgages/',
                'dropDown': [
                    {
                        'dropDownHeaderText': 'San Francisco',
                        'dropDownList': [
                            {'text': 'Mortgage Overview', 'href': '/mortgages/'},
                            {'text': 'Get Pre-Qualified',
                             'href': '/mortgages/pre-approval?omni_src=mortgage%7Cglobal_nav'},
                            {'text': 'Mortgage Rates', 'href': '/mortgage-rates/San_Francisco,CA/'},
                            {'text': 'Refinance Rates', 'href': '/refinance-rates/San_Francisco,CA/'}
                        ]
                    },
                    {'text': 'Mortgage Calculator', 'href': '/mortgage-payment-calculator/'},
                    {'text': 'Affordability Calculator', 'href': '/house-affordability-calculator/'},
                    {'text': 'Rent vs Buy Calculator', 'href': '/rent_vs_buy/'},
                    {'text': 'Refinance Calculator', 'href': '/home-loan-refinance-calculator/'},
                ]
            },
        ]
    ),
    (
        'https://www.trulia.com/for_sale/37.538050449811,38.04863829018,-122.31357480293,-121.68186093574_xy/SINGLE-FAMILY_HOME_type/11_zm/',
        [
            {
                'sectionHeaderText': 'Buy',
                'href': '/CA/San_Ramon/',
                'dropDown': [
                    {
                        'dropDownHeaderText': 'San Ramon',
                        'dropDownList': [
                            {'text': 'Homes for Sale', 'href': '/CA/San_Ramon/'},
                            {'text': 'Open Houses', 'href': '/for_sale/San_Ramon,CA/p_oh/'},
                            {'text': 'New Homes', 'href': '/for_sale/San_Ramon,CA/new_homes_lt/'},
                            {'text': 'Recently Sold', 'href': '/sold/San_Ramon,CA/'}
                        ]
                    },
                    {'text': 'See Newest Listings', 'href': '/new-for-sale-properties/CA/'}
                ]
            },
            {
                'sectionHeaderText': 'Rent',
                'href': '/for_rent/San_Ramon,CA/',
                'dropDown': [
                    {
                        'dropDownHeaderText': 'San Ramon',
                        'dropDownList': [
                            {'text': 'All Rentals', 'href': '/for_rent/San_Ramon,CA/'},
                            {'text': 'Apartments for Rent',
                             'href': '/for_rent/San_Ramon,CA/APARTMENT,APARTMENT_COMMUNITY,APARTMENT%7CCONDO%7CTOWNHOUSE,CONDO,COOP,LOFT,TIC_type/'},
                            {'text': 'Houses for Rent', 'href': '/for_rent/San_Ramon,CA/SINGLE-FAMILY_HOME_type/'},
                            {'text': 'Rooms for Rent', 'href': '/for_rent/San_Ramon,CA/ROOM_FOR_RENT_type/'}
                        ]
                    },
                    {'text': 'See Newest Listings', 'href': '/new-for-rent-properties/CA/'},
                    {'text': 'Post A Rental Listing', 'href': '/post-rental/'}
                ]
            },
            {
                'sectionHeaderText': 'Mortgage',
                'href': '/mortgages/',
                'dropDown': [
                    {
                        'dropDownHeaderText': 'San Ramon',
                        'dropDownList': [
                            {'text': 'Mortgage Overview', 'href': '/mortgages/'},
                            {'text': 'Get Pre-Qualified',
                             'href': '/mortgages/pre-approval?omni_src=mortgage%7Cglobal_nav'},
                            {'text': 'Mortgage Rates', 'href': '/mortgage-rates/San_Ramon,CA/'},
                            {'text': 'Refinance Rates', 'href': '/refinance-rates/San_Ramon,CA/'}
                        ]
                    },
                    {'text': 'Mortgage Calculator', 'href': '/mortgage-payment-calculator/'},
                    {'text': 'Affordability Calculator', 'href': '/house-affordability-calculator/'},
                    {'text': 'Rent vs Buy Calculator', 'href': '/rent_vs_buy/'},
                    {'text': 'Refinance Calculator', 'href': '/home-loan-refinance-calculator/'},
                ]
            },
        ]
    ),
    (
        'https://www.trulia.com/for_sale/36.861754825027,37.888660248935,-121.57103313202,-120.30760539765_xy/foreclosure_lt/10_zm/',
        [
            {
                'sectionHeaderText': 'Buy',
                'href': '/CA/Hilmar/',
                'dropDown': [
                    {
                        'dropDownHeaderText': 'Hilmar',
                        'dropDownList': [
                            {'text': 'Homes for Sale', 'href': '/CA/Hilmar/'},
                            {'text': 'Open Houses', 'href': '/for_sale/Hilmar,CA/p_oh/'},
                            {'text': 'New Homes', 'href': '/for_sale/Hilmar,CA/new_homes_lt/'},
                            {'text': 'Recently Sold', 'href': '/sold/Hilmar,CA/'}
                        ]
                    },
                    {'text': 'See Newest Listings', 'href': '/new-for-sale-properties/CA/'}
                ]
            },
            {
                'sectionHeaderText': 'Rent',
                'href': '/for_rent/Hilmar,CA/',
                'dropDown': [
                    {
                        'dropDownHeaderText': 'Hilmar',
                        'dropDownList': [
                            {'text': 'All Rentals', 'href': '/for_rent/Hilmar,CA/'},
                            {'text': 'Apartments for Rent',
                             'href': '/for_rent/Hilmar,CA/APARTMENT,APARTMENT_COMMUNITY,APARTMENT%7CCONDO%7CTOWNHOUSE,CONDO,COOP,LOFT,TIC_type/'},
                            {'text': 'Houses for Rent', 'href': '/for_rent/Hilmar,CA/SINGLE-FAMILY_HOME_type/'},
                            {'text': 'Rooms for Rent', 'href': '/for_rent/Hilmar,CA/ROOM_FOR_RENT_type/'}
                        ]
                    },
                    {'text': 'See Newest Listings', 'href': '/new-for-rent-properties/CA/'},
                    {'text': 'Post A Rental Listing', 'href': '/post-rental/'}
                ]
            },
            {
                'sectionHeaderText': 'Mortgage',
                'href': '/mortgages/',
                'dropDown': [
                    {
                        'dropDownHeaderText': 'Hilmar',
                        'dropDownList': [
                            {'text': 'Mortgage Overview', 'href': '/mortgages/'},
                            {'text': 'Get Pre-Qualified',
                             'href': '/mortgages/pre-approval?omni_src=mortgage%7Cglobal_nav'},
                            {'text': 'Mortgage Rates', 'href': '/mortgage-rates/Hilmar,CA/'},
                            {'text': 'Refinance Rates', 'href': '/refinance-rates/Hilmar,CA/'}
                        ]
                    },
                    {'text': 'Mortgage Calculator', 'href': '/mortgage-payment-calculator/'},
                    {'text': 'Affordability Calculator', 'href': '/house-affordability-calculator/'},
                    {'text': 'Rent vs Buy Calculator', 'href': '/rent_vs_buy/'},
                    {'text': 'Refinance Calculator', 'href': '/home-loan-refinance-calculator/'},
                ]
            },
        ]
    ),
    (
        'https://www.trulia.com/for_sale/37.684308412501,37.812034250769,-122.48558262954,-122.32765416274_xy/',
        [
            {
                'sectionHeaderText': 'Buy',
                'href': '/CA/San_Francisco/',
                'dropDown': [
                    {
                        'dropDownHeaderText': 'San Francisco',
                        'dropDownList': [
                            {'text': 'Homes for Sale', 'href': '/CA/San_Francisco/'},
                            {'text': 'Open Houses', 'href': '/for_sale/San_Francisco,CA/p_oh/'},
                            {'text': 'New Homes', 'href': '/for_sale/San_Francisco,CA/new_homes_lt/'},
                            {'text': 'Recently Sold', 'href': '/sold/San_Francisco,CA/'}
                        ]
                    },
                    {'text': 'See Newest Listings', 'href': '/new-for-sale-properties/CA/'}
                ]
            },
            {
                'sectionHeaderText': 'Rent',
                'href': '/for_rent/San_Francisco,CA/',
                'dropDown': [
                    {
                        'dropDownHeaderText': 'San Francisco',
                        'dropDownList': [
                            {'text': 'All Rentals', 'href': '/for_rent/San_Francisco,CA/'},
                            {'text': 'Apartments for Rent',
                             'href': '/for_rent/San_Francisco,CA/APARTMENT,APARTMENT_COMMUNITY,APARTMENT%7CCONDO%7CTOWNHOUSE,CONDO,COOP,LOFT,TIC_type/'},
                            {'text': 'Houses for Rent', 'href': '/for_rent/San_Francisco,CA/SINGLE-FAMILY_HOME_type/'},
                            {'text': 'Rooms for Rent', 'href': '/for_rent/San_Francisco,CA/ROOM_FOR_RENT_type/'}
                        ]
                    },
                    {'text': 'See Newest Listings', 'href': '/new-for-rent-properties/CA/'},
                    {'text': 'Post A Rental Listing', 'href': '/post-rental/'}
                ]
            },
            {
                'sectionHeaderText': 'Mortgage',
                'href': '/mortgages/',
                'dropDown': [
                    {
                        'dropDownHeaderText': 'San Francisco',
                        'dropDownList': [
                            {'text': 'Mortgage Overview', 'href': '/mortgages/'},
                            {'text': 'Get Pre-Qualified',
                             'href': '/mortgages/pre-approval?omni_src=mortgage%7Cglobal_nav'},
                            {'text': 'Mortgage Rates', 'href': '/mortgage-rates/San_Francisco,CA/'},
                            {'text': 'Refinance Rates', 'href': '/refinance-rates/San_Francisco,CA/'}
                        ]
                    },
                    {'text': 'Mortgage Calculator', 'href': '/mortgage-payment-calculator/'},
                    {'text': 'Affordability Calculator', 'href': '/house-affordability-calculator/'},
                    {'text': 'Rent vs Buy Calculator', 'href': '/rent_vs_buy/'},
                    {'text': 'Refinance Calculator', 'href': '/home-loan-refinance-calculator/'},
                ]
            },
        ]
    ),
    (
        'https://www.trulia.com/for_sale/29.797777039763,30.092292361495,-90.271999096805,-89.956142163212_xy/1p_beds/12_zm/',
        [
            {
                'sectionHeaderText': 'Buy',
                'href': '/LA/New_Orleans/',
                'dropDown': [
                    {
                        'dropDownHeaderText': 'New Orleans',
                        'dropDownList': [
                            {'text': 'Homes for Sale', 'href': '/LA/New_Orleans/'},
                            {'text': 'Open Houses', 'href': '/for_sale/New_Orleans,LA/p_oh/'},
                            {'text': 'New Homes', 'href': '/for_sale/New_Orleans,LA/new_homes_lt/'},
                            {'text': 'Recently Sold', 'href': '/sold/New_Orleans,LA/'}
                        ]
                    },
                    {'text': 'See Newest Listings', 'href': '/new-for-sale-properties/LA/'}
                ]
            },
            {
                'sectionHeaderText': 'Rent',
                'href': '/for_rent/New_Orleans,LA/',
                'dropDown': [
                    {
                        'dropDownHeaderText': 'New Orleans',
                        'dropDownList': [
                            {'text': 'All Rentals', 'href': '/for_rent/New_Orleans,LA/'},
                            {'text': 'Apartments for Rent',
                             'href': '/for_rent/New_Orleans,LA/APARTMENT,APARTMENT_COMMUNITY,APARTMENT%7CCONDO%7CTOWNHOUSE,CONDO,COOP,LOFT,TIC_type/'},
                            {'text': 'Houses for Rent', 'href': '/for_rent/New_Orleans,LA/SINGLE-FAMILY_HOME_type/'},
                            {'text': 'Rooms for Rent', 'href': '/for_rent/New_Orleans,LA/ROOM_FOR_RENT_type/'}
                        ]
                    },
                    {'text': 'See Newest Listings', 'href': '/new-for-rent-properties/LA/'},
                    {'text': 'Post A Rental Listing', 'href': '/post-rental/'}
                ]
            },
            {
                'sectionHeaderText': 'Mortgage',
                'href': '/mortgages/',
                'dropDown': [
                    {
                        'dropDownHeaderText': 'New Orleans',
                        'dropDownList': [
                            {'text': 'Mortgage Overview', 'href': '/mortgages/'},
                            {'text': 'Get Pre-Qualified',
                             'href': '/mortgages/pre-approval?omni_src=mortgage%7Cglobal_nav'},
                            {'text': 'Mortgage Rates', 'href': '/mortgage-rates/New_Orleans,LA/'},
                            {'text': 'Refinance Rates', 'href': '/refinance-rates/New_Orleans,LA/'}
                        ]
                    },
                    {'text': 'Mortgage Calculator', 'href': '/mortgage-payment-calculator/'},
                    {'text': 'Affordability Calculator', 'href': '/house-affordability-calculator/'},
                    {'text': 'Rent vs Buy Calculator', 'href': '/rent_vs_buy/'},
                    {'text': 'Refinance Calculator', 'href': '/home-loan-refinance-calculator/'},
                ]
            },
        ]
    ),
    (
        'https://www.trulia.com/for_sale/San_Mateo,CA/37.514857406994,37.580526177666,-122.35212773203,-122.27316349863_xy/14_zm/',
        [
            {
                'sectionHeaderText': 'Buy',
                'href': '/CA/San_Mateo/',
                'dropDown': [
                    {
                        'dropDownHeaderText': 'San Mateo',
                        'dropDownList': [
                            {'text': 'Homes for Sale', 'href': '/CA/San_Mateo/'},
                            {'text': 'Open Houses', 'href': '/for_sale/San_Mateo,CA/p_oh/'},
                            {'text': 'New Homes', 'href': '/for_sale/San_Mateo,CA/new_homes_lt/'},
                            {'text': 'Recently Sold', 'href': '/sold/San_Mateo,CA/'}
                        ]
                    },
                    {'text': 'See Newest Listings', 'href': '/new-for-sale-properties/CA/'}
                ]
            },
            {
                'sectionHeaderText': 'Rent',
                'href': '/for_rent/San_Mateo,CA/',
                'dropDown': [
                    {
                        'dropDownHeaderText': 'San Mateo',
                        'dropDownList': [
                            {'text': 'All Rentals', 'href': '/for_rent/San_Mateo,CA/'},
                            {'text': 'Apartments for Rent',
                             'href': '/for_rent/San_Mateo,CA/APARTMENT,APARTMENT_COMMUNITY,APARTMENT%7CCONDO%7CTOWNHOUSE,CONDO,COOP,LOFT,TIC_type/'},
                            {'text': 'Houses for Rent', 'href': '/for_rent/San_Mateo,CA/SINGLE-FAMILY_HOME_type/'},
                            {'text': 'Rooms for Rent', 'href': '/for_rent/San_Mateo,CA/ROOM_FOR_RENT_type/'}
                        ]
                    },
                    {'text': 'See Newest Listings', 'href': '/new-for-rent-properties/CA/'},
                    {'text': 'Post A Rental Listing', 'href': '/post-rental/'}
                ]
            },
            {
                'sectionHeaderText': 'Mortgage',
                'href': '/mortgages/',
                'dropDown': [
                    {
                        'dropDownHeaderText': 'San Mateo',
                        'dropDownList': [
                            {'text': 'Mortgage Overview', 'href': '/mortgages/'},
                            {'text': 'Get Pre-Qualified',
                             'href': '/mortgages/pre-approval?omni_src=mortgage%7Cglobal_nav'},
                            {'text': 'Mortgage Rates', 'href': '/mortgage-rates/San_Mateo,CA/'},
                            {'text': 'Refinance Rates', 'href': '/refinance-rates/San_Mateo,CA/'}
                        ]
                    },
                    {'text': 'Mortgage Calculator', 'href': '/mortgage-payment-calculator/'},
                    {'text': 'Affordability Calculator', 'href': '/house-affordability-calculator/'},
                    {'text': 'Rent vs Buy Calculator', 'href': '/rent_vs_buy/'},
                    {'text': 'Refinance Calculator', 'href': '/home-loan-refinance-calculator/'},
                ]
            },
        ]
    ),
    (
        'https://www.trulia.com/for_sale/600088777_sd/',
        [
            {
                'sectionHeaderText': 'Buy',
                'href': '/CA/San_Francisco/',
                'dropDown': [
                    {
                        'dropDownHeaderText': 'San Francisco',
                        'dropDownList': [
                            {'text': 'Homes for Sale', 'href': '/CA/San_Francisco/'},
                            {'text': 'Open Houses', 'href': '/for_sale/San_Francisco,CA/p_oh/'},
                            {'text': 'New Homes', 'href': '/for_sale/San_Francisco,CA/new_homes_lt/'},
                            {'text': 'Recently Sold', 'href': '/sold/San_Francisco,CA/'}
                        ]
                    },
                    {'text': 'See Newest Listings', 'href': '/new-for-sale-properties/CA/'}
                ]
            },
            {
                'sectionHeaderText': 'Rent',
                'href': '/for_rent/San_Francisco,CA/',
                'dropDown': [
                    {
                        'dropDownHeaderText': 'San Francisco',
                        'dropDownList': [
                            {'text': 'All Rentals', 'href': '/for_rent/San_Francisco,CA/'},
                            {'text': 'Apartments for Rent',
                             'href': '/for_rent/San_Francisco,CA/APARTMENT,APARTMENT_COMMUNITY,APARTMENT%7CCONDO%7CTOWNHOUSE,CONDO,COOP,LOFT,TIC_type/'},
                            {'text': 'Houses for Rent', 'href': '/for_rent/San_Francisco,CA/SINGLE-FAMILY_HOME_type/'},
                            {'text': 'Rooms for Rent', 'href': '/for_rent/San_Francisco,CA/ROOM_FOR_RENT_type/'}
                        ]
                    },
                    {'text': 'See Newest Listings', 'href': '/new-for-rent-properties/CA/'},
                    {'text': 'Post A Rental Listing', 'href': '/post-rental/'}
                ]
            },
            {
                'sectionHeaderText': 'Mortgage',
                'href': '/mortgages/',
                'dropDown': [
                    {
                        'dropDownHeaderText': 'San Francisco',
                        'dropDownList': [
                            {'text': 'Mortgage Overview', 'href': '/mortgages/'},
                            {'text': 'Get Pre-Qualified',
                             'href': '/mortgages/pre-approval?omni_src=mortgage%7Cglobal_nav'},
                            {'text': 'Mortgage Rates', 'href': '/mortgage-rates/San_Francisco,CA/'},
                            {'text': 'Refinance Rates', 'href': '/refinance-rates/San_Francisco,CA/'}
                        ]
                    },
                    {'text': 'Mortgage Calculator', 'href': '/mortgage-payment-calculator/'},
                    {'text': 'Affordability Calculator', 'href': '/house-affordability-calculator/'},
                    {'text': 'Rent vs Buy Calculator', 'href': '/rent_vs_buy/'},
                    {'text': 'Refinance Calculator', 'href': '/home-loan-refinance-calculator/'},
                ]
            },
        ]
    ),
    (
        'https://www.trulia.com/for_sale/5700044525_sd/',
        [
            {
                'sectionHeaderText': 'Buy',
                'href': '/CA/San_Francisco/',
                'dropDown': [
                    {
                        'dropDownHeaderText': 'San Francisco',
                        'dropDownList': [
                            {'text': 'Homes for Sale', 'href': '/CA/San_Francisco/'},
                            {'text': 'Open Houses', 'href': '/for_sale/San_Francisco,CA/p_oh/'},
                            {'text': 'New Homes', 'href': '/for_sale/San_Francisco,CA/new_homes_lt/'},
                            {'text': 'Recently Sold', 'href': '/sold/San_Francisco,CA/'}
                        ]
                    },
                    {'text': 'See Newest Listings', 'href': '/new-for-sale-properties/CA/'}
                ]
            },
            {
                'sectionHeaderText': 'Rent',
                'href': '/for_rent/San_Francisco,CA/',
                'dropDown': [
                    {
                        'dropDownHeaderText': 'San Francisco',
                        'dropDownList': [
                            {'text': 'All Rentals', 'href': '/for_rent/San_Francisco,CA/'},
                            {'text': 'Apartments for Rent',
                             'href': '/for_rent/San_Francisco,CA/APARTMENT,APARTMENT_COMMUNITY,APARTMENT%7CCONDO%7CTOWNHOUSE,CONDO,COOP,LOFT,TIC_type/'},
                            {'text': 'Houses for Rent', 'href': '/for_rent/San_Francisco,CA/SINGLE-FAMILY_HOME_type/'},
                            {'text': 'Rooms for Rent', 'href': '/for_rent/San_Francisco,CA/ROOM_FOR_RENT_type/'}
                        ]
                    },
                    {'text': 'See Newest Listings', 'href': '/new-for-rent-properties/CA/'},
                    {'text': 'Post A Rental Listing', 'href': '/post-rental/'}
                ]
            },
            {
                'sectionHeaderText': 'Mortgage',
                'href': '/mortgages/',
                'dropDown': [
                    {
                        'dropDownHeaderText': 'San Francisco',
                        'dropDownList': [
                            {'text': 'Mortgage Overview', 'href': '/mortgages/'},
                            {'text': 'Get Pre-Qualified',
                             'href': '/mortgages/pre-approval?omni_src=mortgage%7Cglobal_nav'},
                            {'text': 'Mortgage Rates', 'href': '/mortgage-rates/San_Francisco,CA/'},
                            {'text': 'Refinance Rates', 'href': '/refinance-rates/San_Francisco,CA/'}
                        ]
                    },
                    {'text': 'Mortgage Calculator', 'href': '/mortgage-payment-calculator/'},
                    {'text': 'Affordability Calculator', 'href': '/house-affordability-calculator/'},
                    {'text': 'Rent vs Buy Calculator', 'href': '/rent_vs_buy/'},
                    {'text': 'Refinance Calculator', 'href': '/home-loan-refinance-calculator/'},
                ]
            },
        ]
    ),

]

for_sale_footer_test_cases = [
    (
        'https://www.trulia.com/CA/San_Francisco/',
        [
            {
                'text': 'Nearby Real Estate',
                'citiesLinkGroup': [
                    {'text': 'Houses for Sale Near Me', 'href': '/houses-for-sale-near-me'},
                    {'text': 'Houses for Sale Near Me by Owner', 'href': '/houses-for-sale-near-me-by-owner'},
                    {'text': 'Open Houses Near Me', 'href': '/open-houses-near-me'},
                    {'text': 'Land for Sale Near Me', 'href': '/land-for-sale-near-me'},
                    {'text': 'Townhomes for Sale Near Me', 'href': '/townhomes-for-sale-near-me'},
                    {'text': 'Condos for Sale Near Me', 'href': '/condos-for-sale-near-me'}
                ]
            },
            {
                'text': 'San Francisco Neighborhoods',
                'href': '/sitemaps/neighborhood/California/San_Francisco/'
            },
            {
                'text': 'San Francisco Property Types',
                'href': '/sitemaps/city-for-sale/CA/San_Francisco/',
                'propertyTypesLinkGroup': [
                    {'text': 'For Sale by Owner (FSBO)',
                     'href': '/for_sale/San_Francisco,CA/fsbo_lt/'},
                    {'text': 'Single Family Homes', 'href': '/for_sale/San_Francisco,CA/SINGLE-FAMILY_HOME_type/'},
                    {'text': 'Condos',
                     'href': '/for_sale/San_Francisco,CA/CONDO_type/'},
                    {'text': 'Townhomes', 'href': '/for_sale/San_Francisco,CA/TOWNHOUSE_type/'},
                    {'text': 'Co-Ops', 'href': '/for_sale/San_Francisco,CA/COOP_type/'},
                    {'text': 'Apartments', 'href': '/for_sale/San_Francisco,CA/APARTMENT_type/'},
                    {'text': 'Mobile/Manufactured Homes',
                     'href': '/for_sale/San_Francisco,CA/MOBILE|MANUFACTURED_type/'}
                ]
            },
            {
                'text': 'Real Estate and Mortgage Guides',
                'localInfoLinksGroup': [
                    {'text': 'San Francisco Real Estate Guide', 'href': '/real_estate/San_Francisco-California/'},
                    {'text': 'San Francisco Schools', 'href': '/real_estate/San_Francisco-California/schools/'},
                    {'text': 'Newest Homes for Sale in California', 'href': '/new-for-sale-properties/CA/'},
                    {'text': 'Newest Rentals in California', 'href': '/new-for-rent-properties/CA/'},
                    {'text': 'San Francisco Mortgage', 'href': '/mortgage-rates/San_Francisco,CA/'},
                    {'text': 'San Francisco Refinance', 'href': '/refinance-rates/San_Francisco,CA/'}
                ]
            }
        ]
    ),
    (
        'https://www.trulia.com/for_sale/San_Francisco,CA/p_oh/',
        [
            {
                'text': 'Nearby Real Estate',
                'citiesLinkGroup': [
                    {'text': 'Houses for Sale Near Me', 'href': '/houses-for-sale-near-me'},
                    {'text': 'Houses for Sale Near Me by Owner', 'href': '/houses-for-sale-near-me-by-owner'},
                    {'text': 'Open Houses Near Me', 'href': '/open-houses-near-me'},
                    {'text': 'Land for Sale Near Me', 'href': '/land-for-sale-near-me'},
                    {'text': 'Townhomes for Sale Near Me', 'href': '/townhomes-for-sale-near-me'},
                    {'text': 'Condos for Sale Near Me', 'href': '/condos-for-sale-near-me'}
                ]
            },
            {
                'text': 'San Francisco Neighborhoods | Nearby Zip Codes',
                'href': '/sitemaps/neighborhood/California/San_Francisco/'
            },
            {
                'text': 'San Francisco Property Types',
                'href': '/sitemaps/city-for-sale/CA/San_Francisco/',
                'propertyTypesLinkGroup': [
                    {'text': 'For Sale by Owner (FSBO)',
                     'href': '/for_sale/San_Francisco,CA/fsbo_lt/'},
                    {'text': 'Single Family Homes', 'href': '/for_sale/San_Francisco,CA/SINGLE-FAMILY_HOME_type/'},
                    {'text': 'Condos',
                     'href': '/for_sale/San_Francisco,CA/CONDO_type/'},
                    {'text': 'Townhomes', 'href': '/for_sale/San_Francisco,CA/TOWNHOUSE_type/'},
                    {'text': 'Co-Ops', 'href': '/for_sale/San_Francisco,CA/COOP_type/'},
                    {'text': 'Apartments', 'href': '/for_sale/San_Francisco,CA/APARTMENT_type/'},
                    {'text': 'Mobile/Manufactured Homes',
                     'href': '/for_sale/San_Francisco,CA/MOBILE|MANUFACTURED_type/'}
                ]
            },
            {
                'text': 'Real Estate and Mortgage Guides',
                'localInfoLinksGroup': [
                    {'text': 'San Francisco Real Estate Guide', 'href': '/real_estate/San_Francisco-California/'},
                    {'text': 'San Francisco Schools', 'href': '/real_estate/San_Francisco-California/schools/'},
                    {'text': 'Newest Homes for Sale in California', 'href': '/new-for-sale-properties/CA/'},
                    {'text': 'Newest Rentals in California', 'href': '/new-for-rent-properties/CA/'},
                    {'text': 'San Francisco Mortgage', 'href': '/mortgage-rates/San_Francisco,CA/'},
                    {'text': 'San Francisco Refinance', 'href': '/refinance-rates/San_Francisco,CA/'}
                ]
            }
        ]
    ),
    (
        'https://www.trulia.com/for_sale/San_Francisco,CA/new_homes_lt/',
        [
            {
                'text': 'Nearby Real Estate',
                'citiesLinkGroup': [
                    {'text': 'Houses for Sale Near Me', 'href': '/houses-for-sale-near-me'},
                    {'text': 'Houses for Sale Near Me by Owner', 'href': '/houses-for-sale-near-me-by-owner'},
                    {'text': 'Open Houses Near Me', 'href': '/open-houses-near-me'},
                    {'text': 'Land for Sale Near Me', 'href': '/land-for-sale-near-me'},
                    {'text': 'Townhomes for Sale Near Me', 'href': '/townhomes-for-sale-near-me'},
                    {'text': 'Condos for Sale Near Me', 'href': '/condos-for-sale-near-me'}
                ]
            },
            {
                'text': 'San Francisco Neighborhoods',
                'href': '/sitemaps/neighborhood/California/San_Francisco/'
            },
            {
                'text': 'San Francisco Property Types',
                'href': '/sitemaps/city-for-sale/CA/San_Francisco/',
                'propertyTypesLinkGroup': [
                    {'text': 'For Sale by Owner (FSBO)',
                     'href': '/for_sale/San_Francisco,CA/fsbo_lt/'},
                    {'text': 'Single Family Homes', 'href': '/for_sale/San_Francisco,CA/SINGLE-FAMILY_HOME_type/'},
                    {'text': 'Condos',
                     'href': '/for_sale/San_Francisco,CA/CONDO_type/'},
                    {'text': 'Townhomes', 'href': '/for_sale/San_Francisco,CA/TOWNHOUSE_type/'},
                    {'text': 'Co-Ops', 'href': '/for_sale/San_Francisco,CA/COOP_type/'},
                    {'text': 'Apartments', 'href': '/for_sale/San_Francisco,CA/APARTMENT_type/'},
                    {'text': 'Mobile/Manufactured Homes',
                     'href': '/for_sale/San_Francisco,CA/MOBILE|MANUFACTURED_type/'}
                ]
            },
            {
                'text': 'Real Estate and Mortgage Guides',
                'localInfoLinksGroup': [
                    {'text': 'San Francisco Real Estate Guide', 'href': '/real_estate/San_Francisco-California/'},
                    {'text': 'San Francisco Schools', 'href': '/real_estate/San_Francisco-California/schools/'},
                    {'text': 'Newest Homes for Sale in California', 'href': '/new-for-sale-properties/CA/'},
                    {'text': 'Newest Rentals in California', 'href': '/new-for-rent-properties/CA/'},
                    {'text': 'San Francisco Mortgage', 'href': '/mortgage-rates/San_Francisco,CA/'},
                    {'text': 'San Francisco Refinance', 'href': '/refinance-rates/San_Francisco,CA/'}
                ]
            }
        ]
    ),
    (
        'https://www.trulia.com/for_sale/San_Francisco,CA/SINGLE-FAMILY_HOME_type/',
        [
            {
                'text': 'Nearby Real Estate',
                'citiesLinkGroup': [
                    {'text': 'Houses for Sale Near Me', 'href': '/houses-for-sale-near-me'},
                    {'text': 'Houses for Sale Near Me by Owner', 'href': '/houses-for-sale-near-me-by-owner'},
                    {'text': 'Open Houses Near Me', 'href': '/open-houses-near-me'},
                    {'text': 'Land for Sale Near Me', 'href': '/land-for-sale-near-me'},
                    {'text': 'Townhomes for Sale Near Me', 'href': '/townhomes-for-sale-near-me'},
                    {'text': 'Condos for Sale Near Me', 'href': '/condos-for-sale-near-me'}
                ]
            },
            {
                'text': 'San Francisco Neighborhoods',
                'href': '/sitemaps/neighborhood/California/San_Francisco/'
            },
            {
                'text': 'San Francisco Property Types',
                'href': '/sitemaps/city-for-sale/CA/San_Francisco/',
                'propertyTypesLinkGroup': [
                    {'text': 'For Sale by Owner (FSBO)',
                     'href': '/for_sale/San_Francisco,CA/fsbo_lt/'},
                    {'text': 'Single Family Homes', 'href': '/for_sale/San_Francisco,CA/SINGLE-FAMILY_HOME_type/'},
                    {'text': 'Condos',
                     'href': '/for_sale/San_Francisco,CA/CONDO_type/'},
                    {'text': 'Townhomes', 'href': '/for_sale/San_Francisco,CA/TOWNHOUSE_type/'},
                    {'text': 'Co-Ops', 'href': '/for_sale/San_Francisco,CA/COOP_type/'},
                    {'text': 'Apartments', 'href': '/for_sale/San_Francisco,CA/APARTMENT_type/'},
                    {'text': 'Mobile/Manufactured Homes',
                     'href': '/for_sale/San_Francisco,CA/MOBILE|MANUFACTURED_type/'}
                ]
            },
            {
                'text': 'Real Estate and Mortgage Guides',
                'localInfoLinksGroup': [
                    {'text': 'San Francisco Real Estate Guide', 'href': '/real_estate/San_Francisco-California/'},
                    {'text': 'San Francisco Schools', 'href': '/real_estate/San_Francisco-California/schools/'},
                    {'text': 'Newest Homes for Sale in California', 'href': '/new-for-sale-properties/CA/'},
                    {'text': 'Newest Rentals in California', 'href': '/new-for-rent-properties/CA/'},
                    {'text': 'San Francisco Mortgage', 'href': '/mortgage-rates/San_Francisco,CA/'},
                    {'text': 'San Francisco Refinance', 'href': '/refinance-rates/San_Francisco,CA/'}
                ]
            }
        ]
    ),
    (
        'https://www.trulia.com/for_sale/San_Francisco,CA/',
        [
            {
                'text': 'Nearby Real Estate',
                'citiesLinkGroup': [
                    {'text': 'Houses for Sale Near Me', 'href': '/houses-for-sale-near-me'},
                    {'text': 'Houses for Sale Near Me by Owner', 'href': '/houses-for-sale-near-me-by-owner'},
                    {'text': 'Open Houses Near Me', 'href': '/open-houses-near-me'},
                    {'text': 'Land for Sale Near Me', 'href': '/land-for-sale-near-me'},
                    {'text': 'Townhomes for Sale Near Me', 'href': '/townhomes-for-sale-near-me'},
                    {'text': 'Condos for Sale Near Me', 'href': '/condos-for-sale-near-me'}
                ]
            },
            {
                'text': 'San Francisco Neighborhoods',
                'href': '/sitemaps/neighborhood/California/San_Francisco/'
            },
            {
                'text': 'San Francisco Property Types',
                'href': '/sitemaps/city-for-sale/CA/San_Francisco/',
                'propertyTypesLinkGroup': [
                    {'text': 'For Sale by Owner (FSBO)',
                     'href': '/for_sale/San_Francisco,CA/fsbo_lt/'},
                    {'text': 'Single Family Homes', 'href': '/for_sale/San_Francisco,CA/SINGLE-FAMILY_HOME_type/'},
                    {'text': 'Condos',
                     'href': '/for_sale/San_Francisco,CA/CONDO_type/'},
                    {'text': 'Townhomes', 'href': '/for_sale/San_Francisco,CA/TOWNHOUSE_type/'},
                    {'text': 'Co-Ops', 'href': '/for_sale/San_Francisco,CA/COOP_type/'},
                    {'text': 'Apartments', 'href': '/for_sale/San_Francisco,CA/APARTMENT_type/'},
                    {'text': 'Mobile/Manufactured Homes',
                     'href': '/for_sale/San_Francisco,CA/MOBILE|MANUFACTURED_type/'}
                ]
            },
            {
                'text': 'Real Estate and Mortgage Guides',
                'localInfoLinksGroup': [
                    {'text': 'San Francisco Real Estate Guide', 'href': '/real_estate/San_Francisco-California/'},
                    {'text': 'San Francisco Schools', 'href': '/real_estate/San_Francisco-California/schools/'},
                    {'text': 'Newest Homes for Sale in California', 'href': '/new-for-sale-properties/CA/'},
                    {'text': 'Newest Rentals in California', 'href': '/new-for-rent-properties/CA/'},
                    {'text': 'San Francisco Mortgage', 'href': '/mortgage-rates/San_Francisco,CA/'},
                    {'text': 'San Francisco Refinance', 'href': '/refinance-rates/San_Francisco,CA/'}
                ]
            }
        ]
    ),
    (
        'https://www.trulia.com/for_sale/06075_c/new_homes_lt/',
        [
            {
                'text': 'Nearby Real Estate',
                'citiesLinkGroup': [
                    {'text': 'Houses for Sale Near Me', 'href': '/houses-for-sale-near-me'},
                    {'text': 'Houses for Sale Near Me by Owner', 'href': '/houses-for-sale-near-me-by-owner'},
                    {'text': 'Open Houses Near Me', 'href': '/open-houses-near-me'},
                    {'text': 'Land for Sale Near Me', 'href': '/land-for-sale-near-me'},
                    {'text': 'Townhomes for Sale Near Me', 'href': '/townhomes-for-sale-near-me'},
                    {'text': 'Condos for Sale Near Me', 'href': '/condos-for-sale-near-me'}
                ]
            },
            {
                'text': 'San Francisco Neighborhoods',
                'href': '/sitemaps/neighborhood/California/San_Francisco/'
            },
            {
                'text': 'San Francisco Property Types',
                'href': '/sitemaps/city-for-sale/CA/San_Francisco/',
                'propertyTypesLinkGroup': [
                    {'text': 'For Sale by Owner (FSBO)',
                     'href': '/for_sale/San_Francisco,CA/fsbo_lt/'},
                    {'text': 'Single Family Homes', 'href': '/for_sale/San_Francisco,CA/SINGLE-FAMILY_HOME_type/'},
                    {'text': 'Condos',
                     'href': '/for_sale/San_Francisco,CA/CONDO_type/'},
                    {'text': 'Townhomes', 'href': '/for_sale/San_Francisco,CA/TOWNHOUSE_type/'},
                    {'text': 'Co-Ops', 'href': '/for_sale/San_Francisco,CA/COOP_type/'},
                    {'text': 'Apartments', 'href': '/for_sale/San_Francisco,CA/APARTMENT_type/'},
                    {'text': 'Mobile/Manufactured Homes',
                     'href': '/for_sale/San_Francisco,CA/MOBILE|MANUFACTURED_type/'}
                ]
            },
            {
                'text': 'Real Estate and Mortgage Guides',
                'localInfoLinksGroup': [
                    {'text': 'San Francisco Real Estate Guide', 'href': '/real_estate/San_Francisco-California/'},
                    {'text': 'San Francisco Schools', 'href': '/real_estate/San_Francisco-California/schools/'},
                    {'text': 'Newest Homes for Sale in California', 'href': '/new-for-sale-properties/CA/'},
                    {'text': 'Newest Rentals in California', 'href': '/new-for-rent-properties/CA/'},
                    {'text': 'San Francisco Mortgage', 'href': '/mortgage-rates/San_Francisco,CA/'},
                    {'text': 'San Francisco Refinance', 'href': '/refinance-rates/San_Francisco,CA/'}
                ]
            }
        ]
    ),
    (
        'https://www.trulia.com/for_sale/06075_c/SINGLE-FAMILY_HOME_type/',
        [
            {
                'text': 'Nearby Real Estate',
                'citiesLinkGroup': [
                    {'text': 'Houses for Sale Near Me', 'href': '/houses-for-sale-near-me'},
                    {'text': 'Houses for Sale Near Me by Owner', 'href': '/houses-for-sale-near-me-by-owner'},
                    {'text': 'Open Houses Near Me', 'href': '/open-houses-near-me'},
                    {'text': 'Land for Sale Near Me', 'href': '/land-for-sale-near-me'},
                    {'text': 'Townhomes for Sale Near Me', 'href': '/townhomes-for-sale-near-me'},
                    {'text': 'Condos for Sale Near Me', 'href': '/condos-for-sale-near-me'}
                ]
            },
            {
                'text': 'San Francisco Neighborhoods',
                'href': '/sitemaps/neighborhood/California/San_Francisco/'
            },
            {
                'text': 'San Francisco Property Types',
                'href': '/sitemaps/city-for-sale/CA/San_Francisco/',
                'propertyTypesLinkGroup': [
                    {'text': 'For Sale by Owner (FSBO)',
                     'href': '/for_sale/San_Francisco,CA/fsbo_lt/'},
                    {'text': 'Single Family Homes', 'href': '/for_sale/San_Francisco,CA/SINGLE-FAMILY_HOME_type/'},
                    {'text': 'Condos',
                     'href': '/for_sale/San_Francisco,CA/CONDO_type/'},
                    {'text': 'Townhomes', 'href': '/for_sale/San_Francisco,CA/TOWNHOUSE_type/'},
                    {'text': 'Co-Ops', 'href': '/for_sale/San_Francisco,CA/COOP_type/'},
                    {'text': 'Apartments', 'href': '/for_sale/San_Francisco,CA/APARTMENT_type/'},
                    {'text': 'Mobile/Manufactured Homes',
                     'href': '/for_sale/San_Francisco,CA/MOBILE|MANUFACTURED_type/'}
                ]
            },
            {
                'text': 'Real Estate and Mortgage Guides',
                'localInfoLinksGroup': [
                    {'text': 'San Francisco Real Estate Guide', 'href': '/real_estate/San_Francisco-California/'},
                    {'text': 'San Francisco Schools', 'href': '/real_estate/San_Francisco-California/schools/'},
                    {'text': 'Newest Homes for Sale in California', 'href': '/new-for-sale-properties/CA/'},
                    {'text': 'Newest Rentals in California', 'href': '/new-for-rent-properties/CA/'},
                    {'text': 'San Francisco Mortgage', 'href': '/mortgage-rates/San_Francisco,CA/'},
                    {'text': 'San Francisco Refinance', 'href': '/refinance-rates/San_Francisco,CA/'}
                ]
            }
        ]
    ),
    (
        'https://www.trulia.com/for_sale/06075_c/p_oh/',
        [
            {
                'text': 'Nearby Real Estate',
                'citiesLinkGroup': [
                    {'text': 'Houses for Sale Near Me', 'href': '/houses-for-sale-near-me'},
                    {'text': 'Houses for Sale Near Me by Owner', 'href': '/houses-for-sale-near-me-by-owner'},
                    {'text': 'Open Houses Near Me', 'href': '/open-houses-near-me'},
                    {'text': 'Land for Sale Near Me', 'href': '/land-for-sale-near-me'},
                    {'text': 'Townhomes for Sale Near Me', 'href': '/townhomes-for-sale-near-me'},
                    {'text': 'Condos for Sale Near Me', 'href': '/condos-for-sale-near-me'}
                ]
            },
            {
                'text': 'San Francisco Neighborhoods',
                'href': '/sitemaps/neighborhood/California/San_Francisco/'
            },
            {
                'text': 'San Francisco Property Types',
                'href': '/sitemaps/city-for-sale/CA/San_Francisco/',
                'propertyTypesLinkGroup': [
                    {'text': 'For Sale by Owner (FSBO)',
                     'href': '/for_sale/San_Francisco,CA/fsbo_lt/'},
                    {'text': 'Single Family Homes', 'href': '/for_sale/San_Francisco,CA/SINGLE-FAMILY_HOME_type/'},
                    {'text': 'Condos',
                     'href': '/for_sale/San_Francisco,CA/CONDO_type/'},
                    {'text': 'Townhomes', 'href': '/for_sale/San_Francisco,CA/TOWNHOUSE_type/'},
                    {'text': 'Co-Ops', 'href': '/for_sale/San_Francisco,CA/COOP_type/'},
                    {'text': 'Apartments', 'href': '/for_sale/San_Francisco,CA/APARTMENT_type/'},
                    {'text': 'Mobile/Manufactured Homes',
                     'href': '/for_sale/San_Francisco,CA/MOBILE|MANUFACTURED_type/'}
                ]
            },
            {
                'text': 'Real Estate and Mortgage Guides',
                'localInfoLinksGroup': [
                    {'text': 'San Francisco Real Estate Guide', 'href': '/real_estate/San_Francisco-California/'},
                    {'text': 'San Francisco Schools', 'href': '/real_estate/San_Francisco-California/schools/'},
                    {'text': 'Newest Homes for Sale in California', 'href': '/new-for-sale-properties/CA/'},
                    {'text': 'Newest Rentals in California', 'href': '/new-for-rent-properties/CA/'},
                    {'text': 'San Francisco Mortgage', 'href': '/mortgage-rates/San_Francisco,CA/'},
                    {'text': 'San Francisco Refinance', 'href': '/refinance-rates/San_Francisco,CA/'}
                ]
            }
        ]
    ),
    (
        'https://www.trulia.com/for_sale/1468_nh/new_homes_lt/',
        [
            {
                'text': 'Nearby Real Estate',
                'citiesLinkGroup': [
                    {'text': 'Houses for Sale Near Me', 'href': '/houses-for-sale-near-me'},
                    {'text': 'Houses for Sale Near Me by Owner', 'href': '/houses-for-sale-near-me-by-owner'},
                    {'text': 'Open Houses Near Me', 'href': '/open-houses-near-me'},
                    {'text': 'Land for Sale Near Me', 'href': '/land-for-sale-near-me'},
                    {'text': 'Townhomes for Sale Near Me', 'href': '/townhomes-for-sale-near-me'},
                    {'text': 'Condos for Sale Near Me', 'href': '/condos-for-sale-near-me'}
                ]
            },
            {
                'text': 'San Francisco Neighborhoods',
                'href': '/sitemaps/neighborhood/California/San_Francisco/'
            },
            {
                'text': 'Tenderloin Property Types',
                'href': '/sitemaps/city-for-sale/CA/San_Francisco/',
                'propertyTypesLinkGroup': [
                    {'text': 'For Sale by Owner (FSBO)',
                     'href': '/for_sale/1468_nh/fsbo_lt/'},
                    {'text': 'Single Family Homes', 'href': '/for_sale/1468_nh/SINGLE-FAMILY_HOME_type/'},
                    {'text': 'Condos',
                     'href': '/for_sale/1468_nh/CONDO_type/'},
                    {'text': 'Townhomes', 'href': '/for_sale/1468_nh/TOWNHOUSE_type/'},
                    {'text': 'Co-Ops', 'href': '/for_sale/1468_nh/COOP_type/'},
                    {'text': 'Apartments', 'href': '/for_sale/1468_nh/APARTMENT_type/'},
                    {'text': 'Mobile/Manufactured Homes',
                     'href': '/for_sale/1468_nh/MOBILE|MANUFACTURED_type/'}
                ]
            },
            {
                'text': 'Real Estate and Mortgage Guides',
                'localInfoLinksGroup': [
                    {'text': 'San Francisco Real Estate Guide', 'href': '/real_estate/San_Francisco-California/'},
                    {'text': 'San Francisco Schools', 'href': '/real_estate/San_Francisco-California/schools/'},
                    {'text': 'Newest Homes for Sale in California', 'href': '/new-for-sale-properties/CA/'},
                    {'text': 'Newest Rentals in California', 'href': '/new-for-rent-properties/CA/'},
                    {'text': 'San Francisco Mortgage', 'href': '/mortgage-rates/San_Francisco,CA/'},
                    {'text': 'San Francisco Refinance', 'href': '/refinance-rates/San_Francisco,CA/'}
                ]
            }
        ]
    ),
    (
        'https://www.trulia.com/for_sale/1468_nh/p_oh/',
        [
            {
                'text': 'Nearby Real Estate',
                'citiesLinkGroup': [
                    {'text': 'Houses for Sale Near Me', 'href': '/houses-for-sale-near-me'},
                    {'text': 'Houses for Sale Near Me by Owner', 'href': '/houses-for-sale-near-me-by-owner'},
                    {'text': 'Open Houses Near Me', 'href': '/open-houses-near-me'},
                    {'text': 'Land for Sale Near Me', 'href': '/land-for-sale-near-me'},
                    {'text': 'Townhomes for Sale Near Me', 'href': '/townhomes-for-sale-near-me'},
                    {'text': 'Condos for Sale Near Me', 'href': '/condos-for-sale-near-me'}
                ]
            },
            {
                'text': 'San Francisco Neighborhoods',
                'href': '/sitemaps/neighborhood/California/San_Francisco/'
            },
            {
                'text': 'Tenderloin Property Types',
                'href': '/sitemaps/city-for-sale/CA/San_Francisco/',
                'propertyTypesLinkGroup': [
                    {'text': 'For Sale by Owner (FSBO)',
                     'href': '/for_sale/1468_nh/fsbo_lt/'},
                    {'text': 'Single Family Homes', 'href': '/for_sale/1468_nh/SINGLE-FAMILY_HOME_type/'},
                    {'text': 'Condos',
                     'href': '/for_sale/1468_nh/CONDO_type/'},
                    {'text': 'Townhomes', 'href': '/for_sale/1468_nh/TOWNHOUSE_type/'},
                    {'text': 'Co-Ops', 'href': '/for_sale/1468_nh/COOP_type/'},
                    {'text': 'Apartments', 'href': '/for_sale/1468_nh/APARTMENT_type/'},
                    {'text': 'Mobile/Manufactured Homes',
                     'href': '/for_sale/1468_nh/MOBILE|MANUFACTURED_type/'}
                ]
            },
            {
                'text': 'Real Estate and Mortgage Guides',
                'localInfoLinksGroup': [
                    {'text': 'San Francisco Real Estate Guide', 'href': '/real_estate/San_Francisco-California/'},
                    {'text': 'San Francisco Schools', 'href': '/real_estate/San_Francisco-California/schools/'},
                    {'text': 'Newest Homes for Sale in California', 'href': '/new-for-sale-properties/CA/'},
                    {'text': 'Newest Rentals in California', 'href': '/new-for-rent-properties/CA/'},
                    {'text': 'San Francisco Mortgage', 'href': '/mortgage-rates/San_Francisco,CA/'},
                    {'text': 'San Francisco Refinance', 'href': '/refinance-rates/San_Francisco,CA/'}
                ]
            }
        ]
    ),
    (
        'https://www.trulia.com/for_sale/06075_c/fsbo_lt/',
        [
            {
                'text': 'Nearby Real Estate',
                'citiesLinkGroup': [
                    {'text': 'Houses for Sale Near Me', 'href': '/houses-for-sale-near-me'},
                    {'text': 'Houses for Sale Near Me by Owner', 'href': '/houses-for-sale-near-me-by-owner'},
                    {'text': 'Open Houses Near Me', 'href': '/open-houses-near-me'},
                    {'text': 'Land for Sale Near Me', 'href': '/land-for-sale-near-me'},
                    {'text': 'Townhomes for Sale Near Me', 'href': '/townhomes-for-sale-near-me'},
                    {'text': 'Condos for Sale Near Me', 'href': '/condos-for-sale-near-me'}
                ]
            },
            {
                'text': 'San Francisco Neighborhoods',
                'href': '/sitemaps/neighborhood/California/San_Francisco/'
            },
            {
                'text': 'San Francisco Property Types',
                'href': '/sitemaps/city-for-sale/CA/San_Francisco/',
                'propertyTypesLinkGroup': [
                    {'text': 'For Sale by Owner (FSBO)',
                     'href': '/for_sale/San_Francisco,CA/fsbo_lt/'},
                    {'text': 'Single Family Homes', 'href': '/for_sale/San_Francisco,CA/SINGLE-FAMILY_HOME_type/'},
                    {'text': 'Condos',
                     'href': '/for_sale/San_Francisco,CA/CONDO_type/'},
                    {'text': 'Townhomes', 'href': '/for_sale/San_Francisco,CA/TOWNHOUSE_type/'},
                    {'text': 'Co-Ops', 'href': '/for_sale/San_Francisco,CA/COOP_type/'},
                    {'text': 'Apartments', 'href': '/for_sale/San_Francisco,CA/APARTMENT_type/'},
                    {'text': 'Mobile/Manufactured Homes',
                     'href': '/for_sale/San_Francisco,CA/MOBILE|MANUFACTURED_type/'}
                ]
            },
            {
                'text': 'Real Estate and Mortgage Guides',
                'localInfoLinksGroup': [
                    {'text': 'San Francisco Real Estate Guide', 'href': '/real_estate/San_Francisco-California/'},
                    {'text': 'San Francisco Schools', 'href': '/real_estate/San_Francisco-California/schools/'},
                    {'text': 'Newest Homes for Sale in California', 'href': '/new-for-sale-properties/CA/'},
                    {'text': 'Newest Rentals in California', 'href': '/new-for-rent-properties/CA/'},
                    {'text': 'San Francisco Mortgage', 'href': '/mortgage-rates/San_Francisco,CA/'},
                    {'text': 'San Francisco Refinance', 'href': '/refinance-rates/San_Francisco,CA/'}
                ]
            }
        ]
    ),
    (
        'https://www.trulia.com/for_sale/San_Francisco,CA/fsbo_lt/',
        [
            {
                'text': 'Nearby Real Estate',
                'citiesLinkGroup': [
                    {'text': 'Houses for Sale Near Me', 'href': '/houses-for-sale-near-me'},
                    {'text': 'Houses for Sale Near Me by Owner', 'href': '/houses-for-sale-near-me-by-owner'},
                    {'text': 'Open Houses Near Me', 'href': '/open-houses-near-me'},
                    {'text': 'Land for Sale Near Me', 'href': '/land-for-sale-near-me'},
                    {'text': 'Townhomes for Sale Near Me', 'href': '/townhomes-for-sale-near-me'},
                    {'text': 'Condos for Sale Near Me', 'href': '/condos-for-sale-near-me'}
                ]
            },
            {
                'text': 'San Francisco Neighborhoods',
                'href': '/sitemaps/neighborhood/California/San_Francisco/'
            },
            {
                'text': 'San Francisco Property Types',
                'href': '/sitemaps/city-for-sale/CA/San_Francisco/',
                'propertyTypesLinkGroup': [
                    {'text': 'For Sale by Owner (FSBO)',
                     'href': '/for_sale/San_Francisco,CA/fsbo_lt/'},
                    {'text': 'Single Family Homes', 'href': '/for_sale/San_Francisco,CA/SINGLE-FAMILY_HOME_type/'},
                    {'text': 'Condos',
                     'href': '/for_sale/San_Francisco,CA/CONDO_type/'},
                    {'text': 'Townhomes', 'href': '/for_sale/San_Francisco,CA/TOWNHOUSE_type/'},
                    {'text': 'Co-Ops', 'href': '/for_sale/San_Francisco,CA/COOP_type/'},
                    {'text': 'Apartments', 'href': '/for_sale/San_Francisco,CA/APARTMENT_type/'},
                    {'text': 'Mobile/Manufactured Homes',
                     'href': '/for_sale/San_Francisco,CA/MOBILE|MANUFACTURED_type/'}
                ]
            },
            {
                'text': 'Real Estate and Mortgage Guides',
                'localInfoLinksGroup': [
                    {'text': 'San Francisco Real Estate Guide', 'href': '/real_estate/San_Francisco-California/'},
                    {'text': 'San Francisco Schools', 'href': '/real_estate/San_Francisco-California/schools/'},
                    {'text': 'Newest Homes for Sale in California', 'href': '/new-for-sale-properties/CA/'},
                    {'text': 'Newest Rentals in California', 'href': '/new-for-rent-properties/CA/'},
                    {'text': 'San Francisco Mortgage', 'href': '/mortgage-rates/San_Francisco,CA/'},
                    {'text': 'San Francisco Refinance', 'href': '/refinance-rates/San_Francisco,CA/'}
                ]
            }
        ]
    ),
    (
        'https://www.trulia.com/for_sale/San_Francisco,CA/foreclosure_lt/',
        [
            {
                'text': 'Nearby Real Estate',
                'citiesLinkGroup': [
                    {'text': 'Houses for Sale Near Me', 'href': '/houses-for-sale-near-me'},
                    {'text': 'Houses for Sale Near Me by Owner', 'href': '/houses-for-sale-near-me-by-owner'},
                    {'text': 'Open Houses Near Me', 'href': '/open-houses-near-me'},
                    {'text': 'Land for Sale Near Me', 'href': '/land-for-sale-near-me'},
                    {'text': 'Townhomes for Sale Near Me', 'href': '/townhomes-for-sale-near-me'},
                    {'text': 'Condos for Sale Near Me', 'href': '/condos-for-sale-near-me'}
                ]
            },
            {
                'text': 'San Francisco Neighborhoods',
                'href': '/sitemaps/neighborhood/California/San_Francisco/'
            },
            {
                'text': 'San Francisco Property Types',
                'href': '/sitemaps/city-for-sale/CA/San_Francisco/',
                'propertyTypesLinkGroup': [
                    {'text': 'For Sale by Owner (FSBO)',
                     'href': '/for_sale/San_Francisco,CA/fsbo_lt/'},
                    {'text': 'Single Family Homes', 'href': '/for_sale/San_Francisco,CA/SINGLE-FAMILY_HOME_type/'},
                    {'text': 'Condos',
                     'href': '/for_sale/San_Francisco,CA/CONDO_type/'},
                    {'text': 'Townhomes', 'href': '/for_sale/San_Francisco,CA/TOWNHOUSE_type/'},
                    {'text': 'Co-Ops', 'href': '/for_sale/San_Francisco,CA/COOP_type/'},
                    {'text': 'Apartments', 'href': '/for_sale/San_Francisco,CA/APARTMENT_type/'},
                    {'text': 'Mobile/Manufactured Homes',
                     'href': '/for_sale/San_Francisco,CA/MOBILE|MANUFACTURED_type/'}
                ]
            },
            {
                'text': 'Real Estate and Mortgage Guides',
                'localInfoLinksGroup': [
                    {'text': 'San Francisco Real Estate Guide', 'href': '/real_estate/San_Francisco-California/'},
                    {'text': 'San Francisco Schools', 'href': '/real_estate/San_Francisco-California/schools/'},
                    {'text': 'Newest Homes for Sale in California', 'href': '/new-for-sale-properties/CA/'},
                    {'text': 'Newest Rentals in California', 'href': '/new-for-rent-properties/CA/'},
                    {'text': 'San Francisco Mortgage', 'href': '/mortgage-rates/San_Francisco,CA/'},
                    {'text': 'San Francisco Refinance', 'href': '/refinance-rates/San_Francisco,CA/'}
                ]
            }
        ]
    ),
    (
        'https://www.trulia.com/for_sale/06075_c/foreclosure_lt/',
        [
            {
                'text': 'Nearby Real Estate',
                'citiesLinkGroup': [
                    {'text': 'Houses for Sale Near Me', 'href': '/houses-for-sale-near-me'},
                    {'text': 'Houses for Sale Near Me by Owner', 'href': '/houses-for-sale-near-me-by-owner'},
                    {'text': 'Open Houses Near Me', 'href': '/open-houses-near-me'},
                    {'text': 'Land for Sale Near Me', 'href': '/land-for-sale-near-me'},
                    {'text': 'Townhomes for Sale Near Me', 'href': '/townhomes-for-sale-near-me'},
                    {'text': 'Condos for Sale Near Me', 'href': '/condos-for-sale-near-me'}
                ]
            },
            {
                'text': 'San Francisco Neighborhoods',
                'href': '/sitemaps/neighborhood/California/San_Francisco/'
            },
            {
                'text': 'San Francisco Property Types',
                'href': '/sitemaps/city-for-sale/CA/San_Francisco/',
                'propertyTypesLinkGroup': [
                    {'text': 'For Sale by Owner (FSBO)',
                     'href': '/for_sale/San_Francisco,CA/fsbo_lt/'},
                    {'text': 'Single Family Homes', 'href': '/for_sale/San_Francisco,CA/SINGLE-FAMILY_HOME_type/'},
                    {'text': 'Condos',
                     'href': '/for_sale/San_Francisco,CA/CONDO_type/'},
                    {'text': 'Townhomes', 'href': '/for_sale/San_Francisco,CA/TOWNHOUSE_type/'},
                    {'text': 'Co-Ops', 'href': '/for_sale/San_Francisco,CA/COOP_type/'},
                    {'text': 'Apartments', 'href': '/for_sale/San_Francisco,CA/APARTMENT_type/'},
                    {'text': 'Mobile/Manufactured Homes',
                     'href': '/for_sale/San_Francisco,CA/MOBILE|MANUFACTURED_type/'}
                ]
            },
            {
                'text': 'Real Estate and Mortgage Guides',
                'localInfoLinksGroup': [
                    {'text': 'San Francisco Real Estate Guide', 'href': '/real_estate/San_Francisco-California/'},
                    {'text': 'San Francisco Schools', 'href': '/real_estate/San_Francisco-California/schools/'},
                    {'text': 'Newest Homes for Sale in California', 'href': '/new-for-sale-properties/CA/'},
                    {'text': 'Newest Rentals in California', 'href': '/new-for-rent-properties/CA/'},
                    {'text': 'San Francisco Mortgage', 'href': '/mortgage-rates/San_Francisco,CA/'},
                    {'text': 'San Francisco Refinance', 'href': '/refinance-rates/San_Francisco,CA/'}
                ]
            }
        ]
    ),
    (
        'https://www.trulia.com/for_sale/95073_zip/foreclosure_lt/',
        [
            {
                'text': 'Nearby Real Estate',
                'citiesLinkGroup': [
                    {'text': 'Houses for Sale Near Me', 'href': '/houses-for-sale-near-me'},
                    {'text': 'Houses for Sale Near Me by Owner', 'href': '/houses-for-sale-near-me-by-owner'},
                    {'text': 'Open Houses Near Me', 'href': '/open-houses-near-me'},
                    {'text': 'Land for Sale Near Me', 'href': '/land-for-sale-near-me'},
                    {'text': 'Townhomes for Sale Near Me', 'href': '/townhomes-for-sale-near-me'},
                    {'text': 'Condos for Sale Near Me', 'href': '/condos-for-sale-near-me'}
                ]
            },
            {
                'text': 'Soquel Neighborhoods',
                'href': '/sitemaps/neighborhood/California/Soquel/'
            },
            {
                'text': 'Soquel Property Types',
                'href': '/sitemaps/city-for-sale/CA/Soquel/',
                'propertyTypesLinkGroup': [
                    {'text': 'For Sale by Owner (FSBO)',
                     'href': '/for_sale/Soquel,CA/fsbo_lt/'},
                    {'text': 'Single Family Homes', 'href': '/for_sale/Soquel,CA/SINGLE-FAMILY_HOME_type/'},
                    {'text': 'Condos',
                     'href': '/for_sale/Soquel,CA/CONDO_type/'},
                    {'text': 'Townhomes', 'href': '/for_sale/Soquel,CA/TOWNHOUSE_type/'},
                    {'text': 'Co-Ops', 'href': '/for_sale/Soquel,CA/COOP_type/'},
                    {'text': 'Apartments', 'href': '/for_sale/Soquel,CA/APARTMENT_type/'},
                    {'text': 'Mobile/Manufactured Homes',
                     'href': '/for_sale/Soquel,CA/MOBILE|MANUFACTURED_type/'}
                ]
            },
            {
                'text': 'Real Estate and Mortgage Guides',
                'localInfoLinksGroup': [
                    {'text': 'Soquel Real Estate Guide', 'href': '/real_estate/Soquel-California/'},
                    {'text': 'Soquel Schools', 'href': '/real_estate/Soquel-California/schools/'},
                    {'text': 'Newest Homes for Sale in California', 'href': '/new-for-sale-properties/CA/'},
                    {'text': 'Newest Rentals in California', 'href': '/new-for-rent-properties/CA/'},
                    {'text': 'Soquel Mortgage', 'href': '/mortgage-rates/Soquel,CA/'},
                    {'text': 'Soquel Refinance', 'href': '/refinance-rates/Soquel,CA/'}
                ]
            }
        ]
    ),
    (
        'https://www.trulia.com/for_sale/New_York,NY/resale_lt/',
        [
            {
                'text': 'Nearby Real Estate',
                'citiesLinkGroup': [
                    {'text': 'Houses for Sale Near Me', 'href': '/houses-for-sale-near-me'},
                    {'text': 'Houses for Sale Near Me by Owner', 'href': '/houses-for-sale-near-me-by-owner'},
                    {'text': 'Open Houses Near Me', 'href': '/open-houses-near-me'},
                    {'text': 'Land for Sale Near Me', 'href': '/land-for-sale-near-me'},
                    {'text': 'Townhomes for Sale Near Me', 'href': '/townhomes-for-sale-near-me'},
                    {'text': 'Condos for Sale Near Me', 'href': '/condos-for-sale-near-me'}
                ]
            },
            {
                'text': 'New York Neighborhoods',
                'href': '/sitemaps/neighborhood/New_York/New_York/'
            },
            {
                'text': 'New York Property Types',
                'href': '/sitemaps/city-for-sale/NY/New_York/',
                'propertyTypesLinkGroup': [
                    {'text': 'For Sale by Owner (FSBO)',
                     'href': '/for_sale/New_York,NY/fsbo_lt/'},
                    {'text': 'Single Family Homes', 'href': '/for_sale/New_York,NY/SINGLE-FAMILY_HOME_type/'},
                    {'text': 'Condos',
                     'href': '/for_sale/New_York,NY/CONDO_type/'},
                    {'text': 'Townhomes', 'href': '/for_sale/New_York,NY/TOWNHOUSE_type/'},
                    {'text': 'Co-Ops', 'href': '/for_sale/New_York,NY/COOP_type/'},
                    {'text': 'Apartments', 'href': '/for_sale/New_York,NY/APARTMENT_type/'},
                    {'text': 'Mobile/Manufactured Homes',
                     'href': '/for_sale/New_York,NY/MOBILE|MANUFACTURED_type/'}
                ]
            },
            {
                'text': 'Real Estate and Mortgage Guides',
                'localInfoLinksGroup': [
                    {'text': 'New York Real Estate Guide', 'href': '/real_estate/New_York-New_York/'},
                    {'text': 'New York Schools', 'href': '/real_estate/New_York-New_York/schools/'},
                    {'text': 'Newest Homes for Sale in New York', 'href': '/new-for-sale-properties/NY/'},
                    {'text': 'Newest Rentals in New York', 'href': '/new-for-rent-properties/NY/'},
                    {'text': 'New York Mortgage', 'href': '/mortgage-rates/New_York,NY/'},
                    {'text': 'New York Refinance', 'href': '/refinance-rates/New_York,NY/'}
                ]
            }
        ]
    ),
    (
        'https://www.trulia.com/for_sale/New_York,NY/foreclosure,fsbo,new_homes,resale,sale_pending_lt/',
        [
            {
                'text': 'Nearby Real Estate',
                'citiesLinkGroup': [
                    {'text': 'Houses for Sale Near Me', 'href': '/houses-for-sale-near-me'},
                    {'text': 'Houses for Sale Near Me by Owner', 'href': '/houses-for-sale-near-me-by-owner'},
                    {'text': 'Open Houses Near Me', 'href': '/open-houses-near-me'},
                    {'text': 'Land for Sale Near Me', 'href': '/land-for-sale-near-me'},
                    {'text': 'Townhomes for Sale Near Me', 'href': '/townhomes-for-sale-near-me'},
                    {'text': 'Condos for Sale Near Me', 'href': '/condos-for-sale-near-me'}
                ]
            },
            {
                'text': 'New York Neighborhoods',
                'href': '/sitemaps/neighborhood/New_York/New_York/'
            },
            {
                'text': 'New York Property Types',
                'href': '/sitemaps/city-for-sale/NY/New_York/',
                'propertyTypesLinkGroup': [
                    {'text': 'For Sale by Owner (FSBO)',
                     'href': '/for_sale/New_York,NY/fsbo_lt/'},
                    {'text': 'Single Family Homes', 'href': '/for_sale/New_York,NY/SINGLE-FAMILY_HOME_type/'},
                    {'text': 'Condos',
                     'href': '/for_sale/New_York,NY/CONDO_type/'},
                    {'text': 'Townhomes', 'href': '/for_sale/New_York,NY/TOWNHOUSE_type/'},
                    {'text': 'Co-Ops', 'href': '/for_sale/New_York,NY/COOP_type/'},
                    {'text': 'Apartments', 'href': '/for_sale/New_York,NY/APARTMENT_type/'},
                    {'text': 'Mobile/Manufactured Homes',
                     'href': '/for_sale/New_York,NY/MOBILE|MANUFACTURED_type/'}
                ]
            },
            {
                'text': 'Real Estate and Mortgage Guides',
                'localInfoLinksGroup': [
                    {'text': 'New York Real Estate Guide', 'href': '/real_estate/New_York-New_York/'},
                    {'text': 'New York Schools', 'href': '/real_estate/New_York-New_York/schools/'},
                    {'text': 'Newest Homes for Sale in New York', 'href': '/new-for-sale-properties/NY/'},
                    {'text': 'Newest Rentals in New York', 'href': '/new-for-rent-properties/NY/'},
                    {'text': 'New York Mortgage', 'href': '/mortgage-rates/New_York,NY/'},
                    {'text': 'New York Refinance', 'href': '/refinance-rates/New_York,NY/'}
                ]
            }
        ]
    ),
    (
        'https://www.trulia.com/for_sale/San_Mateo,CA/37.514857406994,37.580526177666,-122.35212773203,-122.27316349863_xy/14_zm/',
        [
            {
                'text': 'Nearby Real Estate',
                'citiesLinkGroup': [
                    {'text': 'Houses for Sale Near Me', 'href': '/houses-for-sale-near-me'},
                    {'text': 'Houses for Sale Near Me by Owner', 'href': '/houses-for-sale-near-me-by-owner'},
                    {'text': 'Open Houses Near Me', 'href': '/open-houses-near-me'},
                    {'text': 'Land for Sale Near Me', 'href': '/land-for-sale-near-me'},
                    {'text': 'Townhomes for Sale Near Me', 'href': '/townhomes-for-sale-near-me'},
                    {'text': 'Condos for Sale Near Me', 'href': '/condos-for-sale-near-me'}
                ]
            },
            {
                'text': 'San Mateo Neighborhoods',
                'href': '/sitemaps/neighborhood/California/San_Mateo/'
            },
            {
                'text': 'San Mateo Property Types',
                'href': '/sitemaps/city-for-sale/CA/San_Mateo/',
                'propertyTypesLinkGroup': [
                    {'text': 'For Sale by Owner (FSBO)',
                     'href': '/for_sale/San_Mateo,CA/fsbo_lt/'},
                    {'text': 'Single Family Homes', 'href': '/for_sale/San_Mateo,CA/SINGLE-FAMILY_HOME_type/'},
                    {'text': 'Condos',
                     'href': '/for_sale/San_Mateo,CA/CONDO_type/'},
                    {'text': 'Townhomes', 'href': '/for_sale/San_Mateo,CA/TOWNHOUSE_type/'},
                    {'text': 'Co-Ops', 'href': '/for_sale/San_Mateo,CA/COOP_type/'},
                    {'text': 'Apartments', 'href': '/for_sale/San_Mateo,CA/APARTMENT_type/'},
                    {'text': 'Mobile/Manufactured Homes',
                     'href': '/for_sale/San_Mateo,CA/MOBILE|MANUFACTURED_type/'}
                ]
            },
            {
                'text': 'Real Estate and Mortgage Guides',
                'localInfoLinksGroup': [
                    {'text': 'San Mateo Real Estate Guide', 'href': '/real_estate/San_Mateo-California/'},
                    {'text': 'San Mateo Schools', 'href': '/real_estate/San_Mateo-California/schools/'},
                    {'text': 'Newest Homes for Sale in California', 'href': '/new-for-sale-properties/CA/'},
                    {'text': 'Newest Rentals in California', 'href': '/new-for-rent-properties/CA/'},
                    {'text': 'San Mateo Mortgage', 'href': '/mortgage-rates/San_Mateo,CA/'},
                    {'text': 'San Mateo Refinance', 'href': '/refinance-rates/San_Mateo,CA/'}
                ]
            }
        ]
    ),
    (
        'https://www.trulia.com/NY/New_York/',
        [
            {
                'text': 'Nearby Real Estate',
                'citiesLinkGroup': [
                    {'text': 'Houses for Sale Near Me', 'href': '/houses-for-sale-near-me'},
                    {'text': 'Houses for Sale Near Me by Owner', 'href': '/houses-for-sale-near-me-by-owner'},
                    {'text': 'Open Houses Near Me', 'href': '/open-houses-near-me'},
                    {'text': 'Land for Sale Near Me', 'href': '/land-for-sale-near-me'},
                    {'text': 'Townhomes for Sale Near Me', 'href': '/townhomes-for-sale-near-me'},
                    {'text': 'Condos for Sale Near Me', 'href': '/condos-for-sale-near-me'}
                ]
            },
            {
                'text': 'New York Neighborhoods',
                'href': '/sitemaps/neighborhood/New_York/New_York/'
            },
            {
                'text': 'New York Property Types',
                'href': '/sitemaps/city-for-sale/NY/New_York/',
                'propertyTypesLinkGroup': [
                    {'text': 'For Sale by Owner (FSBO)',
                     'href': '/for_sale/New_York,NY/fsbo_lt/'},
                    {'text': 'Single Family Homes', 'href': '/for_sale/New_York,NY/SINGLE-FAMILY_HOME_type/'},
                    {'text': 'Condos',
                     'href': '/for_sale/New_York,NY/CONDO_type/'},
                    {'text': 'Townhomes', 'href': '/for_sale/New_York,NY/TOWNHOUSE_type/'},
                    {'text': 'Co-Ops', 'href': '/for_sale/New_York,NY/COOP_type/'},
                    {'text': 'Apartments', 'href': '/for_sale/New_York,NY/APARTMENT_type/'},
                    {'text': 'Mobile/Manufactured Homes',
                     'href': '/for_sale/New_York,NY/MOBILE|MANUFACTURED_type/'}
                ]
            },
            {
                'text': 'Real Estate and Mortgage Guides',
                'localInfoLinksGroup': [
                    {'text': 'New York Real Estate Guide', 'href': '/real_estate/New_York-New_York/'},
                    {'text': 'New York Schools', 'href': '/real_estate/New_York-New_York/schools/'},
                    {'text': 'Newest Homes for Sale in New York', 'href': '/new-for-sale-properties/NY/'},
                    {'text': 'Newest Rentals in New York', 'href': '/new-for-rent-properties/NY/'},
                    {'text': 'New York Mortgage', 'href': '/mortgage-rates/New_York,NY/'},
                    {'text': 'New York Refinance', 'href': '/refinance-rates/New_York,NY/'}
                ]
            }
        ]
    ),
    (
        'https://www.trulia.com/for_sale/1468_nh/SINGLE-FAMILY_HOME_type/',
        [
            {
                'text': 'Nearby Real Estate',
                'citiesLinkGroup': [
                    {'text': 'Houses for Sale Near Me', 'href': '/houses-for-sale-near-me'},
                    {'text': 'Houses for Sale Near Me by Owner', 'href': '/houses-for-sale-near-me-by-owner'},
                    {'text': 'Open Houses Near Me', 'href': '/open-houses-near-me'},
                    {'text': 'Land for Sale Near Me', 'href': '/land-for-sale-near-me'},
                    {'text': 'Townhomes for Sale Near Me', 'href': '/townhomes-for-sale-near-me'},
                    {'text': 'Condos for Sale Near Me', 'href': '/condos-for-sale-near-me'}
                ]
            },
            {
                'text': 'San Francisco Neighborhoods',
                'href': '/sitemaps/neighborhood/California/San_Francisco/'
            },
            {
                'text': 'Tenderloin Property Types',
                'href': '/sitemaps/city-for-sale/CA/San_Francisco/',
                'propertyTypesLinkGroup': [
                    {'text': 'For Sale by Owner (FSBO)',
                     'href': '/for_sale/1468_nh/fsbo_lt/'},
                    {'text': 'Single Family Homes', 'href': '/for_sale/1468_nh/SINGLE-FAMILY_HOME_type/'},
                    {'text': 'Condos',
                     'href': '/for_sale/1468_nh/CONDO_type/'},
                    {'text': 'Townhomes', 'href': '/for_sale/1468_nh/TOWNHOUSE_type/'},
                    {'text': 'Co-Ops', 'href': '/for_sale/1468_nh/COOP_type/'},
                    {'text': 'Apartments', 'href': '/for_sale/1468_nh/APARTMENT_type/'},
                    {'text': 'Mobile/Manufactured Homes',
                     'href': '/for_sale/1468_nh/MOBILE|MANUFACTURED_type/'}
                ]
            },
            {
                'text': 'Real Estate and Mortgage Guides',
                'localInfoLinksGroup': [
                    {'text': 'San Francisco Real Estate Guide', 'href': '/real_estate/San_Francisco-California/'},
                    {'text': 'San Francisco Schools', 'href': '/real_estate/San_Francisco-California/schools/'},
                    {'text': 'Newest Homes for Sale in California', 'href': '/new-for-sale-properties/CA/'},
                    {'text': 'Newest Rentals in California', 'href': '/new-for-rent-properties/CA/'},
                    {'text': 'San Francisco Mortgage', 'href': '/mortgage-rates/San_Francisco,CA/'},
                    {'text': 'San Francisco Refinance', 'href': '/refinance-rates/San_Francisco,CA/'}
                ]
            }
        ]
    ),
    (
        'https://www.trulia.com/for_sale/1468_nh/p_oh/',
        [
            {
                'text': 'Nearby Real Estate',
                'citiesLinkGroup': [
                    {'text': 'Houses for Sale Near Me', 'href': '/houses-for-sale-near-me'},
                    {'text': 'Houses for Sale Near Me by Owner', 'href': '/houses-for-sale-near-me-by-owner'},
                    {'text': 'Open Houses Near Me', 'href': '/open-houses-near-me'},
                    {'text': 'Land for Sale Near Me', 'href': '/land-for-sale-near-me'},
                    {'text': 'Townhomes for Sale Near Me', 'href': '/townhomes-for-sale-near-me'},
                    {'text': 'Condos for Sale Near Me', 'href': '/condos-for-sale-near-me'}
                ]
            },
            {
                'text': 'San Francisco Neighborhoods',
                'href': '/sitemaps/neighborhood/California/San_Francisco/'
            },
            {
                'text': 'Tenderloin Property Types',
                'href': '/sitemaps/city-for-sale/CA/San_Francisco/',
                'propertyTypesLinkGroup': [
                    {'text': 'For Sale by Owner (FSBO)',
                     'href': '/for_sale/1468_nh/fsbo_lt/'},
                    {'text': 'Single Family Homes', 'href': '/for_sale/1468_nh/SINGLE-FAMILY_HOME_type/'},
                    {'text': 'Condos',
                     'href': '/for_sale/1468_nh/CONDO_type/'},
                    {'text': 'Townhomes', 'href': '/for_sale/1468_nh/TOWNHOUSE_type/'},
                    {'text': 'Co-Ops', 'href': '/for_sale/1468_nh/COOP_type/'},
                    {'text': 'Apartments', 'href': '/for_sale/1468_nh/APARTMENT_type/'},
                    {'text': 'Mobile/Manufactured Homes',
                     'href': '/for_sale/1468_nh/MOBILE|MANUFACTURED_type/'}
                ]
            },
            {
                'text': 'Real Estate and Mortgage Guides',
                'localInfoLinksGroup': [
                    {'text': 'San Francisco Real Estate Guide', 'href': '/real_estate/San_Francisco-California/'},
                    {'text': 'San Francisco Schools', 'href': '/real_estate/San_Francisco-California/schools/'},
                    {'text': 'Newest Homes for Sale in California', 'href': '/new-for-sale-properties/CA/'},
                    {'text': 'Newest Rentals in California', 'href': '/new-for-rent-properties/CA/'},
                    {'text': 'San Francisco Mortgage', 'href': '/mortgage-rates/San_Francisco,CA/'},
                    {'text': 'San Francisco Refinance', 'href': '/refinance-rates/San_Francisco,CA/'}
                ]
            }
        ]
    ),
    (
        'https://www.trulia.com/for_sale/1468_nh/fsbo_lt/',
        [
            {
                'text': 'Nearby Real Estate',
                'citiesLinkGroup': [
                    {'text': 'Houses for Sale Near Me', 'href': '/houses-for-sale-near-me'},
                    {'text': 'Houses for Sale Near Me by Owner', 'href': '/houses-for-sale-near-me-by-owner'},
                    {'text': 'Open Houses Near Me', 'href': '/open-houses-near-me'},
                    {'text': 'Land for Sale Near Me', 'href': '/land-for-sale-near-me'},
                    {'text': 'Townhomes for Sale Near Me', 'href': '/townhomes-for-sale-near-me'},
                    {'text': 'Condos for Sale Near Me', 'href': '/condos-for-sale-near-me'}
                ]
            },
            {
                'text': 'San Francisco Neighborhoods',
                'href': '/sitemaps/neighborhood/California/San_Francisco/'
            },
            {
                'text': 'Tenderloin Property Types',
                'href': '/sitemaps/city-for-sale/CA/San_Francisco/',
                'propertyTypesLinkGroup': [
                    {'text': 'For Sale by Owner (FSBO)',
                     'href': '/for_sale/1468_nh/fsbo_lt/'},
                    {'text': 'Single Family Homes', 'href': '/for_sale/1468_nh/SINGLE-FAMILY_HOME_type/'},
                    {'text': 'Condos',
                     'href': '/for_sale/1468_nh/CONDO_type/'},
                    {'text': 'Townhomes', 'href': '/for_sale/1468_nh/TOWNHOUSE_type/'},
                    {'text': 'Co-Ops', 'href': '/for_sale/1468_nh/COOP_type/'},
                    {'text': 'Apartments', 'href': '/for_sale/1468_nh/APARTMENT_type/'},
                    {'text': 'Mobile/Manufactured Homes',
                     'href': '/for_sale/1468_nh/MOBILE|MANUFACTURED_type/'}
                ]
            },
            {
                'text': 'Real Estate and Mortgage Guides',
                'localInfoLinksGroup': [
                    {'text': 'San Francisco Real Estate Guide', 'href': '/real_estate/San_Francisco-California/'},
                    {'text': 'San Francisco Schools', 'href': '/real_estate/San_Francisco-California/schools/'},
                    {'text': 'Newest Homes for Sale in California', 'href': '/new-for-sale-properties/CA/'},
                    {'text': 'Newest Rentals in California', 'href': '/new-for-rent-properties/CA/'},
                    {'text': 'San Francisco Mortgage', 'href': '/mortgage-rates/San_Francisco,CA/'},
                    {'text': 'San Francisco Refinance', 'href': '/refinance-rates/San_Francisco,CA/'}
                ]
            }
        ]
    ),
    (
        'https://www.trulia.com/for_sale/1468_nh/foreclosure_lt/',
        [
            {
                'text': 'Nearby Real Estate',
                'citiesLinkGroup': [
                    {'text': 'Houses for Sale Near Me', 'href': '/houses-for-sale-near-me'},
                    {'text': 'Houses for Sale Near Me by Owner', 'href': '/houses-for-sale-near-me-by-owner'},
                    {'text': 'Open Houses Near Me', 'href': '/open-houses-near-me'},
                    {'text': 'Land for Sale Near Me', 'href': '/land-for-sale-near-me'},
                    {'text': 'Townhomes for Sale Near Me', 'href': '/townhomes-for-sale-near-me'},
                    {'text': 'Condos for Sale Near Me', 'href': '/condos-for-sale-near-me'}
                ]
            },
            {
                'text': 'San Francisco Neighborhoods',
                'href': '/sitemaps/neighborhood/California/San_Francisco/'
            },
            {
                'text': 'Tenderloin Property Types',
                'href': '/sitemaps/city-for-sale/CA/San_Francisco/',
                'propertyTypesLinkGroup': [
                    {'text': 'For Sale by Owner (FSBO)',
                     'href': '/for_sale/1468_nh/fsbo_lt/'},
                    {'text': 'Single Family Homes', 'href': '/for_sale/1468_nh/SINGLE-FAMILY_HOME_type/'},
                    {'text': 'Condos',
                     'href': '/for_sale/1468_nh/CONDO_type/'},
                    {'text': 'Townhomes', 'href': '/for_sale/1468_nh/TOWNHOUSE_type/'},
                    {'text': 'Co-Ops', 'href': '/for_sale/1468_nh/COOP_type/'},
                    {'text': 'Apartments', 'href': '/for_sale/1468_nh/APARTMENT_type/'},
                    {'text': 'Mobile/Manufactured Homes',
                     'href': '/for_sale/1468_nh/MOBILE|MANUFACTURED_type/'}
                ]
            },
            {
                'text': 'Real Estate and Mortgage Guides',
                'localInfoLinksGroup': [
                    {'text': 'San Francisco Real Estate Guide', 'href': '/real_estate/San_Francisco-California/'},
                    {'text': 'San Francisco Schools', 'href': '/real_estate/San_Francisco-California/schools/'},
                    {'text': 'Newest Homes for Sale in California', 'href': '/new-for-sale-properties/CA/'},
                    {'text': 'Newest Rentals in California', 'href': '/new-for-rent-properties/CA/'},
                    {'text': 'San Francisco Mortgage', 'href': '/mortgage-rates/San_Francisco,CA/'},
                    {'text': 'San Francisco Refinance', 'href': '/refinance-rates/San_Francisco,CA/'}
                ]
            }
        ]
    ),
    (
        'https://www.trulia.com/for_sale/1468_nh/p_oh/',
        [
            {
                'text': 'Nearby Real Estate',
                'citiesLinkGroup': [
                    {'text': 'Houses for Sale Near Me', 'href': '/houses-for-sale-near-me'},
                    {'text': 'Houses for Sale Near Me by Owner', 'href': '/houses-for-sale-near-me-by-owner'},
                    {'text': 'Open Houses Near Me', 'href': '/open-houses-near-me'},
                    {'text': 'Land for Sale Near Me', 'href': '/land-for-sale-near-me'},
                    {'text': 'Townhomes for Sale Near Me', 'href': '/townhomes-for-sale-near-me'},
                    {'text': 'Condos for Sale Near Me', 'href': '/condos-for-sale-near-me'}
                ]
            },
            {
                'text': 'San Francisco Neighborhoods',
                'href': '/sitemaps/neighborhood/California/San_Francisco/'
            },
            {
                'text': 'Tenderloin Property Types',
                'href': '/sitemaps/city-for-sale/CA/San_Francisco/',
                'propertyTypesLinkGroup': [
                    {'text': 'For Sale by Owner (FSBO)',
                     'href': '/for_sale/1468_nh/fsbo_lt/'},
                    {'text': 'Single Family Homes', 'href': '/for_sale/1468_nh/SINGLE-FAMILY_HOME_type/'},
                    {'text': 'Condos',
                     'href': '/for_sale/1468_nh/CONDO_type/'},
                    {'text': 'Townhomes', 'href': '/for_sale/1468_nh/TOWNHOUSE_type/'},
                    {'text': 'Co-Ops', 'href': '/for_sale/1468_nh/COOP_type/'},
                    {'text': 'Apartments', 'href': '/for_sale/1468_nh/APARTMENT_type/'},
                    {'text': 'Mobile/Manufactured Homes',
                     'href': '/for_sale/1468_nh/MOBILE|MANUFACTURED_type/'}
                ]
            },
            {
                'text': 'Real Estate and Mortgage Guides',
                'localInfoLinksGroup': [
                    {'text': 'San Francisco Real Estate Guide', 'href': '/real_estate/San_Francisco-California/'},
                    {'text': 'San Francisco Schools', 'href': '/real_estate/San_Francisco-California/schools/'},
                    {'text': 'Newest Homes for Sale in California', 'href': '/new-for-sale-properties/CA/'},
                    {'text': 'Newest Rentals in California', 'href': '/new-for-rent-properties/CA/'},
                    {'text': 'San Francisco Mortgage', 'href': '/mortgage-rates/San_Francisco,CA/'},
                    {'text': 'San Francisco Refinance', 'href': '/refinance-rates/San_Francisco,CA/'}
                ]
            }
        ]
    ),
    (
        'https://www.trulia.com/for_sale/San_Francisco,CA/english-cottages/',
        [
            {
                'text': 'Nearby Real Estate',
                'citiesLinkGroup': [
                    {'text': 'Houses for Sale Near Me', 'href': '/houses-for-sale-near-me'},
                    {'text': 'Houses for Sale Near Me by Owner', 'href': '/houses-for-sale-near-me-by-owner'},
                    {'text': 'Open Houses Near Me', 'href': '/open-houses-near-me'},
                    {'text': 'Land for Sale Near Me', 'href': '/land-for-sale-near-me'},
                    {'text': 'Townhomes for Sale Near Me', 'href': '/townhomes-for-sale-near-me'},
                    {'text': 'Condos for Sale Near Me', 'href': '/condos-for-sale-near-me'}
                ]
            },
            {
                'text': 'San Francisco Neighborhoods',
                'href': '/sitemaps/neighborhood/California/San_Francisco/'
            },
            {
                'text': 'San Francisco Property Types',
                'href': '/sitemaps/city-for-sale/CA/San_Francisco/',
                'propertyTypesLinkGroup': [
                    {'text': 'For Sale by Owner (FSBO)',
                     'href': '/for_sale/San_Francisco,CA/fsbo_lt/'},
                    {'text': 'Single Family Homes', 'href': '/for_sale/San_Francisco,CA/SINGLE-FAMILY_HOME_type/'},
                    {'text': 'Condos',
                     'href': '/for_sale/San_Francisco,CA/CONDO_type/'},
                    {'text': 'Townhomes', 'href': '/for_sale/San_Francisco,CA/TOWNHOUSE_type/'},
                    {'text': 'Co-Ops', 'href': '/for_sale/San_Francisco,CA/COOP_type/'},
                    {'text': 'Apartments', 'href': '/for_sale/San_Francisco,CA/APARTMENT_type/'},
                    {'text': 'Mobile/Manufactured Homes',
                     'href': '/for_sale/San_Francisco,CA/MOBILE|MANUFACTURED_type/'}
                ]
            },
            {
                'text': 'Real Estate and Mortgage Guides',
                'localInfoLinksGroup': [
                    {'text': 'San Francisco Real Estate Guide', 'href': '/real_estate/San_Francisco-California/'},
                    {'text': 'San Francisco Schools', 'href': '/real_estate/San_Francisco-California/schools/'},
                    {'text': 'Newest Homes for Sale in California', 'href': '/new-for-sale-properties/CA/'},
                    {'text': 'Newest Rentals in California', 'href': '/new-for-rent-properties/CA/'},
                    {'text': 'San Francisco Mortgage', 'href': '/mortgage-rates/San_Francisco,CA/'},
                    {'text': 'San Francisco Refinance', 'href': '/refinance-rates/San_Francisco,CA/'}
                ]
            }
        ]
    ),
    (
        'https://www.trulia.com/for_sale/37.538050449811,38.04863829018,-122.31357480293,-121.68186093574_xy/SINGLE-FAMILY_HOME_type/11_zm/',
        [
            {
                'text': 'Nearby Real Estate',
                'citiesLinkGroup': [
                    {'text': 'Houses for Sale Near Me', 'href': '/houses-for-sale-near-me'},
                    {'text': 'Houses for Sale Near Me by Owner', 'href': '/houses-for-sale-near-me-by-owner'},
                    {'text': 'Open Houses Near Me', 'href': '/open-houses-near-me'},
                    {'text': 'Land for Sale Near Me', 'href': '/land-for-sale-near-me'},
                    {'text': 'Townhomes for Sale Near Me', 'href': '/townhomes-for-sale-near-me'},
                    {'text': 'Condos for Sale Near Me', 'href': '/condos-for-sale-near-me'}
                ]
            },
            {
                'text': 'San Ramon Property Types',
                'href': '/sitemaps/city-for-sale/CA/San_Ramon/',
                'propertyTypesLinkGroup': [
                    {'text': 'For Sale by Owner (FSBO)',
                     'href': '/for_sale/San_Ramon,CA/fsbo_lt/'},
                    {'text': 'Single Family Homes', 'href': '/for_sale/San_Ramon,CA/SINGLE-FAMILY_HOME_type/'},
                    {'text': 'Condos',
                     'href': '/for_sale/San_Ramon,CA/CONDO_type/'},
                    {'text': 'Townhomes', 'href': '/for_sale/San_Ramon,CA/TOWNHOUSE_type/'},
                    {'text': 'Co-Ops', 'href': '/for_sale/San_Ramon,CA/COOP_type/'},
                    {'text': 'Apartments', 'href': '/for_sale/San_Ramon,CA/APARTMENT_type/'},
                    {'text': 'Mobile/Manufactured Homes',
                     'href': '/for_sale/San_Ramon,CA/MOBILE|MANUFACTURED_type/'}
                ]
            },
            {
                'text': 'Real Estate and Mortgage Guides',
                'localInfoLinksGroup': [
                    {'text': 'San Ramon Real Estate Guide', 'href': '/real_estate/San_Ramon-California/'},
                    {'text': 'San Ramon Schools', 'href': '/real_estate/San_Ramon-California/schools/'},
                    {'text': 'Newest Homes for Sale in California', 'href': '/new-for-sale-properties/CA/'},
                    {'text': 'Newest Rentals in California', 'href': '/new-for-rent-properties/CA/'},
                    {'text': 'San Ramon Mortgage', 'href': '/mortgage-rates/San_Ramon,CA/'},
                    {'text': 'San Ramon Refinance', 'href': '/refinance-rates/San_Ramon,CA/'}
                ]
            }
        ]
    ),
    (
        'https://www.trulia.com/for_sale/36.861754825027,37.888660248935,-121.57103313202,-120.30760539765_xy/foreclosure_lt/10_zm/',
        [
            {
                'text': 'Nearby Real Estate',
                'citiesLinkGroup': [
                    {'text': 'Houses for Sale Near Me', 'href': '/houses-for-sale-near-me'},
                    {'text': 'Houses for Sale Near Me by Owner', 'href': '/houses-for-sale-near-me-by-owner'},
                    {'text': 'Open Houses Near Me', 'href': '/open-houses-near-me'},
                    {'text': 'Land for Sale Near Me', 'href': '/land-for-sale-near-me'},
                    {'text': 'Townhomes for Sale Near Me', 'href': '/townhomes-for-sale-near-me'},
                    {'text': 'Condos for Sale Near Me', 'href': '/condos-for-sale-near-me'}
                ]
            },
            {
                'text': 'Hilmar Neighborhoods',
                'href': '/sitemaps/neighborhood/California/Hilmar/'
            },
            {
                'text': 'Hilmar Property Types',
                'href': '/sitemaps/city-for-sale/CA/Hilmar/',
                'propertyTypesLinkGroup': [
                    {'text': 'For Sale by Owner (FSBO)',
                     'href': '/for_sale/Hilmar,CA/fsbo_lt/'},
                    {'text': 'Single Family Homes', 'href': '/for_sale/Hilmar,CA/SINGLE-FAMILY_HOME_type/'},
                    {'text': 'Condos',
                     'href': '/for_sale/Hilmar,CA/CONDO_type/'},
                    {'text': 'Townhomes', 'href': '/for_sale/Hilmar,CA/TOWNHOUSE_type/'},
                    {'text': 'Co-Ops', 'href': '/for_sale/Hilmar,CA/COOP_type/'},
                    {'text': 'Apartments', 'href': '/for_sale/Hilmar,CA/APARTMENT_type/'},
                    {'text': 'Mobile/Manufactured Homes',
                     'href': '/for_sale/Hilmar,CA/MOBILE|MANUFACTURED_type/'}
                ]
            },
            {
                'text': 'Real Estate and Mortgage Guides',
                'localInfoLinksGroup': [
                    {'text': 'Hilmar Real Estate Guide', 'href': '/real_estate/Hilmar-California/'},
                    {'text': 'Hilmar Schools', 'href': '/real_estate/Hilmar-California/schools/'},
                    {'text': 'Newest Homes for Sale in California', 'href': '/new-for-sale-properties/CA/'},
                    {'text': 'Newest Rentals in California', 'href': '/new-for-rent-properties/CA/'},
                    {'text': 'Hilmar Mortgage', 'href': '/mortgage-rates/Hilmar,CA/'},
                    {'text': 'Hilmar Refinance', 'href': '/refinance-rates/Hilmar,CA/'}
                ]
            }
        ]
    ), (
        'https://www.trulia.com/for_sale/37.684308412501,37.812034250769,-122.48558262954,-122.32765416274_xy/',
        [
            {
                'text': 'Nearby Real Estate',
                'citiesLinkGroup': [
                    {'text': 'Houses for Sale Near Me', 'href': '/houses-for-sale-near-me'},
                    {'text': 'Houses for Sale Near Me by Owner', 'href': '/houses-for-sale-near-me-by-owner'},
                    {'text': 'Open Houses Near Me', 'href': '/open-houses-near-me'},
                    {'text': 'Land for Sale Near Me', 'href': '/land-for-sale-near-me'},
                    {'text': 'Townhomes for Sale Near Me', 'href': '/townhomes-for-sale-near-me'},
                    {'text': 'Condos for Sale Near Me', 'href': '/condos-for-sale-near-me'}
                ]
            },
            {
                'text': 'San Francisco Neighborhoods',
                'href': '/sitemaps/neighborhood/California/San_Francisco/'
            },
            {
                'text': 'San Francisco Property Types',
                'href': '/sitemaps/city-for-sale/CA/San_Francisco/',
                'propertyTypesLinkGroup': [
                    {'text': 'For Sale by Owner (FSBO)',
                     'href': '/for_sale/San_Francisco,CA/fsbo_lt/'},
                    {'text': 'Single Family Homes', 'href': '/for_sale/San_Francisco,CA/SINGLE-FAMILY_HOME_type/'},
                    {'text': 'Condos',
                     'href': '/for_sale/San_Francisco,CA/CONDO_type/'},
                    {'text': 'Townhomes', 'href': '/for_sale/San_Francisco,CA/TOWNHOUSE_type/'},
                    {'text': 'Co-Ops', 'href': '/for_sale/San_Francisco,CA/COOP_type/'},
                    {'text': 'Apartments', 'href': '/for_sale/San_Francisco,CA/APARTMENT_type/'},
                    {'text': 'Mobile/Manufactured Homes',
                     'href': '/for_sale/San_Francisco,CA/MOBILE|MANUFACTURED_type/'}
                ]
            },
            {
                'text': 'Real Estate and Mortgage Guides',
                'localInfoLinksGroup': [
                    {'text': 'San Francisco Real Estate Guide', 'href': '/real_estate/San_Francisco-California/'},
                    {'text': 'San Francisco Schools', 'href': '/real_estate/San_Francisco-California/schools/'},
                    {'text': 'Newest Homes for Sale in California', 'href': '/new-for-sale-properties/CA/'},
                    {'text': 'Newest Rentals in California', 'href': '/new-for-rent-properties/CA/'},
                    {'text': 'San Francisco Mortgage', 'href': '/mortgage-rates/San_Francisco,CA/'},
                    {'text': 'San Francisco Refinance', 'href': '/refinance-rates/San_Francisco,CA/'}
                ]
            }
        ]
    ),
    (
        'https://www.trulia.com/for_sale/29.797777039763,30.092292361495,-90.271999096805,-89.956142163212_xy/1p_beds/12_zm/',
        [
            {
                'text': 'Nearby Real Estate',
                'citiesLinkGroup': [
                    {'text': 'Houses for Sale Near Me', 'href': '/houses-for-sale-near-me'},
                    {'text': 'Houses for Sale Near Me by Owner', 'href': '/houses-for-sale-near-me-by-owner'},
                    {'text': 'Open Houses Near Me', 'href': '/open-houses-near-me'},
                    {'text': 'Land for Sale Near Me', 'href': '/land-for-sale-near-me'},
                    {'text': 'Townhomes for Sale Near Me', 'href': '/townhomes-for-sale-near-me'},
                    {'text': 'Condos for Sale Near Me', 'href': '/condos-for-sale-near-me'}
                ]
            },
            {
                'text': 'New Orleans Neighborhoods',
                'href': '/sitemaps/neighborhood/Louisiana/New_Orleans/'
            },
            {
                'text': 'New Orleans Property Types',
                'href': '/sitemaps/city-for-sale/LA/New_Orleans/',
                'propertyTypesLinkGroup': [
                    {'text': 'For Sale by Owner (FSBO)',
                     'href': '/for_sale/New_Orleans,LA/fsbo_lt/'},
                    {'text': 'Single Family Homes', 'href': '/for_sale/New_Orleans,LA/SINGLE-FAMILY_HOME_type/'},
                    {'text': 'Condos',
                     'href': '/for_sale/New_Orleans,LA/CONDO_type/'},
                    {'text': 'Townhomes', 'href': '/for_sale/New_Orleans,LA/TOWNHOUSE_type/'},
                    {'text': 'Co-Ops', 'href': '/for_sale/New_Orleans,LA/COOP_type/'},
                    {'text': 'Apartments', 'href': '/for_sale/New_Orleans,LA/APARTMENT_type/'},
                    {'text': 'Mobile/Manufactured Homes',
                     'href': '/for_sale/New_Orleans,LA/MOBILE|MANUFACTURED_type/'}
                ]
            },
            {
                'text': 'Real Estate and Mortgage Guides',
                'localInfoLinksGroup': [
                    {'text': 'New Orleans Real Estate Guide', 'href': '/real_estate/New_Orleans-Louisiana/'},
                    {'text': 'New Orleans Schools', 'href': '/real_estate/New_Orleans-Louisiana/schools/'},
                    {'text': 'Newest Homes for Sale in Louisiana', 'href': '/new-for-sale-properties/LA/'},
                    {'text': 'Newest Rentals in Louisiana', 'href': '/new-for-rent-properties/LA/'},
                    {'text': 'New Orleans Mortgage', 'href': '/mortgage-rates/New_Orleans,LA/'},
                    {'text': 'New Orleans Refinance', 'href': '/refinance-rates/New_Orleans,LA/'}
                ]
            }
        ]
    ),
    (
        'https://www.trulia.com/for_sale/San_Mateo,CA/37.514857406994,37.580526177666,-122.35212773203,-122.27316349863_xy/14_zm/',
        [
            {
                'text': 'Nearby Real Estate',
                'citiesLinkGroup': [
                    {'text': 'Houses for Sale Near Me', 'href': '/houses-for-sale-near-me'},
                    {'text': 'Houses for Sale Near Me by Owner', 'href': '/houses-for-sale-near-me-by-owner'},
                    {'text': 'Open Houses Near Me', 'href': '/open-houses-near-me'},
                    {'text': 'Land for Sale Near Me', 'href': '/land-for-sale-near-me'},
                    {'text': 'Townhomes for Sale Near Me', 'href': '/townhomes-for-sale-near-me'},
                    {'text': 'Condos for Sale Near Me', 'href': '/condos-for-sale-near-me'}
                ]
            },
            {
                'text': 'San Mateo Neighborhoods',
                'href': '/sitemaps/neighborhood/California/San_Mateo/'
            },
            {
                'text': 'San Mateo Property Types',
                'href': '/sitemaps/city-for-sale/CA/San_Mateo/',
                'propertyTypesLinkGroup': [
                    {'text': 'For Sale by Owner (FSBO)',
                     'href': '/for_sale/San_Mateo,CA/fsbo_lt/'},
                    {'text': 'Single Family Homes', 'href': '/for_sale/San_Mateo,CA/SINGLE-FAMILY_HOME_type/'},
                    {'text': 'Condos',
                     'href': '/for_sale/San_Mateo,CA/CONDO_type/'},
                    {'text': 'Townhomes', 'href': '/for_sale/San_Mateo,CA/TOWNHOUSE_type/'},
                    {'text': 'Co-Ops', 'href': '/for_sale/San_Mateo,CA/COOP_type/'},
                    {'text': 'Apartments', 'href': '/for_sale/San_Mateo,CA/APARTMENT_type/'},
                    {'text': 'Mobile/Manufactured Homes',
                     'href': '/for_sale/San_Mateo,CA/MOBILE|MANUFACTURED_type/'}
                ]
            },
            {
                'text': 'Real Estate and Mortgage Guides',
                'localInfoLinksGroup': [
                    {'text': 'San Mateo Real Estate Guide', 'href': '/real_estate/San_Mateo-California/'},
                    {'text': 'San Mateo Schools', 'href': '/real_estate/San_Mateo-California/schools/'},
                    {'text': 'Newest Homes for Sale in California', 'href': '/new-for-sale-properties/CA/'},
                    {'text': 'Newest Rentals in California', 'href': '/new-for-rent-properties/CA/'},
                    {'text': 'San Mateo Mortgage', 'href': '/mortgage-rates/San_Mateo,CA/'},
                    {'text': 'San Mateo Refinance', 'href': '/refinance-rates/San_Mateo,CA/'}
                ]
            }
        ]
    ),
    (
        'https://www.trulia.com/for_sale/37.514857406994,37.580526177666,-122.35212773203,-122.27316349863_xy/14_zm/',
        [
            {
                'text': 'Nearby Real Estate',
                'citiesLinkGroup': [
                    {'text': 'Houses for Sale Near Me', 'href': '/houses-for-sale-near-me'},
                    {'text': 'Houses for Sale Near Me by Owner', 'href': '/houses-for-sale-near-me-by-owner'},
                    {'text': 'Open Houses Near Me', 'href': '/open-houses-near-me'},
                    {'text': 'Land for Sale Near Me', 'href': '/land-for-sale-near-me'},
                    {'text': 'Townhomes for Sale Near Me', 'href': '/townhomes-for-sale-near-me'},
                    {'text': 'Condos for Sale Near Me', 'href': '/condos-for-sale-near-me'}
                ]
            },
            {
                'text': 'San Mateo Neighborhoods',
                'href': '/sitemaps/neighborhood/California/San_Mateo/'
            },
            {
                'text': 'San Mateo Property Types',
                'href': '/sitemaps/city-for-sale/CA/San_Mateo/',
                'propertyTypesLinkGroup': [
                    {'text': 'For Sale by Owner (FSBO)',
                     'href': '/for_sale/San_Mateo,CA/fsbo_lt/'},
                    {'text': 'Single Family Homes', 'href': '/for_sale/San_Mateo,CA/SINGLE-FAMILY_HOME_type/'},
                    {'text': 'Condos',
                     'href': '/for_sale/San_Mateo,CA/CONDO_type/'},
                    {'text': 'Townhomes', 'href': '/for_sale/San_Mateo,CA/TOWNHOUSE_type/'},
                    {'text': 'Co-Ops', 'href': '/for_sale/San_Mateo,CA/COOP_type/'},
                    {'text': 'Apartments', 'href': '/for_sale/San_Mateo,CA/APARTMENT_type/'},
                    {'text': 'Mobile/Manufactured Homes',
                     'href': '/for_sale/San_Mateo,CA/MOBILE|MANUFACTURED_type/'}
                ]
            },
            {
                'text': 'Real Estate and Mortgage Guides',
                'localInfoLinksGroup': [
                    {'text': 'San Mateo Real Estate Guide', 'href': '/real_estate/San_Mateo-California/'},
                    {'text': 'San Mateo Schools', 'href': '/real_estate/San_Mateo-California/schools/'},
                    {'text': 'Newest Homes for Sale in California', 'href': '/new-for-sale-properties/CA/'},
                    {'text': 'Newest Rentals in California', 'href': '/new-for-rent-properties/CA/'},
                    {'text': 'San Mateo Mortgage', 'href': '/mortgage-rates/San_Mateo,CA/'},
                    {'text': 'San Mateo Refinance', 'href': '/refinance-rates/San_Mateo,CA/'}
                ]
            }
        ]
    ),

]

for_sale_minimized_footer_test_cases = [
    (
        'https://www.trulia.com/CA/San_Francisco/',
        [
            {'text': 'About Trulia', 'href': '//www.trulia.com/about/'},
            {'text': 'About Zillow Group', 'href': '//www.zillowgroup.com/about-zillow-group/'},
            {'text': 'Careers', 'href': '//www.trulia.com/about/careers/'},
            {'text': 'Newsroom', 'href': '//www.trulia.com/newsroom/'},
            {'text': 'Investor Relations', 'href': 'http://investors.zillowgroup.com/'},
            {'text': 'Advertising Terms', 'href': '//www.trulia.com/terms/advertisers/'},
            {'text': 'Privacy', 'href': '//www.trulia.com/info/privacy/'},
            {'text': 'Terms of Use', 'href': '//www.trulia.com/info/terms/'},
            {'text': 'Listings Quality Policy', 'href': '//www.trulia.com/info/listings-quality-policy/'},
            {'text': 'Subscription Terms', 'href': 'http://www.zillow.com/corp/SATerms.htm'},
            {'text': 'Help', 'href': 'https://support.trulia.com/hc/en-us'},
            {'text': 'Ad Choice', 'href': ''}

        ]
    ),
    (
        'https://www.trulia.com/for_sale/San_Francisco,CA/p_oh/',
        [
            {'text': 'About Trulia', 'href': '//www.trulia.com/about/'},
            {'text': 'About Zillow Group', 'href': '//www.zillowgroup.com/about-zillow-group/'},
            {'text': 'Careers', 'href': '//www.trulia.com/about/careers/'},
            {'text': 'Newsroom', 'href': '//www.trulia.com/newsroom/'},
            {'text': 'Investor Relations', 'href': 'http://investors.zillowgroup.com/'},
            {'text': 'Advertising Terms', 'href': '//www.trulia.com/terms/advertisers/'},
            {'text': 'Privacy', 'href': '//www.trulia.com/info/privacy/'},
            {'text': 'Terms of Use', 'href': '//www.trulia.com/info/terms/'},
            {'text': 'Listings Quality Policy', 'href': '//www.trulia.com/info/listings-quality-policy/'},
            {'text': 'Subscription Terms', 'href': 'http://www.zillow.com/corp/SATerms.htm'},
            {'text': 'Help', 'href': 'https://support.trulia.com/hc/en-us'},
            {'text': 'Ad Choice', 'href': ''}

        ]
    ),
    (
        'https://www.trulia.com/for_sale/San_Francisco,CA/new_homes_lt/',
        [
            {'text': 'About Trulia', 'href': '//www.trulia.com/about/'},
            {'text': 'About Zillow Group', 'href': '//www.zillowgroup.com/about-zillow-group/'},
            {'text': 'Careers', 'href': '//www.trulia.com/about/careers/'},
            {'text': 'Newsroom', 'href': '//www.trulia.com/newsroom/'},
            {'text': 'Investor Relations', 'href': 'http://investors.zillowgroup.com/'},
            {'text': 'Advertising Terms', 'href': '//www.trulia.com/terms/advertisers/'},
            {'text': 'Privacy', 'href': '//www.trulia.com/info/privacy/'},
            {'text': 'Terms of Use', 'href': '//www.trulia.com/info/terms/'},
            {'text': 'Listings Quality Policy', 'href': '//www.trulia.com/info/listings-quality-policy/'},
            {'text': 'Subscription Terms', 'href': 'http://www.zillow.com/corp/SATerms.htm'},
            {'text': 'Help', 'href': 'https://support.trulia.com/hc/en-us'},
            {'text': 'Ad Choice', 'href': ''}

        ]
    ),

    (
        'https://www.trulia.com/for_sale/San_Francisco,CA/SINGLE-FAMILY_HOME_type/',
        [
            {'text': 'About Trulia', 'href': '//www.trulia.com/about/'},
            {'text': 'About Zillow Group', 'href': '//www.zillowgroup.com/about-zillow-group/'},
            {'text': 'Careers', 'href': '//www.trulia.com/about/careers/'},
            {'text': 'Newsroom', 'href': '//www.trulia.com/newsroom/'},
            {'text': 'Investor Relations', 'href': 'http://investors.zillowgroup.com/'},
            {'text': 'Advertising Terms', 'href': '//www.trulia.com/terms/advertisers/'},
            {'text': 'Privacy', 'href': '//www.trulia.com/info/privacy/'},
            {'text': 'Terms of Use', 'href': '//www.trulia.com/info/terms/'},
            {'text': 'Listings Quality Policy', 'href': '//www.trulia.com/info/listings-quality-policy/'},
            {'text': 'Subscription Terms', 'href': 'http://www.zillow.com/corp/SATerms.htm'},
            {'text': 'Help', 'href': 'https://support.trulia.com/hc/en-us'},
            {'text': 'Ad Choice', 'href': ''}

        ]
    ),
    (
        'https://www.trulia.com/for_sale/San_Francisco,CA/',
        [
            {'text': 'About Trulia', 'href': '//www.trulia.com/about/'},
            {'text': 'About Zillow Group', 'href': '//www.zillowgroup.com/about-zillow-group/'},
            {'text': 'Careers', 'href': '//www.trulia.com/about/careers/'},
            {'text': 'Newsroom', 'href': '//www.trulia.com/newsroom/'},
            {'text': 'Investor Relations', 'href': 'http://investors.zillowgroup.com/'},
            {'text': 'Advertising Terms', 'href': '//www.trulia.com/terms/advertisers/'},
            {'text': 'Privacy', 'href': '//www.trulia.com/info/privacy/'},
            {'text': 'Terms of Use', 'href': '//www.trulia.com/info/terms/'},
            {'text': 'Listings Quality Policy', 'href': '//www.trulia.com/info/listings-quality-policy/'},
            {'text': 'Subscription Terms', 'href': 'http://www.zillow.com/corp/SATerms.htm'},
            {'text': 'Help', 'href': 'https://support.trulia.com/hc/en-us'},
            {'text': 'Ad Choice', 'href': ''}

        ]
    ),
    (
        'https://www.trulia.com/for_sale/06075_c/new_homes_lt/',
        [
            {'text': 'About Trulia', 'href': '//www.trulia.com/about/'},
            {'text': 'About Zillow Group', 'href': '//www.zillowgroup.com/about-zillow-group/'},
            {'text': 'Careers', 'href': '//www.trulia.com/about/careers/'},
            {'text': 'Newsroom', 'href': '//www.trulia.com/newsroom/'},
            {'text': 'Investor Relations', 'href': 'http://investors.zillowgroup.com/'},
            {'text': 'Advertising Terms', 'href': '//www.trulia.com/terms/advertisers/'},
            {'text': 'Privacy', 'href': '//www.trulia.com/info/privacy/'},
            {'text': 'Terms of Use', 'href': '//www.trulia.com/info/terms/'},
            {'text': 'Listings Quality Policy', 'href': '//www.trulia.com/info/listings-quality-policy/'},
            {'text': 'Subscription Terms', 'href': 'http://www.zillow.com/corp/SATerms.htm'},
            {'text': 'Help', 'href': 'https://support.trulia.com/hc/en-us'},
            {'text': 'Ad Choice', 'href': ''}

        ]
    ),
    (
        'https://www.trulia.com/for_sale/06075_c/SINGLE-FAMILY_HOME_type/',
        [
            {'text': 'About Trulia', 'href': '//www.trulia.com/about/'},
            {'text': 'About Zillow Group', 'href': '//www.zillowgroup.com/about-zillow-group/'},
            {'text': 'Careers', 'href': '//www.trulia.com/about/careers/'},
            {'text': 'Newsroom', 'href': '//www.trulia.com/newsroom/'},
            {'text': 'Investor Relations', 'href': 'http://investors.zillowgroup.com/'},
            {'text': 'Advertising Terms', 'href': '//www.trulia.com/terms/advertisers/'},
            {'text': 'Privacy', 'href': '//www.trulia.com/info/privacy/'},
            {'text': 'Terms of Use', 'href': '//www.trulia.com/info/terms/'},
            {'text': 'Listings Quality Policy', 'href': '//www.trulia.com/info/listings-quality-policy/'},
            {'text': 'Subscription Terms', 'href': 'http://www.zillow.com/corp/SATerms.htm'},
            {'text': 'Help', 'href': 'https://support.trulia.com/hc/en-us'},
            {'text': 'Ad Choice', 'href': ''}

        ]
    ),
    (
        'https://www.trulia.com/for_sale/Soquel,CA/p_oh/',
        [
            {'text': 'About Trulia', 'href': '//www.trulia.com/about/'},
            {'text': 'About Zillow Group', 'href': '//www.zillowgroup.com/about-zillow-group/'},
            {'text': 'Careers', 'href': '//www.trulia.com/about/careers/'},
            {'text': 'Newsroom', 'href': '//www.trulia.com/newsroom/'},
            {'text': 'Investor Relations', 'href': 'http://investors.zillowgroup.com/'},
            {'text': 'Advertising Terms', 'href': '//www.trulia.com/terms/advertisers/'},
            {'text': 'Privacy', 'href': '//www.trulia.com/info/privacy/'},
            {'text': 'Terms of Use', 'href': '//www.trulia.com/info/terms/'},
            {'text': 'Listings Quality Policy', 'href': '//www.trulia.com/info/listings-quality-policy/'},
            {'text': 'Subscription Terms', 'href': 'http://www.zillow.com/corp/SATerms.htm'},
            {'text': 'Help', 'href': 'https://support.trulia.com/hc/en-us'},
            {'text': 'Ad Choice', 'href': ''}

        ]
    ),
    (
        'https://www.trulia.com/for_sale/Soquel,CA/p_oh/',
        [
            {'text': 'About Trulia', 'href': '//www.trulia.com/about/'},
            {'text': 'About Zillow Group', 'href': '//www.zillowgroup.com/about-zillow-group/'},
            {'text': 'Careers', 'href': '//www.trulia.com/about/careers/'},
            {'text': 'Newsroom', 'href': '//www.trulia.com/newsroom/'},
            {'text': 'Investor Relations', 'href': 'http://investors.zillowgroup.com/'},
            {'text': 'Advertising Terms', 'href': '//www.trulia.com/terms/advertisers/'},
            {'text': 'Privacy', 'href': '//www.trulia.com/info/privacy/'},
            {'text': 'Terms of Use', 'href': '//www.trulia.com/info/terms/'},
            {'text': 'Listings Quality Policy', 'href': '//www.trulia.com/info/listings-quality-policy/'},
            {'text': 'Subscription Terms', 'href': 'http://www.zillow.com/corp/SATerms.htm'},
            {'text': 'Help', 'href': 'https://support.trulia.com/hc/en-us'},
            {'text': 'Ad Choice', 'href': ''}

        ]
    ),
    (
        'https://www.trulia.com/for_sale/1468_nh/new_homes_lt/',
        [
            {'text': 'About Trulia', 'href': '//www.trulia.com/about/'},
            {'text': 'About Zillow Group', 'href': '//www.zillowgroup.com/about-zillow-group/'},
            {'text': 'Careers', 'href': '//www.trulia.com/about/careers/'},
            {'text': 'Newsroom', 'href': '//www.trulia.com/newsroom/'},
            {'text': 'Investor Relations', 'href': 'http://investors.zillowgroup.com/'},
            {'text': 'Advertising Terms', 'href': '//www.trulia.com/terms/advertisers/'},
            {'text': 'Privacy', 'href': '//www.trulia.com/info/privacy/'},
            {'text': 'Terms of Use', 'href': '//www.trulia.com/info/terms/'},
            {'text': 'Listings Quality Policy', 'href': '//www.trulia.com/info/listings-quality-policy/'},
            {'text': 'Subscription Terms', 'href': 'http://www.zillow.com/corp/SATerms.htm'},
            {'text': 'Help', 'href': 'https://support.trulia.com/hc/en-us'},
            {'text': 'Ad Choice', 'href': ''}

        ]
    ),
    (
        'https://www.trulia.com/for_sale/1468_nh/p_oh/',
        [
            {'text': 'About Trulia', 'href': '//www.trulia.com/about/'},
            {'text': 'About Zillow Group', 'href': '//www.zillowgroup.com/about-zillow-group/'},
            {'text': 'Careers', 'href': '//www.trulia.com/about/careers/'},
            {'text': 'Newsroom', 'href': '//www.trulia.com/newsroom/'},
            {'text': 'Investor Relations', 'href': 'http://investors.zillowgroup.com/'},
            {'text': 'Advertising Terms', 'href': '//www.trulia.com/terms/advertisers/'},
            {'text': 'Privacy', 'href': '//www.trulia.com/info/privacy/'},
            {'text': 'Terms of Use', 'href': '//www.trulia.com/info/terms/'},
            {'text': 'Listings Quality Policy', 'href': '//www.trulia.com/info/listings-quality-policy/'},
            {'text': 'Subscription Terms', 'href': 'http://www.zillow.com/corp/SATerms.htm'},
            {'text': 'Help', 'href': 'https://support.trulia.com/hc/en-us'},
            {'text': 'Ad Choice', 'href': ''}

        ]
    ),
    (
        'https://www.trulia.com/for_sale/06075_c/fsbo_lt/',
        [
            {'text': 'About Trulia', 'href': '//www.trulia.com/about/'},
            {'text': 'About Zillow Group', 'href': '//www.zillowgroup.com/about-zillow-group/'},
            {'text': 'Careers', 'href': '//www.trulia.com/about/careers/'},
            {'text': 'Newsroom', 'href': '//www.trulia.com/newsroom/'},
            {'text': 'Investor Relations', 'href': 'http://investors.zillowgroup.com/'},
            {'text': 'Advertising Terms', 'href': '//www.trulia.com/terms/advertisers/'},
            {'text': 'Privacy', 'href': '//www.trulia.com/info/privacy/'},
            {'text': 'Terms of Use', 'href': '//www.trulia.com/info/terms/'},
            {'text': 'Listings Quality Policy', 'href': '//www.trulia.com/info/listings-quality-policy/'},
            {'text': 'Subscription Terms', 'href': 'http://www.zillow.com/corp/SATerms.htm'},
            {'text': 'Help', 'href': 'https://support.trulia.com/hc/en-us'},
            {'text': 'Ad Choice', 'href': ''}

        ]
    ),
    (
        'https://www.trulia.com/for_sale/San_Francisco,CA/fsbo_lt/',
        [
            {'text': 'About Trulia', 'href': '//www.trulia.com/about/'},
            {'text': 'About Zillow Group', 'href': '//www.zillowgroup.com/about-zillow-group/'},
            {'text': 'Careers', 'href': '//www.trulia.com/about/careers/'},
            {'text': 'Newsroom', 'href': '//www.trulia.com/newsroom/'},
            {'text': 'Investor Relations', 'href': 'http://investors.zillowgroup.com/'},
            {'text': 'Advertising Terms', 'href': '//www.trulia.com/terms/advertisers/'},
            {'text': 'Privacy', 'href': '//www.trulia.com/info/privacy/'},
            {'text': 'Terms of Use', 'href': '//www.trulia.com/info/terms/'},
            {'text': 'Listings Quality Policy', 'href': '//www.trulia.com/info/listings-quality-policy/'},
            {'text': 'Subscription Terms', 'href': 'http://www.zillow.com/corp/SATerms.htm'},
            {'text': 'Help', 'href': 'https://support.trulia.com/hc/en-us'},
            {'text': 'Ad Choice', 'href': ''}

        ]
    ),
    (
        'https://www.trulia.com/for_sale/San_Francisco,CA/foreclosure_lt/',
        [
            {'text': 'About Trulia', 'href': '//www.trulia.com/about/'},
            {'text': 'About Zillow Group', 'href': '//www.zillowgroup.com/about-zillow-group/'},
            {'text': 'Careers', 'href': '//www.trulia.com/about/careers/'},
            {'text': 'Newsroom', 'href': '//www.trulia.com/newsroom/'},
            {'text': 'Investor Relations', 'href': 'http://investors.zillowgroup.com/'},
            {'text': 'Advertising Terms', 'href': '//www.trulia.com/terms/advertisers/'},
            {'text': 'Privacy', 'href': '//www.trulia.com/info/privacy/'},
            {'text': 'Terms of Use', 'href': '//www.trulia.com/info/terms/'},
            {'text': 'Listings Quality Policy', 'href': '//www.trulia.com/info/listings-quality-policy/'},
            {'text': 'Subscription Terms', 'href': 'http://www.zillow.com/corp/SATerms.htm'},
            {'text': 'Help', 'href': 'https://support.trulia.com/hc/en-us'},
            {'text': 'Ad Choice', 'href': ''}

        ]
    ),
    (
        'https://www.trulia.com/for_sale/06075_c/foreclosure_lt/',
        [
            {'text': 'About Trulia', 'href': '//www.trulia.com/about/'},
            {'text': 'About Zillow Group', 'href': '//www.zillowgroup.com/about-zillow-group/'},
            {'text': 'Careers', 'href': '//www.trulia.com/about/careers/'},
            {'text': 'Newsroom', 'href': '//www.trulia.com/newsroom/'},
            {'text': 'Investor Relations', 'href': 'http://investors.zillowgroup.com/'},
            {'text': 'Advertising Terms', 'href': '//www.trulia.com/terms/advertisers/'},
            {'text': 'Privacy', 'href': '//www.trulia.com/info/privacy/'},
            {'text': 'Terms of Use', 'href': '//www.trulia.com/info/terms/'},
            {'text': 'Listings Quality Policy', 'href': '//www.trulia.com/info/listings-quality-policy/'},
            {'text': 'Subscription Terms', 'href': 'http://www.zillow.com/corp/SATerms.htm'},
            {'text': 'Help', 'href': 'https://support.trulia.com/hc/en-us'},
            {'text': 'Ad Choice', 'href': ''}

        ]
    ),
    (
        'https://www.trulia.com/for_sale/95073_zip/foreclosure_lt/',
        [
            {'text': 'About Trulia', 'href': '//www.trulia.com/about/'},
            {'text': 'About Zillow Group', 'href': '//www.zillowgroup.com/about-zillow-group/'},
            {'text': 'Careers', 'href': '//www.trulia.com/about/careers/'},
            {'text': 'Newsroom', 'href': '//www.trulia.com/newsroom/'},
            {'text': 'Investor Relations', 'href': 'http://investors.zillowgroup.com/'},
            {'text': 'Advertising Terms', 'href': '//www.trulia.com/terms/advertisers/'},
            {'text': 'Privacy', 'href': '//www.trulia.com/info/privacy/'},
            {'text': 'Terms of Use', 'href': '//www.trulia.com/info/terms/'},
            {'text': 'Listings Quality Policy', 'href': '//www.trulia.com/info/listings-quality-policy/'},
            {'text': 'Subscription Terms', 'href': 'http://www.zillow.com/corp/SATerms.htm'},
            {'text': 'Help', 'href': 'https://support.trulia.com/hc/en-us'},
            {'text': 'Ad Choice', 'href': ''}

        ]
    ),
    (
        'https://www.trulia.com/for_sale/New_York,NY/resale_lt/',
        [
            {'text': 'About Trulia', 'href': '//www.trulia.com/about/'},
            {'text': 'About Zillow Group', 'href': '//www.zillowgroup.com/about-zillow-group/'},
            {'text': 'Careers', 'href': '//www.trulia.com/about/careers/'},
            {'text': 'Newsroom', 'href': '//www.trulia.com/newsroom/'},
            {'text': 'Investor Relations', 'href': 'http://investors.zillowgroup.com/'},
            {'text': 'Advertising Terms', 'href': '//www.trulia.com/terms/advertisers/'},
            {'text': 'Privacy', 'href': '//www.trulia.com/info/privacy/'},
            {'text': 'Terms of Use', 'href': '//www.trulia.com/info/terms/'},
            {'text': 'Listings Quality Policy', 'href': '//www.trulia.com/info/listings-quality-policy/'},
            {'text': 'Subscription Terms', 'href': 'http://www.zillow.com/corp/SATerms.htm'},
            {'text': 'Help', 'href': 'https://support.trulia.com/hc/en-us'},
            {'text': 'Ad Choice', 'href': ''}

        ]
    ),
    (
        'https://www.trulia.com/for_sale/New_York,NY/foreclosure,fsbo,new_homes,resale,sale_pending_lt/',
        [
            {'text': 'About Trulia', 'href': '//www.trulia.com/about/'},
            {'text': 'About Zillow Group', 'href': '//www.zillowgroup.com/about-zillow-group/'},
            {'text': 'Careers', 'href': '//www.trulia.com/about/careers/'},
            {'text': 'Newsroom', 'href': '//www.trulia.com/newsroom/'},
            {'text': 'Investor Relations', 'href': 'http://investors.zillowgroup.com/'},
            {'text': 'Advertising Terms', 'href': '//www.trulia.com/terms/advertisers/'},
            {'text': 'Privacy', 'href': '//www.trulia.com/info/privacy/'},
            {'text': 'Terms of Use', 'href': '//www.trulia.com/info/terms/'},
            {'text': 'Listings Quality Policy', 'href': '//www.trulia.com/info/listings-quality-policy/'},
            {'text': 'Subscription Terms', 'href': 'http://www.zillow.com/corp/SATerms.htm'},
            {'text': 'Help', 'href': 'https://support.trulia.com/hc/en-us'},
            {'text': 'Ad Choice', 'href': ''}

        ]
    ),

    (
        'https://www.trulia.com/for_sale/San_Mateo,CA/37.514857406994,37.580526177666,-122.35212773203,-122.27316349863_xy/14_zm/',
        [
            {'text': 'About Trulia', 'href': '//www.trulia.com/about/'},
            {'text': 'About Zillow Group', 'href': '//www.zillowgroup.com/about-zillow-group/'},
            {'text': 'Careers', 'href': '//www.trulia.com/about/careers/'},
            {'text': 'Newsroom', 'href': '//www.trulia.com/newsroom/'},
            {'text': 'Investor Relations', 'href': 'http://investors.zillowgroup.com/'},
            {'text': 'Advertising Terms', 'href': '//www.trulia.com/terms/advertisers/'},
            {'text': 'Privacy', 'href': '//www.trulia.com/info/privacy/'},
            {'text': 'Terms of Use', 'href': '//www.trulia.com/info/terms/'},
            {'text': 'Listings Quality Policy', 'href': '//www.trulia.com/info/listings-quality-policy/'},
            {'text': 'Subscription Terms', 'href': 'http://www.zillow.com/corp/SATerms.htm'},
            {'text': 'Help', 'href': 'https://support.trulia.com/hc/en-us'},
            {'text': 'Ad Choice', 'href': ''}

        ]
    ),

    (
        'https://www.trulia.com/NY/New_York/',
        [
            {'text': 'About Trulia', 'href': '//www.trulia.com/about/'},
            {'text': 'About Zillow Group', 'href': '//www.zillowgroup.com/about-zillow-group/'},
            {'text': 'Careers', 'href': '//www.trulia.com/about/careers/'},
            {'text': 'Newsroom', 'href': '//www.trulia.com/newsroom/'},
            {'text': 'Investor Relations', 'href': 'http://investors.zillowgroup.com/'},
            {'text': 'Advertising Terms', 'href': '//www.trulia.com/terms/advertisers/'},
            {'text': 'Privacy', 'href': '//www.trulia.com/info/privacy/'},
            {'text': 'Terms of Use', 'href': '//www.trulia.com/info/terms/'},
            {'text': 'Listings Quality Policy', 'href': '//www.trulia.com/info/listings-quality-policy/'},
            {'text': 'Subscription Terms', 'href': 'http://www.zillow.com/corp/SATerms.htm'},
            {'text': 'Help', 'href': 'https://support.trulia.com/hc/en-us'},
            {'text': 'Ad Choice', 'href': ''}

        ]
    ),
    (
        'https://www.trulia.com/for_sale/94546_zip/new_homes_lt/',
        [
            {'text': 'About Trulia', 'href': '//www.trulia.com/about/'},
            {'text': 'About Zillow Group', 'href': '//www.zillowgroup.com/about-zillow-group/'},
            {'text': 'Careers', 'href': '//www.trulia.com/about/careers/'},
            {'text': 'Newsroom', 'href': '//www.trulia.com/newsroom/'},
            {'text': 'Investor Relations', 'href': 'http://investors.zillowgroup.com/'},
            {'text': 'Advertising Terms', 'href': '//www.trulia.com/terms/advertisers/'},
            {'text': 'Privacy', 'href': '//www.trulia.com/info/privacy/'},
            {'text': 'Terms of Use', 'href': '//www.trulia.com/info/terms/'},
            {'text': 'Listings Quality Policy', 'href': '//www.trulia.com/info/listings-quality-policy/'},
            {'text': 'Subscription Terms', 'href': 'http://www.zillow.com/corp/SATerms.htm'},
            {'text': 'Help', 'href': 'https://support.trulia.com/hc/en-us'},
            {'text': 'Ad Choice', 'href': ''}

        ]
    ),
    (
        'https://www.trulia.com/for_sale/1468_nh/SINGLE-FAMILY_HOME_type/',
        [
            {'text': 'About Trulia', 'href': '//www.trulia.com/about/'},
            {'text': 'About Zillow Group', 'href': '//www.zillowgroup.com/about-zillow-group/'},
            {'text': 'Careers', 'href': '//www.trulia.com/about/careers/'},
            {'text': 'Newsroom', 'href': '//www.trulia.com/newsroom/'},
            {'text': 'Investor Relations', 'href': 'http://investors.zillowgroup.com/'},
            {'text': 'Advertising Terms', 'href': '//www.trulia.com/terms/advertisers/'},
            {'text': 'Privacy', 'href': '//www.trulia.com/info/privacy/'},
            {'text': 'Terms of Use', 'href': '//www.trulia.com/info/terms/'},
            {'text': 'Listings Quality Policy', 'href': '//www.trulia.com/info/listings-quality-policy/'},
            {'text': 'Subscription Terms', 'href': 'http://www.zillow.com/corp/SATerms.htm'},
            {'text': 'Help', 'href': 'https://support.trulia.com/hc/en-us'},
            {'text': 'Ad Choice', 'href': ''}

        ]
    ),
    (
        'https://www.trulia.com/for_sale/1468_nh/p_oh/',
        [
            {'text': 'About Trulia', 'href': '//www.trulia.com/about/'},
            {'text': 'About Zillow Group', 'href': '//www.zillowgroup.com/about-zillow-group/'},
            {'text': 'Careers', 'href': '//www.trulia.com/about/careers/'},
            {'text': 'Newsroom', 'href': '//www.trulia.com/newsroom/'},
            {'text': 'Investor Relations', 'href': 'http://investors.zillowgroup.com/'},
            {'text': 'Advertising Terms', 'href': '//www.trulia.com/terms/advertisers/'},
            {'text': 'Privacy', 'href': '//www.trulia.com/info/privacy/'},
            {'text': 'Terms of Use', 'href': '//www.trulia.com/info/terms/'},
            {'text': 'Listings Quality Policy', 'href': '//www.trulia.com/info/listings-quality-policy/'},
            {'text': 'Subscription Terms', 'href': 'http://www.zillow.com/corp/SATerms.htm'},
            {'text': 'Help', 'href': 'https://support.trulia.com/hc/en-us'},
            {'text': 'Ad Choice', 'href': ''}

        ]
    ),
    (
        'https://www.trulia.com/for_sale/1468_nh/fsbo_lt/',
        [
            {'text': 'About Trulia', 'href': '//www.trulia.com/about/'},
            {'text': 'About Zillow Group', 'href': '//www.zillowgroup.com/about-zillow-group/'},
            {'text': 'Careers', 'href': '//www.trulia.com/about/careers/'},
            {'text': 'Newsroom', 'href': '//www.trulia.com/newsroom/'},
            {'text': 'Investor Relations', 'href': 'http://investors.zillowgroup.com/'},
            {'text': 'Advertising Terms', 'href': '//www.trulia.com/terms/advertisers/'},
            {'text': 'Privacy', 'href': '//www.trulia.com/info/privacy/'},
            {'text': 'Terms of Use', 'href': '//www.trulia.com/info/terms/'},
            {'text': 'Listings Quality Policy', 'href': '//www.trulia.com/info/listings-quality-policy/'},
            {'text': 'Subscription Terms', 'href': 'http://www.zillow.com/corp/SATerms.htm'},
            {'text': 'Help', 'href': 'https://support.trulia.com/hc/en-us'},
            {'text': 'Ad Choice', 'href': ''}

        ]
    ),
    (
        'https://www.trulia.com/for_sale/94546_zip/fsbo_lt/',
        [
            {'text': 'About Trulia', 'href': '//www.trulia.com/about/'},
            {'text': 'About Zillow Group', 'href': '//www.zillowgroup.com/about-zillow-group/'},
            {'text': 'Careers', 'href': '//www.trulia.com/about/careers/'},
            {'text': 'Newsroom', 'href': '//www.trulia.com/newsroom/'},
            {'text': 'Investor Relations', 'href': 'http://investors.zillowgroup.com/'},
            {'text': 'Advertising Terms', 'href': '//www.trulia.com/terms/advertisers/'},
            {'text': 'Privacy', 'href': '//www.trulia.com/info/privacy/'},
            {'text': 'Terms of Use', 'href': '//www.trulia.com/info/terms/'},
            {'text': 'Listings Quality Policy', 'href': '//www.trulia.com/info/listings-quality-policy/'},
            {'text': 'Subscription Terms', 'href': 'http://www.zillow.com/corp/SATerms.htm'},
            {'text': 'Help', 'href': 'https://support.trulia.com/hc/en-us'},
            {'text': 'Ad Choice', 'href': ''}

        ]
    ),
    (
        'https://www.trulia.com/for_sale/1468_nh/foreclosure_lt/',
        [
            {'text': 'About Trulia', 'href': '//www.trulia.com/about/'},
            {'text': 'About Zillow Group', 'href': '//www.zillowgroup.com/about-zillow-group/'},
            {'text': 'Careers', 'href': '//www.trulia.com/about/careers/'},
            {'text': 'Newsroom', 'href': '//www.trulia.com/newsroom/'},
            {'text': 'Investor Relations', 'href': 'http://investors.zillowgroup.com/'},
            {'text': 'Advertising Terms', 'href': '//www.trulia.com/terms/advertisers/'},
            {'text': 'Privacy', 'href': '//www.trulia.com/info/privacy/'},
            {'text': 'Terms of Use', 'href': '//www.trulia.com/info/terms/'},
            {'text': 'Listings Quality Policy', 'href': '//www.trulia.com/info/listings-quality-policy/'},
            {'text': 'Subscription Terms', 'href': 'http://www.zillow.com/corp/SATerms.htm'},
            {'text': 'Help', 'href': 'https://support.trulia.com/hc/en-us'},
            {'text': 'Ad Choice', 'href': ''}

        ]
    ),
    (
        'https://www.trulia.com/for_sale/San_Francisco,CA/english-cottages/',
        [
            {'text': 'About Trulia', 'href': '//www.trulia.com/about/'},
            {'text': 'About Zillow Group', 'href': '//www.zillowgroup.com/about-zillow-group/'},
            {'text': 'Careers', 'href': '//www.trulia.com/about/careers/'},
            {'text': 'Newsroom', 'href': '//www.trulia.com/newsroom/'},
            {'text': 'Investor Relations', 'href': 'http://investors.zillowgroup.com/'},
            {'text': 'Advertising Terms', 'href': '//www.trulia.com/terms/advertisers/'},
            {'text': 'Privacy', 'href': '//www.trulia.com/info/privacy/'},
            {'text': 'Terms of Use', 'href': '//www.trulia.com/info/terms/'},
            {'text': 'Listings Quality Policy', 'href': '//www.trulia.com/info/listings-quality-policy/'},
            {'text': 'Subscription Terms', 'href': 'http://www.zillow.com/corp/SATerms.htm'},
            {'text': 'Help', 'href': 'https://support.trulia.com/hc/en-us'},
            {'text': 'Ad Choice', 'href': ''}

        ]
    ),
    (
        'https://www.trulia.com/for_sale/1468_nh/p_oh/',
        [
            {'text': 'About Trulia', 'href': '//www.trulia.com/about/'},
            {'text': 'About Zillow Group', 'href': '//www.zillowgroup.com/about-zillow-group/'},
            {'text': 'Careers', 'href': '//www.trulia.com/about/careers/'},
            {'text': 'Newsroom', 'href': '//www.trulia.com/newsroom/'},
            {'text': 'Investor Relations', 'href': 'http://investors.zillowgroup.com/'},
            {'text': 'Advertising Terms', 'href': '//www.trulia.com/terms/advertisers/'},
            {'text': 'Privacy', 'href': '//www.trulia.com/info/privacy/'},
            {'text': 'Terms of Use', 'href': '//www.trulia.com/info/terms/'},
            {'text': 'Listings Quality Policy', 'href': '//www.trulia.com/info/listings-quality-policy/'},
            {'text': 'Subscription Terms', 'href': 'http://www.zillow.com/corp/SATerms.htm'},
            {'text': 'Help', 'href': 'https://support.trulia.com/hc/en-us'},
            {'text': 'Ad Choice', 'href': ''}

        ]
    ),
    (
        'https://www.trulia.com/for_sale/37.538050449811,38.04863829018,-122.31357480293,-121.68186093574_xy/SINGLE-FAMILY_HOME_type/11_zm/',
        [
            {'text': 'About Trulia', 'href': '//www.trulia.com/about/'},
            {'text': 'About Zillow Group', 'href': '//www.zillowgroup.com/about-zillow-group/'},
            {'text': 'Careers', 'href': '//www.trulia.com/about/careers/'},
            {'text': 'Newsroom', 'href': '//www.trulia.com/newsroom/'},
            {'text': 'Investor Relations', 'href': 'http://investors.zillowgroup.com/'},
            {'text': 'Advertising Terms', 'href': '//www.trulia.com/terms/advertisers/'},
            {'text': 'Privacy', 'href': '//www.trulia.com/info/privacy/'},
            {'text': 'Terms of Use', 'href': '//www.trulia.com/info/terms/'},
            {'text': 'Listings Quality Policy', 'href': '//www.trulia.com/info/listings-quality-policy/'},
            {'text': 'Subscription Terms', 'href': 'http://www.zillow.com/corp/SATerms.htm'},
            {'text': 'Help', 'href': 'https://support.trulia.com/hc/en-us'},
            {'text': 'Ad Choice', 'href': ''}

        ]
    ),
    (
        'https://www.trulia.com/for_sale/36.861754825027,37.888660248935,-121.57103313202,-120.30760539765_xy/foreclosure_lt/10_zm/',
        [
            {'text': 'About Trulia', 'href': '//www.trulia.com/about/'},
            {'text': 'About Zillow Group', 'href': '//www.zillowgroup.com/about-zillow-group/'},
            {'text': 'Careers', 'href': '//www.trulia.com/about/careers/'},
            {'text': 'Newsroom', 'href': '//www.trulia.com/newsroom/'},
            {'text': 'Investor Relations', 'href': 'http://investors.zillowgroup.com/'},
            {'text': 'Advertising Terms', 'href': '//www.trulia.com/terms/advertisers/'},
            {'text': 'Privacy', 'href': '//www.trulia.com/info/privacy/'},
            {'text': 'Terms of Use', 'href': '//www.trulia.com/info/terms/'},
            {'text': 'Listings Quality Policy', 'href': '//www.trulia.com/info/listings-quality-policy/'},
            {'text': 'Subscription Terms', 'href': 'http://www.zillow.com/corp/SATerms.htm'},
            {'text': 'Help', 'href': 'https://support.trulia.com/hc/en-us'},
            {'text': 'Ad Choice', 'href': ''}

        ]
    ),
    (
        'https://www.trulia.com/for_sale/37.684308412501,37.812034250769,-122.48558262954,-122.32765416274_xy/',
        [
            {'text': 'About Trulia', 'href': '//www.trulia.com/about/'},
            {'text': 'About Zillow Group', 'href': '//www.zillowgroup.com/about-zillow-group/'},
            {'text': 'Careers', 'href': '//www.trulia.com/about/careers/'},
            {'text': 'Newsroom', 'href': '//www.trulia.com/newsroom/'},
            {'text': 'Investor Relations', 'href': 'http://investors.zillowgroup.com/'},
            {'text': 'Advertising Terms', 'href': '//www.trulia.com/terms/advertisers/'},
            {'text': 'Privacy', 'href': '//www.trulia.com/info/privacy/'},
            {'text': 'Terms of Use', 'href': '//www.trulia.com/info/terms/'},
            {'text': 'Listings Quality Policy', 'href': '//www.trulia.com/info/listings-quality-policy/'},
            {'text': 'Subscription Terms', 'href': 'http://www.zillow.com/corp/SATerms.htm'},
            {'text': 'Help', 'href': 'https://support.trulia.com/hc/en-us'},
            {'text': 'Ad Choice', 'href': ''}

        ]
    ),
    (
        'https://www.trulia.com/for_sale/29.797777039763,30.092292361495,-90.271999096805,-89.956142163212_xy/1p_beds/12_zm/',
        [
            {'text': 'About Trulia', 'href': '//www.trulia.com/about/'},
            {'text': 'About Zillow Group', 'href': '//www.zillowgroup.com/about-zillow-group/'},
            {'text': 'Careers', 'href': '//www.trulia.com/about/careers/'},
            {'text': 'Newsroom', 'href': '//www.trulia.com/newsroom/'},
            {'text': 'Investor Relations', 'href': 'http://investors.zillowgroup.com/'},
            {'text': 'Advertising Terms', 'href': '//www.trulia.com/terms/advertisers/'},
            {'text': 'Privacy', 'href': '//www.trulia.com/info/privacy/'},
            {'text': 'Terms of Use', 'href': '//www.trulia.com/info/terms/'},
            {'text': 'Listings Quality Policy', 'href': '//www.trulia.com/info/listings-quality-policy/'},
            {'text': 'Subscription Terms', 'href': 'http://www.zillow.com/corp/SATerms.htm'},
            {'text': 'Help', 'href': 'https://support.trulia.com/hc/en-us'},
            {'text': 'Ad Choice', 'href': ''}

        ]
    ),
    (
        'https://www.trulia.com/for_sale/San_Mateo,CA/37.514857406994,37.580526177666,-122.35212773203,-122.27316349863_xy/14_zm/',
        [
            {'text': 'About Trulia', 'href': '//www.trulia.com/about/'},
            {'text': 'About Zillow Group', 'href': '//www.zillowgroup.com/about-zillow-group/'},
            {'text': 'Careers', 'href': '//www.trulia.com/about/careers/'},
            {'text': 'Newsroom', 'href': '//www.trulia.com/newsroom/'},
            {'text': 'Investor Relations', 'href': 'http://investors.zillowgroup.com/'},
            {'text': 'Advertising Terms', 'href': '//www.trulia.com/terms/advertisers/'},
            {'text': 'Privacy', 'href': '//www.trulia.com/info/privacy/'},
            {'text': 'Terms of Use', 'href': '//www.trulia.com/info/terms/'},
            {'text': 'Listings Quality Policy', 'href': '//www.trulia.com/info/listings-quality-policy/'},
            {'text': 'Subscription Terms', 'href': 'http://www.zillow.com/corp/SATerms.htm'},
            {'text': 'Help', 'href': 'https://support.trulia.com/hc/en-us'},
            {'text': 'Ad Choice', 'href': ''}

        ]
    ),
    (
        'https://www.trulia.com/for_sale/37.514857406994,37.580526177666,-122.35212773203,-122.27316349863_xy/14_zm/',
        [
            {'text': 'About Trulia', 'href': '//www.trulia.com/about/'},
            {'text': 'About Zillow Group', 'href': '//www.zillowgroup.com/about-zillow-group/'},
            {'text': 'Careers', 'href': '//www.trulia.com/about/careers/'},
            {'text': 'Newsroom', 'href': '//www.trulia.com/newsroom/'},
            {'text': 'Investor Relations', 'href': 'http://investors.zillowgroup.com/'},
            {'text': 'Advertising Terms', 'href': '//www.trulia.com/terms/advertisers/'},
            {'text': 'Privacy', 'href': '//www.trulia.com/info/privacy/'},
            {'text': 'Terms of Use', 'href': '//www.trulia.com/info/terms/'},
            {'text': 'Listings Quality Policy', 'href': '//www.trulia.com/info/listings-quality-policy/'},
            {'text': 'Subscription Terms', 'href': 'http://www.zillow.com/corp/SATerms.htm'},
            {'text': 'Help', 'href': 'https://support.trulia.com/hc/en-us'},
            {'text': 'Ad Choice', 'href': ''}

        ]
    ),
    (
        'https://www.trulia.com/for_sale/600088777_sd/',
        [
            {'text': 'About Trulia', 'href': '//www.trulia.com/about/'},
            {'text': 'About Zillow Group', 'href': '//www.zillowgroup.com/about-zillow-group/'},
            {'text': 'Careers', 'href': '//www.trulia.com/about/careers/'},
            {'text': 'Newsroom', 'href': '//www.trulia.com/newsroom/'},
            {'text': 'Investor Relations', 'href': 'http://investors.zillowgroup.com/'},
            {'text': 'Advertising Terms', 'href': '//www.trulia.com/terms/advertisers/'},
            {'text': 'Privacy', 'href': '//www.trulia.com/info/privacy/'},
            {'text': 'Terms of Use', 'href': '//www.trulia.com/info/terms/'},
            {'text': 'Listings Quality Policy', 'href': '//www.trulia.com/info/listings-quality-policy/'},
            {'text': 'Subscription Terms', 'href': 'http://www.zillow.com/corp/SATerms.htm'},
            {'text': 'Help', 'href': 'https://support.trulia.com/hc/en-us'},
            {'text': 'Ad Choice', 'href': ''}

        ]
    ),
    (
        'https://www.trulia.com/for_sale/5700044525_sd/',
        [
            {'text': 'About Trulia', 'href': '//www.trulia.com/about/'},
            {'text': 'About Zillow Group', 'href': '//www.zillowgroup.com/about-zillow-group/'},
            {'text': 'Careers', 'href': '//www.trulia.com/about/careers/'},
            {'text': 'Newsroom', 'href': '//www.trulia.com/newsroom/'},
            {'text': 'Investor Relations', 'href': 'http://investors.zillowgroup.com/'},
            {'text': 'Advertising Terms', 'href': '//www.trulia.com/terms/advertisers/'},
            {'text': 'Privacy', 'href': '//www.trulia.com/info/privacy/'},
            {'text': 'Terms of Use', 'href': '//www.trulia.com/info/terms/'},
            {'text': 'Listings Quality Policy', 'href': '//www.trulia.com/info/listings-quality-policy/'},
            {'text': 'Subscription Terms', 'href': 'http://www.zillow.com/corp/SATerms.htm'},
            {'text': 'Help', 'href': 'https://support.trulia.com/hc/en-us'},
            {'text': 'Ad Choice', 'href': ''}

        ]
    ),
]
