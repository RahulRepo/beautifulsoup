# =================================
#
# Test Suites template:
#
#      page_test_cases= [
#         (
#             URL
#         ),
#         (
#             URL
#        ),
#         ......
#      ]
#
#      navbar_test_cases= [
#         (
#             URL,
#             [navbar_menu_links]
#         ),
#         (
#             URL,
#             [navbar_menu_links]
#        ),
#         ......
#      ]
#
#      footer_test_cases= [
#         (
#             URL,
#             [footer_link_group]
#         ),
#         (
#             URL,
#             [footer_link_group]
#         ),
#         ......
#      ]
#
#      minimized_footer_test_cases= [
#         (
#             URL,
#             [minimized_footer_link_group]
#         ),
#         (
#             URL,
#             [minimized_footer_link_group]
#         ),
#        ......
#     ]
#
# =================================

off_market_page_test_cases = [
    (
        'https://www.trulia.com/sold/San_Francisco,CA/'
    ),
    (
        'https://www.trulia.com/sold/San_Francisco,CA/SINGLE-FAMILY_HOME_type/6_srl/'
    )
]

off_market_navbar_test_cases = [
    (
        'https://www.trulia.com/sold/San_Francisco,CA/',
        [
            {
                'sectionHeaderText': 'Buy',
                'href': '/CA/San_Francisco/',
                'dropDown': [
                    {
                        'dropDownHeaderText': 'San Francisco',
                        'dropDownList': [
                            {'text': 'Homes for Sale', 'href': '/CA/San_Francisco/'},
                            {'text': 'Open Houses', 'href': '/for_sale/San_Francisco,CA/p_oh/'},
                            {'text': 'New Homes', 'href': '/for_sale/San_Francisco,CA/new_homes_lt/'},
                            {'text': 'Recently Sold', 'href': '/sold/San_Francisco,CA/'}
                        ]
                    },
                    {'text': 'See Newest Listings', 'href': '/new-for-sale-properties/CA/'}
                ]
            },
            {
                'sectionHeaderText': 'Rent',
                'href': '/for_rent/San_Francisco,CA/',
                'dropDown': [
                    {
                        'dropDownHeaderText': 'San Francisco',
                        'dropDownList': [
                            {'text': 'All Rentals', 'href': '/for_rent/San_Francisco,CA/'},
                            {'text': 'Apartments for Rent', 'href': '/for_rent/San_Francisco,CA/APARTMENT,APARTMENT_COMMUNITY,APARTMENT%7CCONDO%7CTOWNHOUSE,CONDO,COOP,LOFT,TIC_type/'},
                            {'text': 'Houses for Rent', 'href': '/for_rent/San_Francisco,CA/SINGLE-FAMILY_HOME_type/'},
                            {'text': 'Rooms for Rent', 'href': '/for_rent/San_Francisco,CA/ROOM_FOR_RENT_type/'}
                        ]
                    },
                    {'text': 'See Newest Listings', 'href': '/new-for-rent-properties/CA/'},
                    {'text': 'Post A Rental Listing', 'href': '/post-rental/'}
                ]
            },
            {
                'sectionHeaderText': 'Mortgage',
                'href': '/mortgages/',
                'dropDown': [
                    {
                        'dropDownHeaderText': 'San Francisco',
                        'dropDownList': [
                            {'text': 'Mortgage Overview', 'href': '/mortgages/'},
                            {'text': 'Get Pre-Qualified', 'href': '/mortgages/pre-approval?omni_src=mortgage%7Cglobal_nav'},
                            {'text': 'Mortgage Rates', 'href': '/mortgage-rates/San_Francisco,CA/'},
                            {'text': 'Refinance Rates', 'href': '/refinance-rates/San_Francisco,CA/'}
                        ]
                    },
                    {'text': 'Mortgage Calculator', 'href': '/mortgage-payment-calculator/'},
                    {'text': 'Affordability Calculator', 'href': '/house-affordability-calculator/'},
                    {'text': 'Rent vs Buy Calculator', 'href': '/rent_vs_buy/'},
                    {'text': 'Refinance Calculator', 'href': '/home-loan-refinance-calculator/'},
                ]
            },
        ]
    ),
    (
        'https://www.trulia.com/sold/San_Francisco,CA/SINGLE-FAMILY_HOME_type/6_srl/',
        [
            {
                'sectionHeaderText': 'Buy',
                'href': '/CA/San_Francisco/',
                'dropDown': [
                    {
                        'dropDownHeaderText': 'San Francisco',
                        'dropDownList': [
                            {'text': 'Homes for Sale', 'href': '/CA/San_Francisco/'},
                            {'text': 'Open Houses', 'href': '/for_sale/San_Francisco,CA/p_oh/'},
                            {'text': 'New Homes', 'href': '/for_sale/San_Francisco,CA/new_homes_lt/'},
                            {'text': 'Recently Sold', 'href': '/sold/San_Francisco,CA/'}
                        ]
                    },
                    {'text': 'See Newest Listings', 'href': '/new-for-sale-properties/CA/'}
                ]
            },
            {
                'sectionHeaderText': 'Rent',
                'href': '/for_rent/San_Francisco,CA/',
                'dropDown': [
                    {
                        'dropDownHeaderText': 'San Francisco',
                        'dropDownList': [
                            {'text': 'All Rentals', 'href': '/for_rent/San_Francisco,CA/'},
                            {'text': 'Apartments for Rent', 'href': '/for_rent/San_Francisco,CA/APARTMENT,APARTMENT_COMMUNITY,APARTMENT%7CCONDO%7CTOWNHOUSE,CONDO,COOP,LOFT,TIC_type/'},
                            {'text': 'Houses for Rent', 'href': '/for_rent/San_Francisco,CA/SINGLE-FAMILY_HOME_type/'},
                            {'text': 'Rooms for Rent', 'href': '/for_rent/San_Francisco,CA/ROOM_FOR_RENT_type/'}
                        ]
                    },
                    {'text': 'See Newest Listings', 'href': '/new-for-rent-properties/CA/'},
                    {'text': 'Post A Rental Listing', 'href': '/post-rental/'}
                ]
            },
            {
                'sectionHeaderText': 'Mortgage',
                'href': '/mortgages/',
                'dropDown': [
                    {
                        'dropDownHeaderText': 'San Francisco',
                        'dropDownList': [
                            {'text': 'Mortgage Overview', 'href': '/mortgages/'},
                            {'text': 'Get Pre-Qualified', 'href': '/mortgages/pre-approval?omni_src=mortgage%7Cglobal_nav'},
                            {'text': 'Mortgage Rates', 'href': '/mortgage-rates/San_Francisco,CA/'},
                            {'text': 'Refinance Rates', 'href': '/refinance-rates/San_Francisco,CA/'}
                        ]
                    },
                    {'text': 'Mortgage Calculator', 'href': '/mortgage-payment-calculator/'},
                    {'text': 'Affordability Calculator', 'href': '/house-affordability-calculator/'},
                    {'text': 'Rent vs Buy Calculator', 'href': '/rent_vs_buy/'},
                    {'text': 'Refinance Calculator', 'href': '/home-loan-refinance-calculator/'},
                ]
            },
        ]
    )
]

off_market_footer_test_cases = [
    (
        'https://www.trulia.com/sold/San_Francisco,CA/',
        [
            {
                'text': 'Nearby Real Estate',
                'citiesLinkGroup': [
                    {'text': 'Houses for Sale Near Me', 'href': '/houses-for-sale-near-me'},
                    {'text': 'Houses for Sale Near Me by Owner', 'href': '/houses-for-sale-near-me-by-owner'},
                    {'text': 'Open Houses Near Me', 'href': '/open-houses-near-me'},
                    {'text': 'Land for Sale Near Me', 'href': '/land-for-sale-near-me'},
                    {'text': 'Townhomes for Sale Near Me', 'href': '/townhomes-for-sale-near-me'},
                    {'text': 'Condos for Sale Near Me', 'href': '/condos-for-sale-near-me'}
                ]
            },
            {
                'text': 'San Francisco Neighborhoods | Nearby Zip Codes',
                'href': '/sitemaps/neighborhood/California/San_Francisco/'
            },
            {
                'text': 'San Francisco Property Types',
                'href': '/sitemaps/city-for-sale/CA/San_Francisco/',
                'propertyTypesLinkGroup': [
                    {'text': 'For Sale by Owner (FSBO)',
                     'href': '/for_sale/San_Francisco,CA/fsbo_lt/'},
                    {'text': 'Single Family Homes', 'href': '/for_sale/San_Francisco,CA/SINGLE-FAMILY_HOME_type/'},
                    {'text': 'Condos',
                     'href': '/for_sale/San_Francisco,CA/CONDO_type/'},
                    {'text': 'Townhomes', 'href': '/for_sale/San_Francisco,CA/TOWNHOUSE_type/'},
                    {'text': 'Co-Ops', 'href': '/for_sale/San_Francisco,CA/COOP_type/'},
                    {'text': 'Apartments', 'href': '/for_sale/San_Francisco,CA/APARTMENT_type/'},
                    {'text': 'Mobile/Manufactured Homes', 'href': '/for_sale/San_Francisco,CA/MOBILE|MANUFACTURED_type/'}
                ]
            },
            {
                'text': 'Real Estate and Mortgage Guides',
                'localInfoLinksGroup': [
                    {'text': 'San Francisco Real Estate Guide', 'href': '/real_estate/San_Francisco-California/'},
                    {'text': 'San Francisco Schools', 'href': '/real_estate/San_Francisco-California/schools/'},
                    {'text': 'Newest Homes for Sale in California', 'href': '/new-for-sale-properties/CA/'},
                    {'text': 'Newest Rentals in California', 'href': '/new-for-rent-properties/CA/'},
                    {'text': 'San Francisco Mortgage', 'href': '/mortgage-rates/San_Francisco,CA/'},
                    {'text': 'San Francisco Refinance', 'href': '/refinance-rates/San_Francisco,CA/'}
                ]
            }
        ]
    ),
    (
        'https://www.trulia.com/sold/San_Francisco,CA/SINGLE-FAMILY_HOME_type/6_srl/',
        [
            {
                'text': 'Nearby Real Estate',
                'citiesLinkGroup': [
                    {'text': 'Houses for Sale Near Me', 'href': '/houses-for-sale-near-me'},
                    {'text': 'Houses for Sale Near Me by Owner', 'href': '/houses-for-sale-near-me-by-owner'},
                    {'text': 'Open Houses Near Me', 'href': '/open-houses-near-me'},
                    {'text': 'Land for Sale Near Me', 'href': '/land-for-sale-near-me'},
                    {'text': 'Townhomes for Sale Near Me', 'href': '/townhomes-for-sale-near-me'},
                    {'text': 'Condos for Sale Near Me', 'href': '/condos-for-sale-near-me'}
                ]
            },
            {
                'text': 'San Francisco Neighborhoods | Nearby Zip Codes',
                'href': '/sitemaps/neighborhood/California/San_Francisco/'
            },
            {
                'text': 'San Francisco Property Types',
                'href': '/sitemaps/city-for-sale/CA/San_Francisco/',
                'propertyTypesLinkGroup': [
                    {'text': 'For Sale by Owner (FSBO)',
                     'href': '/for_sale/San_Francisco,CA/fsbo_lt/'},
                    {'text': 'Single Family Homes', 'href': '/for_sale/San_Francisco,CA/SINGLE-FAMILY_HOME_type/'},
                    {'text': 'Condos',
                     'href': '/for_sale/San_Francisco,CA/CONDO_type/'},
                    {'text': 'Townhomes', 'href': '/for_sale/San_Francisco,CA/TOWNHOUSE_type/'},
                    {'text': 'Co-Ops', 'href': '/for_sale/San_Francisco,CA/COOP_type/'},
                    {'text': 'Apartments', 'href': '/for_sale/San_Francisco,CA/APARTMENT_type/'},
                    {'text': 'Mobile/Manufactured Homes',
                     'href': '/for_sale/San_Francisco,CA/MOBILE|MANUFACTURED_type/'}
                ]
            },
            {
                'text': 'Real Estate and Mortgage Guides',
                'localInfoLinksGroup': [
                    {'text': 'San Francisco Real Estate Guide', 'href': '/real_estate/San_Francisco-California/'},
                    {'text': 'San Francisco Schools', 'href': '/real_estate/San_Francisco-California/schools/'},
                    {'text': 'Newest Homes for Sale in California', 'href': '/new-for-sale-properties/CA/'},
                    {'text': 'Newest Rentals in California', 'href': '/new-for-rent-properties/CA/'},
                    {'text': 'San Francisco Mortgage', 'href': '/mortgage-rates/San_Francisco,CA/'},
                    {'text': 'San Francisco Refinance', 'href': '/refinance-rates/San_Francisco,CA/'}
                ]
            }
        ]
    )
]

off_market_minimized_footer_test_cases = [
    (
        'https://www.trulia.com/sold/San_Francisco,CA/',
        [
            {'text': 'About Trulia', 'href': '//www.trulia.com/about/'},
            {'text': 'About Zillow Group', 'href': '//www.zillowgroup.com/about-zillow-group/'},
            {'text': 'Careers', 'href': '//www.trulia.com/about/careers/'},
            {'text': 'Newsroom', 'href': '//www.trulia.com/newsroom/'},
            {'text': 'Investor Relations', 'href': 'http://investors.zillowgroup.com/'},
            {'text': 'Advertising Terms', 'href': '//www.trulia.com/terms/advertisers/'},
            {'text': 'Privacy', 'href': '//www.trulia.com/info/privacy/'},
            {'text': 'Terms of Use', 'href': '//www.trulia.com/info/terms/'},
            {'text': 'Listings Quality Policy', 'href': '//www.trulia.com/info/listings-quality-policy/'},
            {'text': 'Subscription Terms', 'href': 'http://www.zillow.com/corp/SATerms.htm'},
            {'text': 'Help', 'href': 'https://support.trulia.com/hc/en-us'},
            {'text': 'Ad Choice', 'href': ''}
        ]
    )
]

