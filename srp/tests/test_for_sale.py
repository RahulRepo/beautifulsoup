import pytest
from srp.src.model.srp_page_model import SrpPageModel
from srp.src.model.seo_page_model_test_case import SEOPageModelTestCase
from srp.tests.test_cases.for_sale_seo_test_cases import for_sale_page_test_cases
from srp.tests.test_cases.for_sale_seo_test_cases import for_sale_navbar_test_cases
from srp.tests.test_cases.for_sale_seo_test_cases import for_sale_footer_test_cases
from srp.tests.test_cases.for_sale_seo_test_cases import for_sale_minimized_footer_test_cases


@pytest.fixture(scope="session")
def for_sale_srp_page_model(request):
    return SrpPageModel(request.param)


class TestForSaleSRP(SEOPageModelTestCase):

    @pytest.mark.parametrize('for_sale_srp_page_model', for_sale_page_test_cases, indirect=True)
    def test_title_count(self, for_sale_srp_page_model):
        assert for_sale_srp_page_model.get_title_count() == 1, 'Page title count is not 1.'
        for_sale_srp_page_model.browserquit()

    @pytest.mark.parametrize('for_sale_srp_page_model', for_sale_page_test_cases, indirect=True)
    def test_h1_count(self, for_sale_srp_page_model):
        assert for_sale_srp_page_model.get_h1_count() == 1, 'Page h1 count is not 1.'
        for_sale_srp_page_model.browserquit()

    @pytest.mark.parametrize('for_sale_srp_page_model', for_sale_page_test_cases, indirect=True)
    def test_title_text(self, for_sale_srp_page_model):
        assert 'Trulia' in for_sale_srp_page_model.get_title_text(), 'Trulia is not shown in page title.'
        for_sale_srp_page_model.browserquit()

    @pytest.mark.parametrize('for_sale_srp_page_model', for_sale_page_test_cases, indirect=True)
    def test_description_count(self, for_sale_srp_page_model):
        assert for_sale_srp_page_model.get_meta_description_count() == 1, 'Meta description count is not 1.'
        for_sale_srp_page_model.browserquit()

    @pytest.mark.parametrize('for_sale_srp_page_model', for_sale_page_test_cases, indirect=True)
    def test_description_text(self, for_sale_srp_page_model):
        description_text = for_sale_srp_page_model.get_meta_description_text()
        assert 50 <= len(description_text) <= 250, 'Meta description size is out of range.'
        for_sale_srp_page_model.browserquit()

    @pytest.mark.parametrize('for_sale_srp_page_model', for_sale_page_test_cases, indirect=True)
    def test_h1_text(self, for_sale_srp_page_model):
        assert 'For Sale' in for_sale_srp_page_model.get_h1_text('Sale') \
               or 'Open Houses' in for_sale_srp_page_model.get_h1_text('Sale'), 'H1 text is missing sold info.'
        for_sale_srp_page_model.browserquit()

    @pytest.mark.parametrize('for_sale_srp_page_model', for_sale_navbar_test_cases, indirect=True)
    def test_navbar_count(self, for_sale_srp_page_model):
        assert for_sale_srp_page_model.get_navbar_count() == 1, 'Navber count is not 1.'
        for_sale_srp_page_model.browserquit()

    @pytest.mark.parametrize('for_sale_srp_page_model', for_sale_navbar_test_cases, indirect=True)
    def test_navbar_menu_has_three_section(self, for_sale_srp_page_model):
        assert len(for_sale_srp_page_model.get_navbar_menu()) == 3, 'Navbar menu section is not 3.'
        for_sale_srp_page_model.browserquit()

    @pytest.mark.parametrize('for_sale_srp_page_model', for_sale_footer_test_cases, indirect=True)
    def test_footer_count(self, for_sale_srp_page_model):
        assert for_sale_srp_page_model.get_footer_count() == 1, 'Footer count is not 1.'
        for_sale_srp_page_model.browserquit()

    @pytest.mark.parametrize('for_sale_srp_page_model, expected_navbar_menu_links', for_sale_navbar_test_cases,
                             indirect=['for_sale_srp_page_model'])
    def test_navbar_menu_link_group(self, for_sale_srp_page_model, expected_navbar_menu_links):
        self.compare_section_header_text(for_sale_srp_page_model.get_navbar_menu_header_text(),
                                         expected_navbar_menu_links)
        self.compare_navbar_menu_links(for_sale_srp_page_model.get_navbar_menu(), expected_navbar_menu_links)
        for_sale_srp_page_model.browserquit()

    @pytest.mark.parametrize('for_sale_srp_page_model, expected_footer_link_group', for_sale_footer_test_cases,
                             indirect=['for_sale_srp_page_model'])
    def test_footer_link_group(self, for_sale_srp_page_model, expected_footer_link_group):
        footer_link_group = for_sale_srp_page_model.get_footer_link_group()
        assert len(footer_link_group) == 4 or len(footer_link_group) == 3, 'Footer link group count is not correct.'
        self.compare_footer_link_group(footer_link_group, expected_footer_link_group)
        for_sale_srp_page_model.browserquit()

    @pytest.mark.parametrize('for_sale_srp_page_model, expected_minimized_footer_link_group',
                             for_sale_minimized_footer_test_cases, indirect=['for_sale_srp_page_model'])
    def test_minimized_footer_link_group(self, for_sale_srp_page_model, expected_minimized_footer_link_group):
        minimized_footer_link_group = for_sale_srp_page_model.get_minimized_footer_link_group()
        self.compare_links(minimized_footer_link_group, expected_minimized_footer_link_group)
        for_sale_srp_page_model.browserquit()
