import pytest
from srp.src.model.srp_page_model import SrpPageModel
from srp.src.model.seo_page_model_test_case import SEOPageModelTestCase
from srp.tests.test_cases.off_market_seo_test_cases import off_market_page_test_cases
from srp.tests.test_cases.off_market_seo_test_cases import off_market_navbar_test_cases
from srp.tests.test_cases.off_market_seo_test_cases import off_market_footer_test_cases
from srp.tests.test_cases.off_market_seo_test_cases import off_market_minimized_footer_test_cases


@pytest.fixture(scope="session")
def off_market_srp_page_model(request):
    return SrpPageModel(request.param)


class TestOffMarketSRP(SEOPageModelTestCase):

    @pytest.mark.parametrize('off_market_srp_page_model', off_market_page_test_cases, indirect=True)
    def test_title_count(self, off_market_srp_page_model):
        assert off_market_srp_page_model.get_title_count() == 1, 'Page title count is not 1.'
        off_market_srp_page_model.browserquit()

    @pytest.mark.parametrize('off_market_srp_page_model', off_market_page_test_cases, indirect=True)
    def test_title_text(self, off_market_srp_page_model):
        assert 'Trulia' in off_market_srp_page_model.get_title_text(), 'Trulia is not shown in page title.'
        off_market_srp_page_model.browserquit()

    @pytest.mark.parametrize('off_market_srp_page_model', off_market_page_test_cases, indirect=True)
    def test_description_count(self, off_market_srp_page_model):
        assert off_market_srp_page_model.get_meta_description_count() == 1, 'Meta description count is not 1.'
        off_market_srp_page_model.browserquit()

    @pytest.mark.parametrize('off_market_srp_page_model', off_market_page_test_cases, indirect=True)
    def test_description_text(self, off_market_srp_page_model):
        description_text = off_market_srp_page_model.get_meta_description_text()
        assert 50 <= len(description_text) <= 250, 'Meta description size is out of range.'
        off_market_srp_page_model.browserquit()

    @pytest.mark.parametrize('off_market_srp_page_model', off_market_page_test_cases, indirect=True)
    def test_h1_count(self, off_market_srp_page_model):
        assert off_market_srp_page_model.get_h1_count() == 1, 'Page h1 count is not 1.'
        off_market_srp_page_model.browserquit()

    @pytest.mark.parametrize('off_market_srp_page_model', off_market_page_test_cases, indirect=True)
    def test_h1_text(self, off_market_srp_page_model):
        assert 'Sold' in off_market_srp_page_model.get_h1_text('sold'), 'Sold is not shown in page h1.'
        off_market_srp_page_model.browserquit()

    @pytest.mark.parametrize('off_market_srp_page_model', off_market_navbar_test_cases, indirect=True)
    def test_navbar_count(self, off_market_srp_page_model):
        assert off_market_srp_page_model.get_navbar_count() == 1, 'Navber count is not 1.'
        off_market_srp_page_model.browserquit()

    @pytest.mark.parametrize('off_market_srp_page_model', off_market_navbar_test_cases, indirect=True)
    def test_navbar_menu_has_three_section(self, off_market_srp_page_model):
        assert len(off_market_srp_page_model.get_navbar_menu()) == 3, 'Navbar menu section is not 3.'
        off_market_srp_page_model.browserquit()

    @pytest.mark.parametrize('off_market_srp_page_model, expected_navbar_menu_links', off_market_navbar_test_cases,
                             indirect=['off_market_srp_page_model'])
    def test_navbar_menu_link_group(self, off_market_srp_page_model, expected_navbar_menu_links):
        self.compare_navbar_menu_links(off_market_srp_page_model.get_navbar_menu(), expected_navbar_menu_links)
        off_market_srp_page_model.browserquit()

    @pytest.mark.parametrize('off_market_srp_page_model', off_market_footer_test_cases, indirect=True)
    def test_footer_count(self, off_market_srp_page_model):
        assert off_market_srp_page_model.get_footer_count() == 1, 'Footer count is not 1.'
        off_market_srp_page_model.browserquit()

    @pytest.mark.parametrize('off_market_srp_page_model, expected_footer_link_group', off_market_footer_test_cases,
                             indirect=['off_market_srp_page_model'])
    def test_footer_link_group(self, off_market_srp_page_model, expected_footer_link_group):
        footer_link_group = off_market_srp_page_model.get_footer_link_group()
        assert len(footer_link_group) == 4 or len(footer_link_group) == 3, 'Footer link group count is not correct.'
        self.compare_footer_link_group(footer_link_group, expected_footer_link_group)
        off_market_srp_page_model.browserquit()

    @pytest.mark.parametrize('off_market_srp_page_model, expected_minimized_footer_link_group',
                             off_market_minimized_footer_test_cases, indirect=['off_market_srp_page_model'])
    def test_minimized_footer_link_group(self, off_market_srp_page_model, expected_minimized_footer_link_group):
        minimized_footer_link_group = off_market_srp_page_model.get_minimized_footer_link_group()
        self.compare_links(minimized_footer_link_group, expected_minimized_footer_link_group)
        off_market_srp_page_model.browserquit()
