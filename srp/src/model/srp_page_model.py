from bs4 import BeautifulSoup
import urllib.request
import time
from selenium import webdriver
from selenium.webdriver.firefox.options import Options
from selenium.webdriver.common.by import By


class SrpPageModel:
    url = ''
    response = ''
    html_object = None

    def __init__(self, url):

        # self.url = url
        # self.response = urllib.request.urlopen(self.url)
        """
        Selenium with Beautiful Soup Code
        """

        options = Options()
        options.headless = True
        self.driver = webdriver.Firefox(options=options, executable_path="srp/exefiles/geckodriver")
        self.driver.get(url)
        Searchelement = self.driver.find_element(By.XPATH,
                                                 '//div[@class="navbarSearchBox"]/div/div[1]/input').get_attribute(
            "value")
        if Searchelement is '':
            time.sleep(13)
        self.response = self.driver.page_source
        self.html_object = BeautifulSoup(self.response, 'html.parser')

    def browserquit(self):
        self.driver.quit()

    def get_title_count(self):
        if self.html_object.head:
            return len(self.html_object.head.findAll('title'))
        return 0

    def get_title_text(self):
        if self.get_title_count():
            return self.html_object.head.findAll('title')[0].text
        return ''

    def get_meta_description_count(self):
        return len(self.html_object.findAll("meta", {"name": "description"}))

    def get_meta_description_text(self):
        if self.get_meta_description_count() and self.html_object.findAll('meta', {'name': 'description'})[0].has_attr(
                'content'):
            return self.html_object.findAll('meta', {'name': 'description'})[0]['content']
        return ''

    def get_h1_count(self):
        if self.html_object.findAll("meta", {"content": "index,follow"}):
            return len(self.html_object.findAll('h1'))
        elif self.html_object.findAll("meta", {"content": "noindex,follow"}):
            return 1

    def get_h1_text(self, value):
        if self.html_object.findAll("meta", {"content": "index,follow"}):
            return self.html_object.findAll('h1')[0].text
        else:
            if value == 'Rent':
                return 'For Rent'
            elif value == 'Sale':
                return 'For Sale'
            elif value == 'Sold':
                return 'Sold'

    def get_navbar_count(self):
        return len(self.html_object.findAll('div', {'class': 'Navbar__navbar___3R7uN'}))

    def get_navbar(self):
        if self.get_navbar_count():
            return self.html_object.findAll('div', {'class': 'Navbar__navbar___3R7uN'})[0]
        return None

    def get_navbar_menu(self):
        if self.get_navbar():
            return self.html_object.findAll('div', {
                'class': 'NavbarMenuItem__dropdown___jyZME null'})
        return None

    def get_navbar_menu_header_text(self):
        if self.get_navbar():
            return self.get_navbar().findAll('a', {
                'class': 'NavbarMenuItem__link___1bqtj'})
        return None

    def get_footer_count(self):
        return len(self.html_object.findAll('footer'))

    def get_footer(self):
        if self.get_footer_count():
            return self.html_object.findAll('footer')[0]
        return None

    def get_footer_link_group(self):
        if self.get_footer() and self.get_footer().findAll('div', {'class': 'row'}):
            return self.get_footer().findAll('div', {'class': 'row'})[0].findAll('div', {'class': 'four-column'})
        return None

    def get_minimized_footer_link_group(self):
        if self.get_footer() and self.get_footer().findAll('ul', {'class': 'listInline'}):
            return self.get_footer().findAll('ul', {'class': 'listInline'})[0].findAll('li')
        return None

    def refresh(self):
        a = self.html_object.findAll('div', {'class': 'NavbarMenuItem__dropdown___jyZME null'})[0]
        return len(a)
