class SEOPageModelTestCase:

    def compare_section_header_text(self, navbar_header, expected):
        assert len(navbar_header) == len(expected), 'Navbar menu section size is not matching expected.'
        for index, section in enumerate(navbar_header):
            expected_data = expected[index]
            assert section.text == expected_data['sectionHeaderText'], \
                expected_data['sectionHeaderText'] + ' navbar section header text is not correct.'
            assert self.get_link_text_from_simple_node_navbar(section) == expected_data['href'], \
                expected_data['sectionHeaderText'] + ' navbar section header link text is not correct.'

    def compare_navbar_menu_links(self, navbar_menu, expected):
        assert len(navbar_menu) == len(expected), 'Navbar menu section size is not matching expected.'
        for index, section in enumerate(navbar_menu):
            expected_data = expected[index]
            drop_down_list = [tag for tag in section]
            self.compare_drop_down(drop_down_list, expected_data['dropDown'])

    def compare_drop_down(self, drop_down, expected_drop_down):
        expectedDropdownValue = []
        drop_downValue = []
        expLinks = []
        drop_downlink = []
        for index, li in enumerate(expected_drop_down):
            try:
                expectedDropdownValue.append(li['dropDownHeaderText'])
                for index1 in li['dropDownList']:
                    expectedDropdownValue.append(index1['text'])
                    expLinks.append((index1['href']))
            except:
                expectedDropdownValue.append(li['text'])
                expLinks.append(li['href'])

        for index, li in enumerate(drop_down):
            if li.text != '':
                drop_downValue.append(li.text)
                try:
                    drop_downlink.append(li['href'])
                except:
                    None
        assert len(expectedDropdownValue) == len(drop_downValue), 'compare dropdown value is not matching'
        assert expectedDropdownValue == drop_downValue, 'dropdown value is not matching'
        assert expLinks == drop_downlink, 'dropdown value is not matching'

    def compare_footer_link_group(self, footer_link_group, expected_footer_link_group):
        assert len(footer_link_group) == len(expected_footer_link_group), \
            'Footer link group size is not matching expected.'
        for index, link_group in enumerate(footer_link_group):
            expected_link_group = expected_footer_link_group[index]
            link_group_header = link_group.find('div', {'class': 'listHeader'})
            assert link_group_header.text in expected_link_group['text'], \
                expected_link_group['text'] + ' footer link group title text is not correct.'
            if link_group_header.find('a'):
                assert self.get_link_text_from_simple_node(link_group_header) == expected_link_group['href'], \
                    expected_link_group['text'] + ' footer link group link text is not correct.'
            links = link_group.findAll('div', {'class': 'pvs'})
            if 'Nearby Rentals' in link_group_header.text:
                assert len(links) >= 3, 'Footer link group size is not correct.'
                self.compare_links(links[:8], expected_link_group['citiesLinkGroup'])
            elif 'Nearby Real Estate' in link_group_header.text:
                assert len(links) >= 2, 'Footer link group size is not correct.'
                self.compare_links(links[:6], expected_link_group['citiesLinkGroup'])
            elif 'Neighborhoods' in link_group_header.text or 'Zip Codes' in link_group_header.text:
                assert 1 <= len(links) <= 326, 'Footer link group size is not correct.'
            elif 'Type' in link_group_header.text:
                self.compare_links(links, expected_link_group['propertyTypesLinkGroup'])
            else:
                self.compare_links(links, expected_link_group['localInfoLinksGroup'])

    def compare_links(self, links, expected_links):
        assert len(links) == len(expected_links), 'Link group size is not matching expected.'
        for index, link in enumerate(links):
            self.compare_link(link, expected_links[index])

    def compare_link(self, li, expected_li):
        received_text = li.find('a').text.rstrip() if li.find('a') else None
        assert received_text == expected_li['text'], 'Anchor text is not correct.'
        received_href = self.get_link_text_from_simple_node(li)
        assert received_href == expected_li['href'], 'Link text is not correct.'

    def get_link_text_from_simple_node_navbar(self, simple_node):
        if simple_node.has_attr('href'):
            return simple_node['href']
        return ''

    def get_link_text_from_simple_node(self, simple_node):
        if simple_node.findAll('a') and simple_node.findAll('a')[0].has_attr('href'):
            return simple_node.findAll('a')[0]['href']
        return ''
